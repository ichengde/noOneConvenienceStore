

class Status {
    private static _instance: Status
    public static get instance() {
        if (!Status._instance) {
            Status._instance = new Status()
        }
        return Status._instance
    }

    option: any
}

let status = Status.instance

export { status }