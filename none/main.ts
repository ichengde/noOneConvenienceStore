import './resource/style.css'
import { projectConfig } from './config'
import { status } from './status'
import { controller } from './controller'

declare var InWalkPlayer
declare var worldRenderer


class InWalkM {
    private static _instance: InWalkM
    public static get instance() {
        if (!InWalkM._instance) {
            InWalkM._instance = new InWalkM()
        }
        return InWalkM._instance
    }

    EnterAnimation: boolean = true
    trigger: HTMLDivElement = <HTMLDivElement>document.querySelector('.trigger-image')

    start() {

        const pic = [['./images/pano/door.jpg'], ['./images/pano/1.jpg'], ['./images/pano/2.jpg'], ['./images/pano/3.jpg']]
        let angle = 0
        status.option = {
            parent: 'vr-panel',
            source: pic,
            publicKey: projectConfig.publicKey,
            showBottomMark: false,
            autoLoad: false,
            bottomMark: false,
            defaultYaw: angle || 0,
            server_hide_bottom_mark: true
        }
        controller.vr = new InWalkPlayer(status.option)

        controller.vr.on('canplay', this.panoramaImageLoadingIsComplete.bind(this))
        controller.vr.load()


    }

    panoramaImageLoadingIsComplete() {
        if (this.EnterAnimation === true) {
            // canplay以后才能禁用陀螺仪 注意触发位置
            controller.vr.startEnterAnimation(3000, 90, 150)

            controller.vr.on('animationComplete', () => {

            })

            this.EnterAnimation = false
        }
        controller.loadPanoFinish()
    }

}

window.onload = function () {
    let main = new InWalkM()

    main.start()
}
