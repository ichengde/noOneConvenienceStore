
import { positionType, SwitchAnimationType } from './type'
import { hotspotData } from './dataSource'
import { status } from './status'

declare var window

class Controller {
    private static _instance: Controller
    public static get instance() {
        if (!Controller._instance) {
            Controller._instance = new Controller()
        }
        return Controller._instance
    }

    vr: any
    hotspotList: any[] = []
    position: positionType = positionType.door
    videoDom = <HTMLVideoElement>document.querySelector('.video')

    videoFinish: Function
    videoPlay: Function

    audioDom = <HTMLVideoElement>document.querySelector('#audio')

    audioData = [
        { name: 'door', src: './audios/door.mp4', flag: false },
        // { name: 'first', src: '', flag: false },
        { name: 'lottery', src: './audios/lottery.mp4', flag: false },
        { name: 'third', src: './audios/third.mp4', flag: false }
    ]
    constructor() {

        window.addEventListener('touchstart', (e) => {
            if (this.position === positionType.door) {
                this.playAudio('door')
            }
            if (this.position === positionType.third) {
                this.playAudio('third')
            }
        }, false)

        window.addEventListener('touchmove', (e) => {
            e.preventDefault();
        }, false)

        this.videoDom.addEventListener('canplay', () => {
        })

        this.videoDom.addEventListener('play', () => {
            this.videoDom.classList.add('videoPlay')
            this.videoDom.classList.remove('videoPause')
            if (this.videoPlay) {
                this.videoPlay()
                this.videoPlay = undefined
            }
        })

        this.videoDom.addEventListener('ended', () => {
            this.videoDom.classList.remove('videoPlay')
            this.videoDom.classList.remove('videoPause')
            if (this.videoFinish) {
                this.videoFinish()
                this.videoFinish = undefined
            }
        })

        window.getRotate = this.getRotate.bind(this)
    }
    loadPanoFinish() {
        console.log(this.position)
        console.log(hotspotData[this.position])
        hotspotData[this.position].forEach((hotspot) => {
            if (hotspot.type === 'point') {
                this.createHotspot(hotspot.longitude, hotspot.latitude, () => {
                    this.audioDom.pause()
                    this.videoDom.src = `./videos/${hotspot.name}.mp4`
                    this.videoDom.play()
                    this.videoPlay = () => {
                        if (this.position === positionType.door) {
                            this.position = positionType.first
                            this.switchSence('next', 0, 0)
                        }
                    }
                    this.videoFinish = () => {
                        if (hotspot.name === 'settlement') {
                            this.position = positionType.door
                            this.vr.reload(status.option)
                        }
                    }

                    if (hotspot.name === 'lottery' && this.position === positionType.third) {
                        this.playAudio('lottery')
                    }
                });
            }

            if (hotspot.type === 'arrow') {
                if (this.position === positionType.first) {
                    this.createArrow(hotspot.longitude, hotspot.latitude, () => {
                        // this.playAudio('first')
                        this.position = positionType.second
                        this.switchSence('next', 0, 180)
                    })
                }

                if (this.position === positionType.second) {
                    this.position = positionType.second
                    this.createArrow(hotspot.longitude, hotspot.latitude, () => {
                        this.position = positionType.third
                        this.switchSence('next', 126, 103)
                    })
                }
            }

            if (this.position === positionType.third) {
                this.vr.autoRotate(-64, 0, 5000, 2);
            }

            if (hotspot.type === 'text') {
                if (this.position === positionType.door) {
                    this.createText(hotspot.name, hotspot.longitude, hotspot.latitude, () => {
                        this.videoDom.src = `./videos/door.mp4`
                        this.videoDom.play()
                        this.videoPlay = () => {
                            if (this.position === positionType.door) {
                                this.position = positionType.first
                                this.switchSence('next', 0, 0)
                            }
                        }
                    })
                }

                if (this.position === positionType.second) {
                    this.createText(hotspot.name, hotspot.longitude, hotspot.latitude, () => {
                        this.audioDom.pause()
                        this.videoDom.src = `./videos/freshFruits.mp4`
                        this.videoDom.play()
                    })
                }
            }
        })
    }

    createText(text, longitude, latitude, action: Function) {
        const param = {
            type: 3,
            longitude: longitude,
            latitude: latitude,
            fontSize: 30,
            fontColor: "#fb463b",
            backgroundColor: "#ffffff",
            vertical: true,
            text: text,
            layer: 2
        };


        const hotspot = this.vr.overlay(param)
        hotspot.event = () => {
            action()
        }
        this.vr.addOverlay(hotspot)
        this.hotspotList.push(hotspot)
    }
    createArrow(longitude, latitude, action: Function) {
        const param = {
            image: './images/arrow.png',
            longitude: longitude,
            latitude: latitude,
            layer: 0,
            type: 2,
            duration: 100,
            landmark: true,
            tilesHorizontal: 1,
            tilesVertical: 25,
            numberOfTiles: 25,
            sizeScale: 1
        }

        const hotspot = this.vr.overlay(param)
        hotspot.event = () => {
            action()
        }
        this.vr.addOverlay(hotspot)
        this.hotspotList.push(hotspot)
    }

    createHotspot(longitude, latitude, func: Function) {

        const param = {
            image: './images/point.png',
            longitude: longitude,
            latitude: latitude,
            layer: 0,
            type: 2,
            duration: 50,
            landmark: false,
            tilesHorizontal: 1,
            tilesVertical: 25,
            numberOfTiles: 25,
            sizeScale: 2
        }

        const hotspot = this.vr.overlay(param)
        hotspot.event = () => {
            // 播放视频
            func()
        }

        this.vr.addOverlay(hotspot)
        this.hotspotList.push(hotspot)
    }

    switchSence(action: 'next' | 'previous', rotateToLongitude = 0, newSenceLongitude) {
        let effect = SwitchAnimationType.Rotate | SwitchAnimationType.Zoom
        let rotateTolatitude = 0


        if (action === 'next') {
            this.vr.switch(true, effect, rotateToLongitude, rotateTolatitude, newSenceLongitude, 0)
        }

        if (action === 'previous') {
            this.vr.switch(false, SwitchAnimationType.None, undefined, undefined, newSenceLongitude, 0)
        }
    }

    getRotate() {

        const longitude = this.vr.getLongitude() / Math.PI * 180
        const latitude = this.vr.getLatitude() / Math.PI * 180

        console.log(longitude, latitude)
    }

    playAudio(which) {
        const data = this.audioData.find((dt) => {
            return dt.name === which
        })
        if (data.flag === false) {
            this.audioDom.src = data.src
            this.audioDom.play()
            data.flag = true
        }
    }
}

let controller = Controller.instance

export { controller }