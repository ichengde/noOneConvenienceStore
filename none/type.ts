
export enum SwitchAnimationType {
    None = 0,
    Rotate = 1,
    Zoom = 1 << 1,
    ZoonAfterRotate = 1 << 2,
    RotateYAfterX = 1 << 3
}


export enum positionType {
    door = 'door',
    first = 'first',
    second = 'second',
    third = 'third'
}

export enum position2pt {
    door,
    first,
    second,
    third
}

