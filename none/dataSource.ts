export const hotspotData = {
    "door": [{
        type: 'point',
        longitude: 10,
        latitude: 10,
        name: 'door',
    }],
    "first": [{
        type: 'arrow',
        longitude: 0,
        latitude: -30,
        name: 'first'
    }],
    "second": [{
        type: 'point',
        longitude: 172,
        latitude: -17,
        name: 'freshFruits'
    }, {
        type: 'point',
        longitude: 50,
        latitude: -14,
        name: 'mangoDry'
    }, {
        type: 'arrow',
        longitude: 120,
        latitude: -45,
        name: 'second'
    }],
    "third": [{
        type: 'point',
        longitude: -32,
        latitude: -13,
        name: 'lottery'
    }, {
        type: 'point',
        longitude: -65,
        latitude: 4,
        name: 'settlement'
    }]
}



/*  {
    "door": [{
        type: 'point',
        longitude: 10,
        latitude: 10,
        name: 'door',
    }, {
        type: 'text',
        longitude: 0,
        latitude: -1,
        name: '欢迎来到WELL GO，点我开门吧！'
    }],
    "first": [{
        type: 'arrow',
        longitude: 0,
        latitude: -30,
        name: 'first'
    }],
    "second": [{
        type: 'point',
        longitude: 172,
        latitude: -17,
        name: 'freshFruits'
    },{
        type: 'text',
        longitude: 172,
        latitude: -27,
        name: 'freshFruits'
    }, {
        type: 'point',
        longitude: 50,
        latitude: -14,
        name: 'mangoDry'
    }, {
        type: 'arrow',
        longitude: 120,
        latitude: -45,
        name: 'second'
    }],
    "third": [{
        type: 'point',
        longitude: -32,
        latitude: -13,
        name: 'lottery'
    }, {
        type: 'point',
        longitude: -65,
        latitude: 4,
        name: 'settlement'
    }]
} */