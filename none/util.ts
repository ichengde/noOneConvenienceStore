
interface ILiteEvent<T> {
    on(handler: { (data?: T): void }): void;
    off(handler: { (data?: T): void }): void;
}

class LiteEvent<T> implements ILiteEvent<T> {
    private handlers: { (data?: T): void; }[] = [];

    public on(handler: { (data?: T): void }): void {
        this.handlers.push(handler);
    }

    public off(handler: { (data?: T): void }): void {
        this.handlers = this.handlers.filter(h => h !== handler);
    }

    public trigger(data?: T) {
        this.handlers.slice(0).forEach(h => h(data));
    }

    public expose(): ILiteEvent<T> {
        return this;
    }
}

class commonUtil {
    static getQueryParameter(name): string {
        name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]')
        let regex = new RegExp('[\\?&]' + name + '=([^&#]*)')
        let results = regex.exec(window.location.search)
        return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '))
    }

    static debug(outStream) {
        console.error(outStream)
    }

    static jsonToStr(params): string {
        let str = ''
        Object.keys(params).forEach(function (key) {
            str += key + '=' + params[key] + '&'
        })
        return str
    }

    static isIOS(): boolean {
        return /(iPad|iPhone|iPod)/g.test(navigator.userAgent)
    }

    static isIOS9OrLess(): boolean {
        if (!this.isIOS()) {
            return false
        }
        let re = /(iPhone|iPad|iPod) OS ([\d_]+)/
        let iOSVersion = navigator.userAgent.match(re)
        if (!iOSVersion) {
            return false
        }
        // Get the last group.
        let versionString = iOSVersion[iOSVersion.length - 1]
        let majorVersion = parseFloat(versionString)
        return majorVersion <= 9
    }

    static browser = {
        versions: (function () {
            let u = navigator.userAgent
            return {
                trident: u.indexOf('Trident') > -1, // IE内核
                presto: u.indexOf('Presto') > -1, // opera内核
                webKit: u.indexOf('AppleWebKit') > -1, // 苹果、谷歌内核
                gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') === -1, // 火狐内核
                mobile: !!u.match(/AppleWebKit.*Mobile.*/), // 是否为移动终端
                ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), // ios终端
                android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1, // android终端或者uc浏览器
                iPhone: u.indexOf('iPhone') > -1, // 是否为iPhone或者QQHD浏览器
                iPad: u.indexOf('iPad') > -1, // 是否iPad
                webApp: u.indexOf('Safari') === -1 // 是否web应该程序，没有头部与底部
            }
        })(),
        language: (navigator.language).toLowerCase()
    }


}



class transitionUtil {
    static getTransitionTimeForHotspot(transition, hotspot, isChoiceFirst) {
        let rightContainer = this.getRightTransitionWithHotspotForEachTransition(transition, hotspot, isChoiceFirst)
        if (rightContainer.reverse === true) {
            return parseFloat(rightContainer.transition.time) + rightContainer.transition.duration - JSON.parse(hotspot.position).timeoffset
        } else {
            return parseFloat(rightContainer.transition.time) + JSON.parse(hotspot.position).timeoffset
        }
    }

    static calcTransitionTimeForFlat(transitions, transitionIndex, hotspot) {
        let transition = transitions[transitionIndex]

        if (transitions.length === 2) {
            if (transitions[0].videoId === transitions[1].videoId) {
                if (transitions[0].reverse === true) {
                    return transitions[0].duration - JSON.parse(hotspot.position).timeoffset
                } else {
                    return JSON.parse(hotspot.position).timeoffset
                }
            }
        }

        if (transition.reverse === true) {
            return parseFloat(transition.time) + transition.duration - JSON.parse(hotspot.position).timeoffset
        } else {
            return parseFloat(transition.time) + JSON.parse(hotspot.position).timeoffset
        }
    }

    static getRightTransitionWithHotspotForEachTransition(transition, hotspot, isChoiceFirst) {
        let _VrVideoControl = this
        let tempJudge
        let rightTransition
        let rightJudge

        transition.forEach(function (videoClip) {
            tempJudge = _VrVideoControl.isTheTransitionToHotspot(videoClip, hotspot)
            if (tempJudge.result === true && isChoiceFirst === true) {
                rightTransition = videoClip
                rightJudge = tempJudge
                isChoiceFirst = false
            }

            if (tempJudge.result === true && isChoiceFirst === false) {
                rightTransition = videoClip
                rightJudge = tempJudge
            }
        })

        if (rightJudge && rightJudge.result === true) {
            return {
                transition: rightTransition,
                reverse: rightJudge.reverse
            }
        } else {
            console.error('rightJudge error', rightJudge)
            return undefined
        }
    }

    static isTheTransitionToHotspot(transition, hotspot) {
        let cmpeHotspotPosition = JSON.parse(hotspot.position)
        let cmpeTransitionHead = parseInt(transition.startNodeId)
        let cmpeTransitionTail = parseInt(transition.endNodeId)

        if (cmpeTransitionHead === cmpeHotspotPosition.nodeId &&
            cmpeTransitionTail === cmpeHotspotPosition.oppositeNodeId) {
            return {
                result: true,
                reverse: false
            }
        }
        if (cmpeTransitionHead === cmpeHotspotPosition.oppositeNodeId &&
            cmpeTransitionTail === cmpeHotspotPosition.nodeId
        ) {
            return {
                result: true,
                reverse: true
            }
        }

        return {
            result: false
        }
    }

    static calcTransitionAngle(transitions, transitionPos, currentTime, survey, angleRatio = 1) {
        // let angleRatio = 0.75
        let angleSpeed = Math.min(transitions[transitionPos].angleSpeed, transitions[transitionPos + 1] ? transitions[transitionPos + 1].angleSpeed : transitions[transitionPos].angleSpeed)
        let spiltTime = (transitionPos < transitions.length - 1) ? (transitions[transitionPos].duration - angleSpeed * angleRatio * 2) : transitions[transitionPos].duration

        let prevAngleSpeed = transitionPos > 0 ? Math.min(transitions[transitionPos].angleSpeed, transitions[transitionPos - 1].angleSpeed) : 0
        let prevSpiltTime = prevAngleSpeed * (1 - angleRatio) * 2


        let diffAngle: number
        let endAngle: number
        let startAngle: number
        let mySplitTime: number
        let duration: number

        if (currentTime < prevSpiltTime && transitionPos > 0) {
            // last transition
            diffAngle = transitions[transitionPos].offset + transitions[transitionPos].corner - transitions[transitionPos - 1].offset
            endAngle = this.formatAngle(transitions[transitionPos].offset)

            let hotspots = survey.getHotspotsByVideoId(transitions[transitionPos].videoId)
            if (transitionPos === transitions.length - 1 && hotspots && hotspots[0].position.focus) {
                endAngle = this.formatAngle(hotspots[0].position.angle)
            }

            let angles = this.minAngle(transitions[transitionPos - 1].offset + diffAngle, endAngle)
            let finalAngle = angles[1] - angles[0]

            startAngle = this.formatAngle(endAngle - finalAngle * (1 - angleRatio))
            duration = prevSpiltTime
            mySplitTime = prevSpiltTime
        } else if (currentTime > spiltTime && transitionPos < transitions.length - 1) {
            diffAngle = transitions[transitionPos + 1].offset + transitions[transitionPos + 1].corner - transitions[transitionPos].offset

            startAngle = this.formatAngle(transitions[transitionPos].offset)
            let finalAngle = transitions[transitionPos + 1].offset - diffAngle

            let hotspots = survey.getHotspotsByVideoId(transitions[transitionPos + 1].videoId)
            if (transitionPos + 1 == transitions.length - 1 && hotspots && hotspots[0].position.focus) {
                finalAngle = this.formatAngle(hotspots[0].position.angle - diffAngle)
            }

            let angles = this.minAngle(finalAngle, startAngle)
            finalAngle = angles[0] - angles[1]

            endAngle = this.formatAngle(startAngle + finalAngle * angleRatio)
            duration = transitions[transitionPos].duration - spiltTime
            mySplitTime = spiltTime
        } else {
            startAngle = this.formatAngle(transitions[transitionPos].offset)
            endAngle = this.formatAngle(transitions[transitionPos].offset)
            let hotspots = survey.getHotspotsByVideoId(transitions[transitionPos].videoId)
            if (transitionPos === transitions.length - 1 && hotspots && hotspots[0].position.focus) {
                startAngle = endAngle = this.formatAngle(hotspots[0].position.angle)
            }
        }

        let angles = this.minAngle(endAngle, startAngle);
        endAngle = angles[0];
        startAngle = angles[1];

        console.log('startAngle', startAngle, 'endAngle', endAngle)
        if (startAngle !== endAngle) {
            // let nowAngle

            let rate = Math.abs(currentTime - mySplitTime) / duration * (endAngle - startAngle)

            return startAngle + rate
        }
        return startAngle
    }

    static formatAngle(a1) {
        a1 = a1 % 360
        if (a1 < 0) a1 += 360
        return a1
    }

    static minAngle(a1, a2) {
        if (a1 - a2 > 180) {
            a1 -= 360;
        } else if (a2 - a1 > 180) {
            a1 += 360;
        }
        return [a1, a2];
    }

    static isObjectValueEqual(a, b) {
        let aProps = Object.getOwnPropertyNames(a)
        let bProps = Object.getOwnPropertyNames(b)

        if (aProps.length !== bProps.length) {
            return false
        }

        for (let i = 0; i < aProps.length; i++) {
            let propName = aProps[i]

            if (a[propName] !== b[propName]) {
                return false
            }
        }

        return true
    }

    static angleOf(a1, a2) {
        a1 = a1 % 360
        if (a1 < 0) a1 += 360
        a2 = a2 % 360
        if (a2 < 0) a2 += 360

        let angle = Math.abs(a1 - a2)
        if (angle > 180) angle -= 360
        return Math.abs(angle)
    }
}

class materialVideoUtilClass {

    private static _instance: materialVideoUtilClass
    public static get instance() {
        if (!materialVideoUtilClass._instance) {
            materialVideoUtilClass._instance = new materialVideoUtilClass()
        }
        return materialVideoUtilClass._instance
    }

    private _video


    get video(): any {
        let v = this._video
        this._video = undefined
        return v
    }
}


let materialVideoUtil = materialVideoUtilClass.instance


export { commonUtil, LiteEvent, transitionUtil, materialVideoUtil }


