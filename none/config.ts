

import { commonUtil } from './util'

class projectConfigFunc  {
    private static instance: projectConfigFunc
    static get Instance() {
        if (!projectConfigFunc.instance) {
            projectConfigFunc.instance = new projectConfigFunc()
        }
        return projectConfigFunc.instance
    }

    panoSrc = []
    publicKey = 'LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0KTUZ3d0RRWUpLb1pJaHZjTkFRRUJCUUFEU3dBd1NBSkJBSTR5RE9Ob1U4U0FXSkl6NjVQaFUvTCtIQVRWeGRtOApPakJIVHNlQW0yVjNmSzdXWUN1ekV4QUtXSVdIVXJ4aGxGSjJHT0Y0YTEzKzM4L3M0bG00L2NjQ0F3RUFBUT09Ci0tLS0tRU5EIFBVQkxJQyBLRVktLS0tLQ=='
    isVrMode = false
    apiSite = location.protocol + '//api.zhixianshi.com/'
    roamId = '59f2e74567f3560044595acb'


    private constructor() {
        if (commonUtil.getQueryParameter('vr') === '') {
            this.isVrMode = false
        } else {
            this.isVrMode = true
        }

        let env = commonUtil.getQueryParameter('environment')
        if (env) {
            this.apiSite = location.protocol + '//' + env + '.zhixianshi.com/'
        } else {
            this.apiSite = location.protocol + '//api.zhixianshi.com/'
        }

        if (commonUtil.getQueryParameter('roamId') !== '') {
            this.roamId = commonUtil.getQueryParameter('roamId')
        }



        // 如果只传优惠券的情况
        if (commonUtil.getQueryParameter('cardId') !== '') {
        }

    }

}

let projectConfig = projectConfigFunc.Instance

export { projectConfig };
