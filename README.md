# demo (http://demo.inreal.tv/)
## install dependence
```bash
npm install
```

## build
```bash
npm run build
```

## watch
```bash
npm run watch
```

## preview
```bash
cd build
http-server
```