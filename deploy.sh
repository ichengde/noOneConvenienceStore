#!/bin/bash

OSS_PATH="oss://zhixianshi-hn-roam/test-html/"
CDN_URL=("http://video.ali.inreal.tv/test-html/")

while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    api)
    OSS_PATH="oss://zhixianshi-hn-roam/html-v2/"
    CDN_URL=("http://video.ali.inreal.tv/html-v2/" "http://szgs.zhixianshi.com/html-v2/")
    shift # past argument
    ;;
    dev)
    OSS_PATH="oss://zhixianshi-hn-roam/test-html/"
    CDN_URL=("http://video.ali.inreal.tv/test-html/")
    shift
    ;;
    zhuzilin)
    OSS_PATH="oss://zhixianshi-hn-roam/zhuzilin/"
    CDN_URL=("http://szgs.zhixianshi.com/zhuzilin/")
    shift
    ;;
    qianhai)
    OSS_PATH="oss://zhixianshi-hn-roam/qianhai/"
    CDN_URL=("http://szgs.zhixianshi.com/qianhai/")
    shift
    ;;
    baogou)
    OSS_PATH="oss://zhixianshi-hn-roam/rainbow/baogou/"
    CDN_URL=("https://mall.zhixianshi.com/rainbow/baogou/")
    shift
    ;;
    none)
    OSS_PATH="oss://zhixianshi-hn-roam/rainbow/noOneConvenienceStore/"
    CDN_URL=("https://mall.zhixianshi.com/rainbow/noOneConvenienceStore/")
    shift
    ;;
    *)
    OSS_PATH="oss://zhixianshi-hn-roam/${key}/html/"
    CDN_URL=("http://video.ali.inreal.tv/${key}/html/")
    shift
    ;;
esac
done

cd deploy

cd OSS_Python_API_20160419

echo "upload files to ${OSS_PATH}"

python osscmd uploadfromdir ../../build ${OSS_PATH} --replace=true --thread_num=10 --host=oss-cn-shenzhen.aliyuncs.com
if [ $? -ne 0 ]; then
	echo "upload to aliyun oss ${OSS_PATH} failed."
	exit 1
fi

FILES=`find ../../build -type f -regex ".*\.\(html\|css\|js\|map\)"`
for entry in ${FILES}
do
    FILE=${OSS_PATH}`echo "$entry" | sed -r 's/^.{12}//'`
    echo -n "set no-cache for ${FILE}"

    python osscmd copy ${FILE} ${FILE} --headers="Cache-Control:no-cache,no-store,must-revalidate#Expires:0" --host=oss-cn-shenzhen.aliyuncs.com
    echo ""
done

cd -

for u in "${CDN_URL[@]}"
do
	echo "refresh cdn $u"
	python refresh_cdn.py -p "$u" -t "Directory"
	if [ $? -ne 0 ]; then
		echo "refresh cdn $u failed."
		exit 1
	fi
done