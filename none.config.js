const webpack = require('webpack');
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
// const CleanWebpackPlugin = require('clean-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const { CheckerPlugin } = require('awesome-typescript-loader')
const fs = require('fs')
const prod = process.env.NODE_ENV === 'production'

var gracefulFs = require('graceful-fs')
gracefulFs.gracefulify(fs)

let sdkPath = './sdk/build'

module.exports = {
  entry: {
    public: './none/main.ts'
  },
  output: {
    path: path.join(__dirname, 'build'),
    filename: '[name].js'
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: 'css-loader'
        })
      },
      {
        test: /\.(png|jpg|gif)$/,
        use: [{
          loader: 'file-loader',
          options: {}
        }]
      },
      {
        test: /\.tsx?$/,
        loader: 'awesome-typescript-loader'
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({ // 根据模板插入css/js等生成最终HTML
      filename: 'index.html',
      template: './none/index.html',
      inject: 'body',
      hash: prod,
      // chunks: ['vendors', 'index'], //需要引入的chunk，不配置就会引入所有页面的资源
      minify: { // 压缩HTML文件
        removeComments: prod, // 移除HTML中的注释
        collapseWhitespace: prod // 删除空白符与换行符
      }
    }),
    new ExtractTextPlugin('style.css'),
    new CopyWebpackPlugin([
      { from: './none/resource/', to: './images' },
      { from: './videos', to: './videos' },
      { from: './audios', to: './audios' },
      { from: sdkPath, to: './vr/build' }
    ]),
    new CheckerPlugin()
  ],
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.jsx']
  },
  devServer: {
    inline: true,
    hot: true
  }
}

if (prod) {
  module.exports.plugins.push(
    new webpack.optimize.UglifyJsPlugin({
      parallel: {
        cache: true,
        workers: 2 // for e.g
      },
      output: {
        comments: false,
        beautify: false,
      },
      compress: {
        // remove warnings
        warnings: false,
        // Drop console statements
        drop_console: true
      },
      ecma: 5,
    }))
} else {
  module.exports.bail = !prod
  module.exports.devtool = 'source-map'
}
