

import { commonUtil } from './control/util/util'
import { projectConfigValidation, videoQuality, loadInfoValidation, detailInfoValidation, bottomMarkValidation } from './control/util/type'
import { debug } from './control/util/debug'
import { individuation } from './control/ui/individuation'
import { wechatApi } from './control/plug/wechatShare';
import * as Cookies from "js-cookie"

class projectConfigFunc implements projectConfigValidation {
    private static instance: projectConfigFunc
    public debug: debug = new debug()
    static get Instance() {
        if (!projectConfigFunc.instance) {
            projectConfigFunc.instance = new projectConfigFunc()
        }
        return projectConfigFunc.instance
    }

    panoSrc = []
    publicKey = 'LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0KTUZ3d0RRWUpLb1pJaHZjTkFRRUJCUUFEU3dBd1NBSkJBSTR5RE9Ob1U4U0FXSkl6NjVQaFUvTCtIQVRWeGRtOApPakJIVHNlQW0yVjNmSzdXWUN1ekV4QUtXSVdIVXJ4aGxGSjJHT0Y0YTEzKzM4L3M0bG00L2NjQ0F3RUFBUT09Ci0tLS0tRU5EIFBVQkxJQyBLRVktLS0tLQ=='
    isVrMode = false
    apiSite = location.protocol + '//api.zhixianshi.com/'
    roamId = '59f2e74567f3560044595acb'
    videoChosen = videoQuality.lowDefinition
    detailInFo: detailInfoValidation
    individuation: individuation = new individuation()
    wechat_app_id = 'wx87c8bf4709916009'

    // API
    // statAppId = 'TlDRzRjdshNT1wSNuAhnkxVD-gzGzoHsz'
    // statAppKey = 'YcbYYD2xXohRvtDaj2gSL15d'

    // DEV
    statAppId = 'lsTyR48xeDrnvLtNvoIAc3Gk-gzGzoHsz'
    statAppKey = 'xkPhL2ujvmMNXomVbyyG3Ct6'

    statPlatform = 'mrmall'
    statVersion = '1.0'
    statChannel = 'weixin'
    statUrl = 'https://khavjyad.api.lncld.net/1.1/stats/open/collect'

    devSite = 'dev.zhixianshi.com'

    public userId: string// = "5a141bdb8d6d810063352be5"

    public wechatApi: wechatApi

    private _loadInfo: loadInfoValidation
    startNavigationId: string
    startAngle: number

    LOGIN_WECHAT_APPID = 'wx662983d9ddf46ebf';

    startTime: number

    private constructor() {
        this.startTime = Date.now()
        if (commonUtil.getQueryParameter('vr') === '') {
            this.isVrMode = false
        } else {
            this.isVrMode = true
        }

        let env = commonUtil.getQueryParameter('environment')
        if (env) {
            this.apiSite = location.protocol + '//' + env + '.zhixianshi.com/'
        } else {
            this.apiSite = location.protocol + '//api.zhixianshi.com/'
        }

        if (commonUtil.getQueryParameter('roamId') !== '') {
            this.roamId = commonUtil.getQueryParameter('roamId')
        }
        this.individuation.toChange(this.roamId)

        if (commonUtil.getQueryParameter('choice') !== '') {
            this.videoChosen = parseInt(commonUtil.getQueryParameter('choice'))
        }

        if (commonUtil.getQueryParameter('located') !== '') {
            this.startNavigationId = commonUtil.getQueryParameter('located')
        }

        if (commonUtil.getQueryParameter('longitude') !== '') {
            this.startAngle = Number(commonUtil.getQueryParameter('longitude'))
        }

        this.getUser();

        // 如果只传优惠券的情况
        if (commonUtil.getQueryParameter('cardId') !== '') {
        }

        this.write(window.location.search)

    }

    private getUser() {
        if (commonUtil.getQueryParameter('userId') !== '') {
            this.userId = commonUtil.getQueryParameter('userId');
        }
        if (commonUtil.getQueryParameter('objectId') !== '') {
            this.userId = commonUtil.getQueryParameter('objectId');
        }
    }

    set loadInfo(info: loadInfoValidation) {
        this._loadInfo = info
    }

    get loadInfo() {
        if (Object.keys(this._loadInfo).length === 0) {
            console.log(Error('loadInfo invalid'))
        }
        return this._loadInfo
    }

    get defaultFov(): number {
        if (!this._loadInfo.welcomeAnimation) {
            return 110
        }
        return this._loadInfo.welcomeAnimation.defaultFov ? this._loadInfo.welcomeAnimation.defaultFov : 110
    }

    get isEntrance(): boolean {
        if (projectConfig.loadInfo.welcomeAnimation &&
            projectConfig.loadInfo.welcomeAnimation.trigger &&
            projectConfig.loadInfo.welcomeAnimation.entrance
        ) {
            return true
        }
        return false
    }


    write(string) {
        const debugBlock = document.querySelector('.debug-block');

        debugBlock.insertAdjacentHTML('beforeend', string);

    }
}

let projectConfig = projectConfigFunc.Instance

export { projectConfig };
