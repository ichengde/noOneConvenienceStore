import './resource/display.css'
import './resource/animate.css'


import { projectConfig } from './config'
import { ui } from './control/ui/uiControl'
import { materialType, videoQuality, displayType, tagNavigation, moveStatusType, guiderType } from './control/util/type'
import { request } from './control/base/requestClient'
import { audio } from './control/base/audio'
import { status } from './control/base/roamStatus'
import { management, searchBar, processBar, walkAway, navigationOperate } from './control/base/roamManagement'
import { commonUtil } from './control/util/util'
import { survey } from './control/base/survey'
import { triggerEntrance } from './control/ui/triggerEntrance'
import { wechatApi } from './control/plug/wechatShare'
import * as PathGen from 'path-gen'
import { pano, panoCore } from './control/pano/core'
import { stat } from './control/util/stat'


declare var InWalkPlayer
declare var worldRenderer


class InWalkM {
    private static _instance: InWalkM
    public static get instance() {
        if (!InWalkM._instance) {
            InWalkM._instance = new InWalkM()
        }
        return InWalkM._instance
    }

    EnterAnimation: boolean = true
    trigger: HTMLDivElement = <HTMLDivElement>document.querySelector('.trigger-image')
    loadingImage: HTMLDivElement = <HTMLDivElement>document.querySelector('#loading-img')
    triggerEntrance: triggerEntrance

    start() {
        stat.sendEventStat('web.load')
        ui.showOrHideWhiteLoading(true)

        this.loadingImage.classList.add('rotate')

        request.info().then((info) => {
            projectConfig.loadInfo = info

            ui.updateProgress(20)
            return request.detail()
        }).then(() => {
            const load = projectConfig.loadInfo

            load.welcomeAnimation = projectConfig.debug.entranceChange(load.welcomeAnimation)
            this.triggerEntrance = new triggerEntrance(load.welcomeAnimation)

            this.pathGenInit()

            ui.updateProgress(30)
            return management.getHotspot()
        }).then(() => {
            this.surveyInit()
            ui.updateProgress(50)
            return management.getHotspotGroup()
        }).then(() => {

            ui.updateProgress(60)
            management.searchBar = new searchBar()
            management.processBar = new processBar()
            management.navigationOperate = new navigationOperate()

            projectConfig.wechatApi = new wechatApi(false)

            ui.updateProgress(70)
            return projectConfig.wechatApi.getShareTemplate()
        }).then(() => {
            ui.updateProgress(80)

            let pic: string[][]

            pic = [[management.getHotspotSnapshot(status.originHotspot)]]

            if (projectConfig.loadInfo.welcomeAnimation && projectConfig.loadInfo.welcomeAnimation.entrance) {
                pic = [projectConfig.loadInfo.welcomeAnimation.entrance]
            }

            const startNavigation: tagNavigation = status.originHotspot
            if (status.defaultNavigation.id !== status.originHotspot.id) {
                projectConfig.wechatApi.setNavigationInfo(moveStatusType.navigation, startNavigation)
            } else {
                projectConfig.wechatApi.setNavigationInfo(moveStatusType.portal)
            }

            const angle = projectConfig.startAngle || startNavigation.info.angle


            status.option = {
                parent: 'vr-panel',
                source: pic,
                publicKey: projectConfig.publicKey,
                showBottomMark: true,
                autoLoad: false,
                bottomMark: management.survey.logoOverlay,
                defaultYaw: angle || 0
            }
            management.vr = new InWalkPlayer(status.option)

            ui.iOSadaptation()

            ui.updateProgress(90)
            return request.preloadPano()
        }).then(() => {
            management.vr.on('canplay', this.panoramaImageLoadingIsComplete.bind(this))
            management.vr.load()

            status.displayMode = displayType.panoramaPicture;

            let duration = Date.now() - projectConfig.startTime
            stat.sendEventStat('web.ready', {duration:duration/1000.0})
        })
    }

    panoramaImageLoadingIsComplete() {

        if (this.EnterAnimation === true) {
            // canplay以后才能禁用陀螺仪 注意触发位置

            if (projectConfig.isEntrance) {

                management.vr.on('animationComplete', () => {
                    this.trigger.style.zIndex = '1001'
                    this.trigger.style.display = 'block'
                    // ui.showTouchTips()
                })

                this.triggerEntrance.animate(projectConfig.loadInfo.welcomeAnimation.animation)

            } else {
                this.triggerImageEnterAfter()
            }


            ui.showOrHideWhiteLoading(false)
            this.loadingImage.remove()
            // worldRenderer.camera.fov = 90
            // worldRenderer.camera.updateProjectionMatrix()
            management.beginnerGuider.checkAndGuidance(guiderType.firstScreenGuide)

            this.EnterAnimation = false
        }

    }


    triggerImageEnterAfter(enterHotspot?: string) {
        let navigation = enterHotspot ? status.hotspotById(enterHotspot) : status.originHotspot

        status.angleOfReloadPano = JSON.parse(navigation.position).angle //  projectConfig.shareInstance.longitude ||
        status.currentHotspot = navigation

        this.trigger.style.display = 'none'
        // ui.showTouchTips()

        panoCore.coupon.fetchCoupon().then(() => {
            pano.showOverlay(navigation.id)
        })
    }

    surveyInit() {
        const detail = projectConfig.detailInFo.detail
        const load = projectConfig.loadInfo
        management.survey = new survey(detail.meta, status.roamHotspot.hotspots, load.videoVersion)
    }

    pathGenInit() {
        const load = projectConfig.loadInfo
        const detail = projectConfig.detailInFo.detail
        management.pathGen = new PathGen(detail.meta, load.uri, detail.angleSpeed, load.videoVersion, detail.navigatorStepLen)
    }


}

window.onload = function () {
    let main = new InWalkM()

    main.start()
}
