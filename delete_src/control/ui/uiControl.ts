import { commonUtil as util } from '../util/util'

class uiContorl {
    private static _instance: uiContorl
    public static get instance() {
        if (!uiContorl._instance) {
            uiContorl._instance = new uiContorl()
        }
        return uiContorl._instance
    }

    setShowOrHide(name, flag, zIndex?) {
        let doms = document.querySelectorAll(name)

        for (let d = 0; d < doms.length; d++) {

            if (flag === true && doms[d]) {
                doms[d].style.display = 'block'
                doms[d].style.zIndex = zIndex || '1001'
            } else {
                doms[d].style.display = 'none'
                doms[d].style.zIndex = zIndex || '0'
            }
        }
    }

    hideBtnGroup() {
        this.setShowOrHide('#angleControlButton', false)
        this.setShowOrHide('#VRmodeControlButton', false)
        this.setShowOrHide('#playPauseControlButton', false)
        this.setShowOrHide('#stopControlButton', false)
    }

    showOrHidePlayButton(flag) {
        if (flag === true) {
            this.setShowOrHide('#playPauseControlButton', true)
        } else {
            this.setShowOrHide('#playPauseControlButton', false)
        }
    }

    showOrHideStopButton(flag) {
        if (flag === true) {
            this.setShowOrHide('#stopControlButton', true)
        } else {
            this.setShowOrHide('#stopControlButton', false)
        }
    }

    showBtnGroup() { // play
        let angleControlButton = <HTMLDivElement>document.querySelector('#angleControlButton')
        angleControlButton.style.display = 'none'

        let VRmodeControlButton = <HTMLDivElement>document.querySelector('#angleControlButton')
        VRmodeControlButton.style.display = 'none'

        let controlBar = <HTMLDivElement>document.querySelector('#controlBar')
        controlBar.style.left = '0px'
        controlBar.style.bottom = '58%'
        controlBar.style.marginLeft = '0px'
        controlBar.style.marginTop = '0px'

        let playPauseControlButton = <HTMLDivElement>document.querySelector('#playPauseControlButton')
        playPauseControlButton.style.display = 'block'

        let stopControlButton = <HTMLDivElement>document.querySelector('#stopControlButton')
        stopControlButton.style.display = 'block'
    }

    showOrHideWhiteLoading(flag) {
        if (flag) {
            this.setShowOrHide('#loading-bg', flag, 2000)
            this.setShowOrHide('#loading-img', flag, 2001)
            this.setShowOrHide('#loading-text', flag, 2005)
        } else {
            this.setShowOrHide('#loading-bg', flag, 0)
            this.setShowOrHide('#loading-img', flag, 0)
            this.setShowOrHide('#loading-text', flag, 0)
        }
    }
    showOrHideLoadingTips(flag) {
        this.setShowOrHide('#loading-tips', flag)
    }

    isHideForSelectRoamPanel() {
        let freeRoamPannel = <HTMLDivElement>document.querySelector('.free-roam-panel')
        if (freeRoamPannel.style.display === 'none') {
            return true
        } else {
            return false
        }
    }

    ShowOrHideRoamSelectSwitch(flag) {
        this.setShowOrHide('.free-roam-switch', flag)
        this.setShowOrHide('.first-floor', flag)
        this.setShowOrHide('.third-floor', flag)
    }

    foldAllSearchBar(): void {
        let barList = <any>document.querySelectorAll('.roam-select-list')
        barList.forEach((list) => {
            this.foldSearchBar(list)
        })
    }

    foldSearchBar(list: HTMLDivElement): void {
        // let list = <any>document.querySelector('#list-' + selectListId)
        list.style.display = 'none'
        list.parentElement.style.paddingBottom = '0px'
    }

    unfoldSearchBar(list: HTMLDivElement): void {
        list.style.display = 'block'
        list.parentElement.style.paddingBottom = '7.5px'
    }

    unselectedSearchBar() {
        const all = document.querySelectorAll('.group-bar')
        for (let i = 0; i < all.length; i++) {
            const bar = <HTMLDivElement>all[i]
            bar.style.height = ''
            bar.style.background = ''
            bar.style.backgroundSize = ''
            bar.style.color = '#505050'
            bar.style.width = ''
        }
    }

    selectedSearchBar(bar) {
        this.unselectedSearchBar()

        bar.style.color = '#ff4257'
        bar.style.background = `url('./images/group_selected.png')`
        bar.style.backgroundSize = `100% 100%`
        bar.style.backgroundPositionY = `bottom`
    }

    showTouchTips() {
        this.setShowOrHide('#touchTips', true)
        setTimeout(() => {
            this.setShowOrHide('#touchTips', false)
        }, 2000)
    }

    showOrHideFlatVideo(flag, z?) {
        let dom = <HTMLVideoElement>document.querySelector('#testVideo')
        if (flag === true) {
            if (z !== undefined) {
                dom.style.zIndex = z
            } else {
                dom.style.zIndex = '1000'
            }
            dom.style.display = 'block'
        } else {
            dom.style.zIndex = '0'
            if (util.isIOS() === true) {
                dom.style.display = 'none'
            }
        }
    }

    showOrHideWideVideoMaterialUI(flag) {
        this.setShowOrHide('#materialBg', flag)
        this.setShowOrHide('#materialClose', flag, 2001)
    }

    updateProgress(num) {
        let dom = document.querySelector('#loading-process')
        dom.innerHTML = num + '%'
    }

    iOSadaptation() {
        let rightTopContainer = <HTMLDivElement>document.querySelector('.rightTop-container')
        let materialClose = <HTMLDivElement>document.querySelector('#materialClose')
        if (util.isIOS() === true) {
            rightTopContainer.style.marginTop = '0px'
            materialClose.style.top = '0px'
            materialClose.style.marginTop = '0px'
        }
    }

    showOrHideOperate(flag?, dom?: 'next' | 'previous') {
        const enterDom = <HTMLDivElement>document.querySelector('.snap-enter')
        const returnDom = <HTMLDivElement>document.querySelector('.snap-return')
        if (dom === undefined) {
            if (flag) {
                enterDom.style.display = 'block'
                returnDom.style.display = 'block'
            } else {
                enterDom.style.display = 'none'
                returnDom.style.display = 'none'
            }
        }

        if (dom === 'next') {
            if (flag) {
                enterDom.style.display = 'block'
            } else {
                enterDom.style.display = 'none'
            }
        }

        if (dom === 'previous') {
            if (flag) {
                returnDom.style.display = 'block'
            } else {
                returnDom.style.display = 'none'
            }
        }
    }

}

let ui = new uiContorl
export { ui }

