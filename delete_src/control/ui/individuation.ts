export class individuation {

    toChange(roamId) {

        switch (roamId) {
            case '59ae545da0bb9f006450f647':
                this.common()
                break
            case '599121bf8d6d810058a3464e':
                this.qianhaiguoshui()
                break
            default:
                this.common()
        }
    }

    private qianhaiguoshui() {

        let input = <any>document.querySelector('.roam-select-input-block')
        input.style.display = 'none'

        let b = <any>document.querySelector('.search-info-container')
        b.style.marginTop = '20px'

    }

    private tianhong() {
        let sp = <any>document.querySelectorAll('.special-choice')
        sp.forEach(element => {
            element.style.opacity = '0'
        })
        
        let logo = <any>document.querySelector('#logo')
        logo.style.opacity = '0'
    }


    private common() {
        let sp = <any>document.querySelectorAll('.special-choice')
        if (sp) {
            for (let i = 0; i < sp.length; i++) {
                const element = sp[i];
                element.style.opacity = '0'

            }
        }


        const dontDomArr = ['#logo' , '#playPauseControlButton']
        let dontDom = []
        if (dontDom.length > 0) {

            dontDomArr.forEach((name) => {
                dontDom.push(document.querySelector(name))
            })

            for (let i = 0; i < dontDom.length; i++) {
                const element = dontDom[i];
                // element.style.opacity = '0'
                element.addEventListener('touchmove', (e) => {
                    e.preventDefault()
                })
            }
        }


    }

}