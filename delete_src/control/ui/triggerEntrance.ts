
import { welcomeAnimationValidation, triggerValidation, triggerButtonValidation } from '../util/type'

declare var worldRenderer
export class triggerEntrance {

    triggerContainer: HTMLDivElement = <HTMLDivElement>document.querySelector('.trigger-image')
    constructor(welcomeInfo: welcomeAnimationValidation) {
        let triggerInfo = welcomeInfo.trigger

        if (triggerInfo) {
            triggerInfo.background && this.addBackground(triggerInfo.background.src, triggerInfo.background.style)

            triggerInfo.button && triggerInfo.button.forEach((b) => {
                this.addButton(b)
            })
        }
    }

    addBackground(src, style) {
        let backgroundTemplate = `
            <img src="${src}" style="${style}"/>
        `
        this.triggerContainer.insertAdjacentHTML('beforeend', backgroundTemplate)
    }

    addButton(button: triggerButtonValidation) {
        let buttonTemplate = `
            <img class="enter-choice" data-id="${button.hotspotId}" src="${button.src}" style="${button.style}"/>
        `
        this.triggerContainer.insertAdjacentHTML('beforeend', buttonTemplate)
    }

    animate(params): void {
        let animation = params
        if (animation) {
            switch (animation.type) {
                case 0:
                    worldRenderer.startEnterAnimation1(animation.duration, animation.endFov, animation.startFov, animation.entranceAngle)
                    break
                case 1:
                    worldRenderer.startEnterAnimation3(animation.duration, animation.endFov, animation.startFov, animation.entranceAngle)
                    break
                default:
                    worldRenderer.startEnterAnimation3(3000, 80, 150, 0)
            }

        } else {
            console.warn('animation unset')
        }
    }
}
