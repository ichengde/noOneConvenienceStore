import { projectConfig } from '../../config'
import { materialVideoStyleType, materialType, displayType, transitionValidation } from '../util/type'
import * as enableInlineVideo from 'iphone-inline-video'
import { ui } from '../ui/uiControl'
import { management, currentTransitionValidation } from '../base/roamManagement'
import { overlayRender } from '../base/overlayRender'
import { commonUtil, transitionUtil, materialVideoUtil } from '../util/util'
import { status } from '../base/roamStatus'
import { audio } from '../base/audio'
import * as Cookies from "js-cookie"

declare var worldRenderer

class flatCore {
    private static _instance: flatCore
    public static get instance() {
        if (!flatCore._instance) {
            flatCore._instance = new flatCore()
        }
        return flatCore._instance
    }

    private flatVideo: HTMLVideoElement
    private timer: number
    private needSeekTime: number
    private needAngle: number
    private toHideVideoBlock: boolean

    iosOrientation: Function

    private firstPlayMaterialVideoFlag: boolean = true

    public shouldRenderVideoMaterialAfterReloadPanoCallbackFunction: Function
    public materialVideoCallbackFunction: Function
    public canStashLookAt: boolean = false

    constructor() {
        this.flatVideo = this.createFlatVideo()

        this.timer = setInterval(() => {
            this.movingReachEnd()
            this.checkWalkVideo()
            this.checkMaterialVideoAfterPano()
            this.stashLngLat();
        }, 10)


        this.androidSeekEvent()
        this.flatVideo.addEventListener('pause', () => {
            // 照片模式 从planeWalkingVideo 到载入全景图
            if (this.flatVideo.currentTime &&
                status.displayMode === displayType.planeWalkingVideo) {
                // 计算暂停时候的时间
                status.displayMode = displayType.panoramaPicture
                management.searchBar.enable()
                ui.showOrHideLoadingTips(true)
                console.log('currentTime', this.flatVideo.currentTime, ' stoptime', status.stopTime)

                status.option.source = [this.getPictureInFlat()]
                management.vr.reload(status.option)
                // management.vr.resumeRender()
                this.toHideVideoBlock = true
            }

            // 播放素材结束
            if ((status.displayMode === displayType.materialVideo &&
                (Math.abs(this.flatVideo.duration - this.flatVideo.currentTime) < 1))) {
                this.materialVideoEnd()
            }
        })

    }

    private getHotspotSnapshot(hotspot) {
        let pic: string
        if (hotspot.snapshot) {
            pic = hotspot.snapshot
        } else {
            let info = JSON.parse(hotspot.position)
            pic = projectConfig.debug.picChange(management.pathGen.getSnapshot(info.videoId, info.timeoffset))
        }
        return pic
    }

    go() {
        management.removeAllOverlays()

        if (status.isPressedBlinkButton === true) {
            ui.showOrHideLoadingTips(true)
            ui.ShowOrHideRoamSelectSwitch(false)

            // 计算 目标点的时间
            let position = JSON.parse(status.endHotspot.position)
            let weTime = position.timeoffset

            status.option.source = [[this.getHotspotSnapshot(status.endHotspot)]]


            // 为了提前判断是否有素材
            let material = management.getMaterial(status.endHotspot.id)
            if (materialVideoUtil.isHasMaterialVideoInMaterials(weTime, material) && !status.skipMaterialVideo) {
                // 进入交互视频模式

                status.targetHotspot = status.endHotspot
                status.angleOfReloadPano = JSON.parse(status.targetHotspot.position).angle

                this.shouldRenderVideoMaterialAfterReloadPanoCallbackFunction = function () {
                    status.option.source = [this.getHotspotSnapshot(status.endHotspot)]

                    management.vr.reload(status.option)

                }
                status.isShouldRenderVideoMaterialAfterReloadPano = true
            } else {
                management.vr.reload(status.option)
                // management.vr.resumeRender()
                this.toHideVideoBlock = true
                // 瞬移时候的播放处理 直接获取目标点的图片

                management.processBar.hide()

                ui.ShowOrHideRoamSelectSwitch(true)
            }
        } else {
            if (status.transitionSeekTime) {
                // 处于transition->hotspot
                this.needSeekTime = status.transitionSeekTime
            } else {
                // 处于hotspot->hotspot
                this.needSeekTime = transitionUtil.calcTransitionTimeForFlat(status.path.transition, 0, status.currentHotspot)
            }

            this.flatVideo.src = status.path.video[projectConfig.videoChosen]
            this.seek(this.needSeekTime)
            this.flatVideo.load()

            this.rotateAndPlay()
        }
    }

    rotateAndPlay() {
        console.warn('play function')
        console.group('angle')

        let cTransition = this.cTranAndPos.transition
        let ctPos = this.cTranAndPos.pos
        status.transitionSeekTime = this.flatVideo.currentTime - cTransition.time

        // 在路上转角处的角度旋转
        let isTailTransition = ctPos + 1 === status.path.transition.length

        let endAngle = this.cTranAndPos.transition.offset

        if (!isTailTransition) {
            endAngle = transitionUtil.calcTransitionAngle(status.path.transition, ctPos, status.transitionSeekTime, management.survey, projectConfig.detailInFo.detail.angleRatio)
        }


        // 最终的角度旋转
        let startAngle = management.vr.getYaw()

        console.log('播放,播放前旋转,开始角度:', startAngle, '结束角度', endAngle)
        this.mySetYaw(startAngle, endAngle, () => {
            this.makeItPlay(false)

            ui.showOrHideLoadingTips(true)
            management.processBar.show()
        })
        console.groupEnd()
    }

    private androidSeekTime: number

    androidSeekEvent() {
        if (!commonUtil.isIOS()) {
            this.flatVideo.addEventListener('loadeddata', () => {
                if (this.androidSeekTime !== undefined) {
                    this.flatVideo.currentTime = this.androidSeekTime
                    this.androidSeekTime = undefined
                }
            })
        }
    }

    seek(time: number) {
        if (commonUtil.isIOS() === true) {
            this.flatVideo.src += '#t=' + time.toString()
        } else {
            this.androidSeekTime = time
        }
        console.log('此时进行seek', this.androidSeekTime)
    }

    bindLoadPanoImageAfter() {
        let firstLoad = true
        management.vr.on('canplay', () => {
            if (firstLoad === true) {
                management.vr._getVideo().loop = false
                firstLoad = false
            }

            if (this.toHideVideoBlock === true &&
                status.displayMode === displayType.panoramaPicture) {
                ui.showOrHideFlatVideo(false)
                this.pictureAngle()
                this.toHideVideoBlock = false
            }

            // 入场时 特殊处理

            if (status.angleOfReloadPano !== undefined) {
                management.vr.setYaw(status.angleOfReloadPano)
                management.reachHotspot()
                status.angleOfReloadPano = undefined
            }
        })
    }

    createFlatVideo(): HTMLVideoElement {
        /*    let flat = document.createElement('video')
           flat.id = 'testVideo'
           flat.loop = false
           // Enable inline video playback in iOS 10+.
           flat.setAttribute('playsinline', 'true')
           flat.setAttribute('crossorigin', 'anonymous')
           // Enable inline video playback in Tencent x5.
           flat.setAttribute('x5-video-player-type', 'h5')
           flat.setAttribute('x5-video-player-fullscreen', 'true')
           flat.setAttribute('x5-video-orientation', 'portrait') */

        const flat = <HTMLVideoElement>document.querySelector('#testVideo')

        enableInlineVideo(flat)
        flat.addEventListener('x5videoexitfullscreen', function () {
            // window.location.href = util.updateUrl(window.location.href, 'time')
            worldRenderer.onResize_()
        })

        /* 
                flat.setAttribute('playbackRate', '4.0')
                flat.defaultPlaybackRate = 4.0 
        */

        document.body.appendChild(flat)


        flat.addEventListener('touchmove', function (event) {
            event.preventDefault()
        }, false);

        return flat
    }

    checkWalkVideo() {
        if (this.flatVideo.currentTime > 0 &&
            this.flatVideo.paused === false &&
            this.shouldRenderVideoMaterialAfterReloadPanoCallbackFunction === undefined &&
            (this.flatVideo.style.zIndex === '0' || this.flatVideo.style.zIndex === undefined || this.flatVideo.style.zIndex === '')) {
            // 播放行走视频
            if (this.needSeekTime !== undefined &&
                status.displayMode === displayType.planeWalkingVideo &&
                this.flatVideo.currentTime > this.needSeekTime + 0.08) {
                console.log('seeked 以后 显示的播放')


                ui.showOrHideFlatVideo(true)
                ui.showOrHideLoadingTips(false)
                // management.vr.pausepauseRender()
                console.log('now all !! currentTime', this.flatVideo.currentTime, ' need:', this.needSeekTime)


                this.needSeekTime = undefined
                status.transitionSeekTime = undefined
            } else {
                if (this.needSeekTime === undefined &&
                    this.flatVideo.currentTime > 0.1) {
                    if (status.pressBarPlay === true) {
                        console.log('按下play按钮,播放视频素材时让其浮现')

                        status.pressBarPlay = false
                        ui.showOrHideFlatVideo(true)
                        ui.showOrHideLoadingTips(false)
                    } else {
                        console.log('从0 播放视频素材时让其浮现')
                        ui.showOrHideFlatVideo(true, 2000)
                        ui.showOrHideLoadingTips(false)
                    }
                    // management.vr.pauseRender()
                }
            }
        }
    }

    checkMaterialVideoAfterPano() {
        // 先渲染视频浮层再载入全景图的特殊处理
        if (status.displayMode === displayType.materialVideo &&
            this.flatVideo.currentTime > 0.2 &&
            this.shouldRenderVideoMaterialAfterReloadPanoCallbackFunction) {
            console.log('特殊视频素材开始播放 变形')

            ui.showOrHideFlatVideo(true, 1003)

            // management.vr.pauseRender()
            ui.showOrHideLoadingTips(false)
            ui.showOrHideWhiteLoading(false)
            if (this.shouldRenderVideoMaterialAfterReloadPanoCallbackFunction) {
                this.shouldRenderVideoMaterialAfterReloadPanoCallbackFunction()
                this.shouldRenderVideoMaterialAfterReloadPanoCallbackFunction = undefined
            }
        }

        if (this.shouldRenderVideoMaterialAfterReloadPanoCallbackFunction &&
            status.isShouldRenderVideoMaterialAfterReloadPano === true) {
            this.showOverlay(status.targetHotspot.id)
            status.isShouldRenderVideoMaterialAfterReloadPano = false

            if (!projectConfig.isEntrance) {
                // 如果开头不载入入场动画
                this.shouldRenderVideoMaterialAfterReloadPanoCallbackFunction = undefined
            }

        }
    }

    showOverlay(id: string, renderMaterial?: Array<any>) {
        let material = management.getMaterial(id)

        let that = this
        overlayRender.setMaterials(material, JSON.parse(status.hotspotById(id).position).timeoffset)

        let momentMaterial = renderMaterial ? renderMaterial : material

        // check video material
        let weTime = JSON.parse(status.hotspotById(id).position).timeoffset
        if (materialVideoUtil.isHasMaterialVideoInMaterials(weTime, momentMaterial)) {

            if (!status.skipMaterialVideo) {
                overlayRender.render(materialVideoUtil.video)
            } else {
                momentMaterial.forEach((m) => {
                    if (m.type === materialType.VIDEO_MATERIAL) {
                        overlayRender.appendChild(m)
                        console.log(m.des)
                        if (m.des) {
                            m.des.forEach(e => {
                                overlayRender.render(e)
                            });
                        }
                    }
                    // audio play
                })
            }

        } else {
            momentMaterial.forEach((m) => {

                if (m.type !== materialType.VIDEO_MATERIAL) {
                    overlayRender.render(m)
                }
            })
        }
        // !status.skipMaterialVideo
        console.groupEnd()
    }

    materialVideoEnd() {

        ui.showOrHideWideVideoMaterialUI(false)

        if (commonUtil.isIOS() === true) {
            this.flatVideo.setAttribute('playsinline', 'true')
        }

        this.flatVideo.pause()
        ui.showOrHideFlatVideo(false)
        // management.vr.resumeRender()

        if (this.materialVideoCallbackFunction !== undefined) {
            this.materialVideoCallbackFunction()
            this.materialVideoCallbackFunction = undefined
        }

        // 恢复默认 => checkMaterialVideoAfterPano
        this.makeCenterFull()

        status.displayMode = displayType.panoramaPicture
        /*
            this.inPicture = true
            this.isPlayMaterialVideo = false
        */

    }

    mySetYaw(startAngle, endAngle, callback) {
        startAngle = parseInt(startAngle) % 360 // this.player.getYaw()
        endAngle = parseInt(endAngle) % 360
        if (startAngle < 0) startAngle += 360
        if (endAngle < 0) endAngle += 360

        var direction = false
        if (endAngle > startAngle) direction = true

        var duration = Math.abs(startAngle - endAngle) / 45 * 1000

        if (Math.abs(startAngle - endAngle) > 180) {
            direction = !direction

            duration = (360 - Math.abs(startAngle - endAngle)) / 45 * 1000
        }
        var type = 0
        // var duration = 3000

        if (Math.abs(startAngle - endAngle) > 2) {
            this.changeYaw_(startAngle, endAngle, direction ? 1 : 0, duration, type, function () {
                callback && callback()
            })
        } else {
            management.vr.setYaw(startAngle, function () {
                callback && callback()
            })
        }
    }

    changeYaw_(start, end, direction, duration, type, callback) {
        var distance = transitionUtil.angleOf(start, end)
        management.vr.setYaw(start, () => {
            if (distance <= 1) {
                if (callback) {
                    setTimeout(function () {
                        callback()
                    }, duration)
                }

                return
            }

            management.vr.setYaw(end, direction ? 1 : 0, duration, type, () => {
                callback && callback()
            })
        })
    }

    movingReachEnd() {
        if (this.flatVideo.currentTime >= status.stopTime &&
            status.stopTime !== 0 &&
            status.displayMode === displayType.planeWalkingVideo) {

            if (status.theRestOfTheBusiness && !status.theRestOfTheBusiness.decision) {
                console.log('%c  没有接下来的路了', ' color: red')
            }

            this.needAngle = JSON.parse(status.endHotspot.position).angle
            management.processBar.hide()
            this.flatVideo.pause()
            // management.vr.resumeRender()
        }
    }

    playMaterialVideo(src, scaleMode, callbackFunction) {
        let pos = src.lastIndexOf('/')
        let nSrc = src.slice(0, pos) + '/flatten' + src.slice(pos)

        this.flatVideo.src = nSrc
        this.flatVideo.load()

        this.flatVideo.play()

        let bg = <HTMLElement>document.querySelector('#materialBg')
        if (scaleMode === materialVideoStyleType.wideScreen) {
            bg.style.display = 'block'
            this.makeWideWidth()
            ui.showOrHideWideVideoMaterialUI(true)
            let closeBtn = document.querySelector('#materialClose')
            closeBtn.addEventListener('click', this.materialVideoEnd.bind(this))

            if (commonUtil.isIOS() === true) {
                let that = this
                window.addEventListener('orientationchange', (e) => {
                    if (window.orientation === -90 || window.orientation === 90) {
                        that.flatVideo.style.top = '0px'
                        that.flatVideo.style.width = 'auto'
                        that.flatVideo.style.height = '100%'
                        // center video
                        that.flatVideo.style.left = '20%'
                        // that.flatVideo.style.marginLeft = '-' + that.flatVideo.offsetWidth / 2 + 'px'
                    }
                    if (window.orientation === 0) {
                        that.flatVideo.style.top = '105px'
                        that.flatVideo.style.height = 'auto'
                        that.flatVideo.style.width = '100%'
                        // relocation video
                        that.flatVideo.style.left = '0px'
                        that.flatVideo.style.marginLeft = '0px'
                    }
                })
            }
        }

        if (scaleMode === materialVideoStyleType.fullScreenCenter ||
            scaleMode === undefined) {

            this.makeCenterFull()
            ui.showOrHideWideVideoMaterialUI(false)
        }

        status.displayMode = displayType.materialVideo
        status.stopTime = undefined
        if (this.firstPlayMaterialVideoFlag === true) {
            ui.showOrHideWhiteLoading(true)
            this.firstPlayMaterialVideoFlag = false
        }
        if (callbackFunction && typeof callbackFunction === 'function') {
            this.materialVideoCallbackFunction = callbackFunction
        }
    }

    makeCenterFull() {
        this.flatVideo.style.width = '100%'
        this.flatVideo.style.transform = 'translateX(-50%) translateY(-50%)'
        this.flatVideo.style.height = 'auto'
        this.flatVideo.style.minHeight = '100%'
        this.flatVideo.style.minWidth = '100%'
        this.flatVideo.style.left = '50%'
        this.flatVideo.style.top = '50%'
    }

    makeWideWidth() {
        this.flatVideo.style.width = '100%'
        this.flatVideo.style.transform = 'translateX(0%) translateY(0%)'
        this.flatVideo.style.height = 'auto'
        this.flatVideo.style.minHeight = '150px'
        this.flatVideo.style.minWidth = '300px'
        this.flatVideo.style.left = '0px'
        this.flatVideo.style.top = '105px'
        if (commonUtil.isIOS() === true) {
            this.flatVideo.setAttribute('playsinline', 'false')
        }
    }

    getPictureInFlat() {
        let currentTime = this.flatVideo.currentTime
        let time = 0

        if (this.cTranAndPos.transition.reverse === true) {
            time = this.cTranAndPos.transition.duration - (currentTime - this.cTranAndPos.transition.time)
        } else {
            time = currentTime - this.cTranAndPos.transition.time
        }
        return projectConfig.debug.picChange(management.pathGen.getSnapshot(this.cTranAndPos.transition.videoId, time))
    }

    private pictureAngle() {
        // 图片载入以后 转角度结束的时候
        let that = this
        let position = JSON.parse(status.endHotspot.position)
        let endAngle = position.angle

        ui.showOrHideLoadingTips(false)

        if (status.isPressedBlinkButton === true) {
            // 跳转到达
            management.vr.setYaw(endAngle, function () {
                console.log('isPressedBlinkButton,角度:', endAngle)

                that.needSeekTime = undefined
                status.transitionSeekTime = undefined
                that.showOverlay(status.endHotspot.id)

                management.reachHotspot()

                status.stopTime = 0
            })
        } else {
            let angleOfTurning
            let cTransition = this.cTranAndPos.transition
            let ctPos = this.cTranAndPos.pos

            if (that.needAngle ||
                (status.stopTime !== 0 &&
                    that.flatVideo.currentTime >= status.stopTime) ||
                (status.stopTime >= that.flatVideo.duration &&
                    Math.abs(that.flatVideo.duration - that.flatVideo.currentTime) < 0.1)) {
                // 步行达到

                status.transitionSeekTime = that.flatVideo.currentTime - cTransition.time

                angleOfTurning = transitionUtil.calcTransitionAngle(status.path.transition, ctPos, status.transitionSeekTime, management.survey, projectConfig.detailInFo.detail.angleRatio)

                let startAngle = angleOfTurning !== undefined ? angleOfTurning : management.vr.getYaw()

                console.log('到达hotspot,开始角度:', startAngle)

                this.mySetYaw(startAngle, endAngle, function () {
                    console.log('旋转结束', endAngle)

                    that.needAngle = undefined
                    that.needSeekTime = undefined
                    status.transitionSeekTime = undefined

                    management.reachHotspot()
                    that.showOverlay(status.endHotspot.id)

                    status.stopTime = 0
                })
            } else {
                // 在路上转角处的角度旋转

                status.transitionSeekTime = that.flatVideo.currentTime - cTransition.time
                angleOfTurning = transitionUtil.calcTransitionAngle(status.path.transition, ctPos, status.transitionSeekTime, management.survey, projectConfig.detailInFo.detail.angleRatio)

                if (angleOfTurning !== undefined) {
                    management.vr.setYaw(angleOfTurning, () => { })
                } else {
                    // 在路上直线角度回正
                    console.log('在路上直线角度回正,到达transition,前进角度:', cTransition.offset)
                    management.vr.setYaw(cTransition.offset, () => {
                    })
                }
            }
        }
    }

    get cTranAndPos(): currentTransitionValidation {
        let currentTime = this.flatVideo.currentTime
        let trst
        let pos: number = -1
        for (trst of status.path.transition) {
            if (currentTime >= trst.time) {
                pos++
            }
        }

        return {
            transition: status.path.transition[pos],
            pos: pos
        }
    }


    makeItPlay(pressBarPlay?: boolean) {
        status.pressBarPlay = pressBarPlay
        status.playing = true
        status.displayMode = displayType.planeWalkingVideo
        management.searchBar.disable()
        this.flatVideo.play()

        management.processBar.update()
    }

    makeItPause() {
        status.playing = false
        status.prevVideoTransition = this.cTranAndPos.transition
        management.searchBar.enable()
        this.flatVideo.pause()

        management.processBar.update()

    }

    stashLngLat() {
        if (this.canStashLookAt === true) {
            const longitude = management.vr.getYaw()
            const latitude = management.vr.getLatitude()
            projectConfig.wechatApi.longitude = longitude
            projectConfig.wechatApi.latitude = latitude
        }
    }
}

let flat = flatCore.instance

export { flat }