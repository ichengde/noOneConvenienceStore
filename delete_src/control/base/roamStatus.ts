import { projectConfig } from '../../config'
import { commonUtil } from '../util/util'
import * as type from '../util/type'
import * as Cookies from "js-cookie"

class roamStatus {
    private static _instance: roamStatus
    public static get instance() {
        if (!roamStatus._instance) {
            roamStatus._instance = new roamStatus()
        }
        return roamStatus._instance
    }
    public option: type.optionValidation

    /*
        操作状态
    */

    isPressedBlinkButton: boolean = false
    // 是否按下瞬移 储存是否在瞬移状态
    isPressedTheOverlay: boolean = false
    // 是否按下浮层 储存是否在业务模式
    pressBarPlay: boolean
    // 是否按下play按钮播放
    private _playing: boolean
    // 播放状态

    private _display: type.displayType
    // 目前的展示模式
    public theRestOfTheBusiness: any
    // 剩余的业务情况
    private _skipMaterialVideo: boolean
    // 业务情况


    /*
        路径
    */

    stopTime: number
    // 停止时间
    prevVideoTransition: any
    // 行进视频中的暂停片段
    private _transitionSeekTime: number
    // 保存行进视频中的暂停以后的seek时间
    originHotspot: type.hotspotValidation
    // 起始导航点
    private _path: type.pathValidation
    // 路径数据
    private _currentHotspot: type.hotspotValidation
    hotspotBeforePreview: type.hotspotValidation
    // 当前导航点
    private _endHotspot: type.hotspotValidation
    // 最终导航点
    private _recordNavigation: Array<type.hotspotValidation> = []
    // 那些记录的导航点

    /*
        先视频后图片
    */

    angleOfReloadPano: number
    // 装载成全景图所要转到的角度
    public isShouldRenderVideoMaterialAfterReloadPano: boolean = false
    targetHotspot: type.hotspotValidation

    /*
        搜索栏使用
    */

    private _roamHotspot: type.roamHotspotValidation
    private _group: type.hotspotGroupValidation

    /*
        UI
    */

    currentHotspotDisplayDom = <HTMLInputElement>document.querySelector('.free-roam-text')
    endHotspotDisplayDom = <HTMLInputElement>document.querySelector('#roam-select-input')

    defaultNavigation

    judgeStartHotspot(item) {
        if (item.start === true) {
            this.defaultNavigation = item
            return true
        } else {
            return false
        }
    }


    hotspotById(id) {
        let weNeedHotspot
        this._roamHotspot.hotspots.forEach(function (roam) {
            if (roam.id === id) {
                weNeedHotspot = roam
            }
        })
        if (weNeedHotspot) {
            return weNeedHotspot
        } else {
            console.error('error, not in an existing path to HotSpot')
        }
    }

    set roamHotspot(d: type.roamHotspotValidation) {
        d.hotspots.forEach((hotspot) => {
            if (this.judgeStartHotspot(hotspot) && !projectConfig.startNavigationId) {
                this.originHotspot = hotspot
                this.endHotspot = hotspot
            }

            if (hotspot.id === projectConfig.startNavigationId) {
                this.originHotspot = hotspot
                this.endHotspot = hotspot
            }
        })

        if (this.originHotspot === undefined) {
            this.originHotspot = this.defaultNavigation
        }

        this._roamHotspot = d
    }

    get roamHotspot(): type.roamHotspotValidation {
        return this._roamHotspot
    }

    set hotspotGroup(d: type.hotspotGroupValidation) {
        this._group = d

        if (this._roamHotspot) {
            this._group.groups = this._group.groups.map((group) => {
                group.hotspots = group.hotspots.map((hotspot) => {

                    for (var h of this._roamHotspot.hotspots) {
                        if (h.id === hotspot.hid) {
                            hotspot.sort = this.getSort(JSON.parse(h.position).sort)
                            hotspot.hide = JSON.parse(h.position).webHide === true
                            return hotspot
                        }
                    }
                })
                group.hotspots = group.hotspots.sort(function (a, b) {
                    return a.sort - b.sort
                })
                return group
            })
        }
    }

    get hotspotGroup(): type.hotspotGroupValidation {
        return this._group
    }

    private getSort(sort) {
        if (sort === undefined) {
            // console.warn(hid, 'not set sort')
            return 99
        } else {
            return sort
        }
    }

    set endHotspot(h: type.hotspotValidation) {
        console.log('update endhotspot', h.desc)
        this._endHotspot = h

        if (this._endHotspot === undefined) {
            this.endHotspotDisplayDom.value = ''
        } else {
            // this.endHotspotDisplayDom.value = this.endHotspot.desc
        }

    }

    get endHotspot(): type.hotspotValidation {
        return this._endHotspot
    }

    set currentHotspot(h: type.hotspotValidation) {
        this._currentHotspot = h


        console.warn('update currentHotspot', this.currentHotspot.desc)
    }

    get currentHotspot(): type.hotspotValidation {
        return this._currentHotspot
    }

    set displayMode(d: type.displayType) {
        console.log('current display', type.displayType[d])
        this._display = d
    }

    get displayMode(): type.displayType {
        return this._display
    }

    set path(d: any) {
        this._path = d
    }

    get path(): any {
        return this._path
    }

    set playing(s: boolean) {
        this._playing = s
    }

    get playing(): boolean {
        return this._playing
    }

    set transitionSeekTime(t: number) {
        this._transitionSeekTime = t
    }

    get transitionSeekTime(): number {
        return this._transitionSeekTime
    }

    set skipMaterialVideo(b: boolean) {
        if (b) {
            // console.log('跳过素材视频', b)
        } else {
            // console.log('需要播放素材视频的状态')
        }
        this._skipMaterialVideo = b
    }

    get skipMaterialVideo(): boolean {
        return this._skipMaterialVideo
    }


    private stashHotspot(hotspot: type.hotspotValidation) {
        this._recordNavigation.push(hotspot)
    }

    public popStashHotspotAndOverlay() {
        return this._recordNavigation.pop()
    }

}

let status = roamStatus.instance

export { status }
