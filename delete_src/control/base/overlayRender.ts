import { projectConfig } from '../../config'
import { materialVideoStyleType, materialType, dislayModeType, overlayValidation, typeDescr } from '../util/type'
import { LiteEvent } from '../util/util'
import { status } from './roamStatus'
import { audio } from './audio'
import { management } from './roamManagement'
import { stat } from '../util/stat'
import { pano } from '../pano/core'


interface relocationValidation {
    longitude: number,
}


class OverlayRender {
    private static _instance: OverlayRender
    static get instance(): OverlayRender {
        if (!OverlayRender._instance) {
            OverlayRender._instance = new OverlayRender()
        }
        return OverlayRender._instance
    }

    originalMaterials: Array<overlayValidation>
    // 原本的浮层素材
    materials: object

    remainingMaterials: object
    // 剩余的浮层素材
    relocation: relocationValidation
    // 重定位
    touchedOverlayIds: Array<number> = []
    // 已经点击了浮层id序列
    showTime: number
    // 显示素材时间
    lastShowMaterials: Array<overlayValidation>
    // 上一次显示的素材

    walker: any

    private _callback: Function

    constructor() {
    }

    setCallback(cb: Function) {
        this._callback = cb
    }

    setMaterials(materials, time) {
        this.materials = {}
        this.remainingMaterials = this.materials
        this.relocation = undefined
        this.originalMaterials = materials
        this.showTime = time


        materials.forEach((m) => {
            try {
                this.materials[m.overlayID] = JSON.parse(JSON.stringify(m))
            } catch (err) {
                this.materials[m.overlayID] = m
                // console.warn('error', this.materials[m.overlayID])
            }
        })
    }

    public appendChild(overlay: overlayValidation) {

        if (overlay.children) {
            overlay.children.forEach((chid: any) => {
                if (typeof chid !== 'object') {
                    this.remainingMaterials[chid] = management.survey.getOverlay(chid)
                } else {
                    if (chid.overlayID) {
                        this.remainingMaterials[chid.overlayID] = chid
                    } else {
                        console.error(`overlay ${overlay.overlayID} children overlayID isn't exist`)
                    }
                }
            })
        }

    }

    render(overlay: overlayValidation) {
        let that = this

        this.appendChild(overlay)

        let relocation = this.relocation
        if (overlay.imageType &&
            overlay.imageType === 1 &&
            overlay.type === materialType.IMAGE_MATERIAL) {
            return
        }

        if (overlay.parent) {
            if (overlay.startLongitude === undefined) {
                overlay.startLongitude = overlay.parent.startLongitude
            }

            if (overlay.startLatitude === undefined) {
                overlay.startLatitude = overlay.parent.startLatitude
            }

            if (overlay.endLongitude === undefined) {
                overlay.endLongitude = overlay.parent.endLongitude
            }

            if (overlay.endLatitude === undefined) {
                overlay.endLatitude = overlay.parent.endLatitude
            }
        }

        if (overlay.type === materialType.AUDIO_MATERIAL) {
            if (status.isPressedTheOverlay === true &&
                overlay.displayMode === dislayModeType.businessMode) {
                audio.setSpeakSrc(overlay.text)
                audio.speakPlay()
            }

            if (status.isPressedTheOverlay === false &&
                (overlay.displayMode === dislayModeType.navigationMode ||
                    overlay.displayMode === undefined)) {
                audio.setSpeakSrc(overlay.text)
                audio.speakPlay()
            }

            if (overlay.displayMode === 2) {
                audio.setSpeakSrc(overlay.text)
                audio.speakPlay()
            }
        }

        if (
            (overlay.type === materialType.ROUTER_MATERIAL ||
                (overlay.type === materialType.COMMAND_MATERIAL &&
                    overlay.text === 'confirm')) &&
            projectConfig.isVrMode === false
        ) {
            that._callback(that.touchedOverlayIds.concat(overlay.overlayID))
        }

        if (overlay.type === materialType.IMAGE_MATERIAL) {
            let relocateOverlay = Object.assign({}, overlay)

            if (relocation) {
                let startLongitude = overlay.startLongitude || 0
                startLongitude -= relocation.longitude
                relocateOverlay.startLongitude = startLongitude

                let endLongitude = overlay.endLongitude || 0
                endLongitude -= relocation.longitude
                relocateOverlay.endLongitude = endLongitude
            }
            if (projectConfig.isVrMode === true) {
                that.walker.addOverlay(relocateOverlay, that.showTime)
            } else {
                management.addOverlay(relocateOverlay, that.showTime)
            }
        }

        if (overlay.type === materialType.VIDEO_MATERIAL) {
            if (projectConfig.isVrMode === true) {
                that.walker.addRoute([overlay.text, overlay.text], [], [])
                that.walker.continue(undefined, undefined, 0, function (success) {
                    if (!success) {
                        that.walker.continue()
                    }
                    that.walker.once('videoEnded', function () {
                        that.handleTouchedEvent_(overlay.overlayID)
                    })
                })
            } else {
                if ((overlay.endTime === undefined && overlay.startTime === undefined) ||
                    (that.showTime > overlay.startTime && that.showTime < overlay.endTime)) {
                    // let origin = that.materials
                    /*    flat.playMaterialVideo(overlay.text, overlay.scaleMode, function () {
                           if (overlay.scaleMode === materialVideoStyleType.wideScreen) {
                               that.clear()

                               that.reset()

                               // flat.showOverlay(status.currentHotspot.id)
                           } else {
                               // that.revert(overlay)
                               that.handleTouchedEvent_(overlay.overlayID)
                           }
                       }) */
                }
            }
        }


        if (overlay.type === materialType.URL_MATERIAL) {
            window.location.href = overlay.text
        }


        if (overlay.type ===materialType.COMMAND_MATERIAL  && overlay.text === 'stat' && overlay.params !== undefined) {
            overlay.params.forEach(function (param) {
                if(param === 'store.ad'){
                    stat.sendEventStat('store.ad.click', {
                        location: status.currentHotspot.id,
                        store: status.currentHotspot.desc,
                    })
                }
            })

            return
        }


    }

    clear() {
        if (projectConfig.isVrMode === true) {
            this.walker.removeAllOverlays()
        } else {
            management.removeAllOverlays()
        }
    }

    getRelocation() {
        return this.relocation
    }

    checkOverlayPosition(parent, child) {
        if (typeof child === 'object') {
            if (!child.startLongitude && !child.startLatitude &&
                parent.startLongitude && parent.startLatitude &&
                parent.endLongitude && parent.endLatitude) {
                child.startLongitude = parent.startLongitude
                child.startLatitude = parent.startLatitude
                child.endLongitude = parent.endLongitude
                child.endLatitude = parent.endLatitude
                child.startTime = parent.startTime
                child.endTime = parent.endTime
            }
        }
        return child
    }

    handleTouchedEvent_(overlayId) {
        let material = this.remainingMaterials[overlayId]

        if (!material) {
            console.error("can't find the overlay", overlayId)
            return
        }

        let overlayIds = []

        management.survey.expandOverlayDes_(material)

        overlayIds.push(overlayId)

        let des = material.des

        if (des.length > 0) {
            des = des.map((d) => {
                return this.checkOverlayPosition(material, d)
            })
        }

        while (des && des.length === 1 &&
            (des[0].type !== materialType.IMAGE_MATERIAL &&
                des[0].type !== materialType.VIDEO_MATERIAL &&
                des[0].type !== materialType.URL_MATERIAL &&
                des[0].type !== materialType.COUPON_MATERIAL &&
                (des[0].type !== materialType.COMMAND_MATERIAL ||
                    (des[0].type === materialType.COMMAND_MATERIAL &&
                        des[0].text !== 'close' &&
                        des[0].text !== 'favorite' &&
                        des[0].text !== 'back' &&
                        des[0].text !== 'revert' &&
                        des[0].text !== 'showNav' &&
                        des[0].text !== 'toast' &&
                        des[0].text !== 'navigateTo' &&
                            des[0].text !== 'stat'
                    )
                )
            )
        ) {
            overlayIds.push(des[0].overlayID)
            des = des.des
        }

        if (des && des.length === 1 && des[0].type === materialType.COUPON_MATERIAL) {

            projectConfig.wechatApi.addCard(des[0].text)

            return
        }

        if (des && des.length === 1 && des[0].type === materialType.COMMAND_MATERIAL && des[0].text === 'favorite') {

            // close
            this.clear()
            this.reset()

            return
        }

        if (des && des.length === 1 && des[0].type === materialType.COMMAND_MATERIAL && des[0].text === 'back') {

            this.revert(material)

            return
        }

        if (des && des.length === 1 && des[0].type === materialType.COMMAND_MATERIAL && des[0].text === 'close') {

            this.revert(material)

            return
        }

        if (des && des.length === 1 && des[0].type === materialType.COMMAND_MATERIAL && des[0].text === 'revert') {

            management.revertLastPoint()

            return
        }

        if (des && des.length === 1 && des[0].type === materialType.COMMAND_MATERIAL && des[0].text === 'showNav') {

            management.searchBar.show()

            return
        }


        if (des && des.length === 1 && des[0].type === materialType.COMMAND_MATERIAL && des[0].text === 'navigateTo') {

            management.navigationOperate.openAndfocusInNavigation(des[0].params[0])

            return
        }

        if (des && des.length === 1 && des[0].type === materialType.COMMAND_MATERIAL && des[0].text === 'toast') {
            const content = des[0].tip
            if (content) {
                const dom = <HTMLElement>document.querySelector('#coupon-tip')
                dom.innerHTML = content
                dom.style.display = 'block'


                this.clear()

                setTimeout(() => {
                    dom.style.display = 'none'
                }, 1000 * 2);
            }

            return
        }

        if (!des) {
            this._callback(this.touchedOverlayIds.concat(overlayIds))
            return
        }

        // this.touchedOverlayIds = this.touchedOverlayIds.concat(overlayIds)

        // this.clear()
        this.remove(material)

        if (material.type === materialType.VIDEO_MATERIAL &&
            projectConfig.isVrMode === true) {
            this.relocation = undefined
            if (material.startLongitude !== undefined) {
                this.relocation = { longitude: material.startLongitude }
            } else {
                this.relocation = { longitude: (material.parent && material.parent.startLongitude) || 0 }
            }
        }

        // this.remainingMaterials = {}
        // 渲染下一次新浮层的地方

        des.forEach((overlay) => {
            overlay.parent = material

            // if (!overlay.startLongitude &&
            //     !overlay.startLatitude &&
            //     overlay.type === materialType.IMAGE_MATERIAL

            // ) {
            //     overlay.startLongitude = material.startLongitude
            //     overlay.startLatitude = material.startLatitude
            //     console.log('epd overlay to add longitude', overlay)
            // }


            this.appendChild(overlay)
            /*
            在渲染情况下针对新浮层的处理
            */
            this.render(overlay)

            this.remainingMaterials[overlay.overlayID] = Object.assign({}, overlay)
        })
    }

    revert(material) {
        let that = this


        let renderOrigin = material.renderParent ? material.renderParent : material
        let parentOverlay = renderOrigin.parent ? renderOrigin.parent : renderOrigin
        let parentOverlayOrigin = parentOverlay.renderParent ? parentOverlay.renderParent : parentOverlay

        that.remove(material)

        if (parentOverlayOrigin.parent === undefined) {
            // console.log('顶层返回')
            pano.showOverlay(status.currentHotspot.id)
        } else {
            // console.log('上一层返回')

            that.render(parentOverlayOrigin)

            that.remainingMaterials[parentOverlayOrigin.overlayID] = Object.assign({}, parentOverlayOrigin)
            parentOverlayOrigin.children && parentOverlayOrigin.children.forEach(function (chid) {
                that.remainingMaterials[chid] = management.survey.getOverlay(chid)
            })
            parentOverlayOrigin.des && parentOverlayOrigin.des.forEach(function (overlay) {
                overlay.parent = parentOverlayOrigin

                that.remainingMaterials[overlay.overlayID] = Object.assign({}, overlay)
            })
        }
    }

    reset() {
        let that = this

        that.touchedOverlayIds = []
        that.materials = {}
        that.remainingMaterials = that.materials

        that.originalMaterials.forEach((m) => {
            try {
                that.materials[m.overlayID] = JSON.parse(JSON.stringify(m))
            }
            catch (err) {
                that.materials[m.overlayID] = m
                console.error('error', err, ' in ', m)
                return
            }
        })
    }


    remove(material) {

        let renderOrigin = material.renderParent ? material.renderParent : material
        let parentOverlay = renderOrigin.parent ? renderOrigin.parent : renderOrigin
        let parentOverlayOrigin = parentOverlay.renderParent ? parentOverlay.renderParent : parentOverlay

        management.customOverlay = management.customOverlay.filter(e => {
            if (e.overlayID === renderOrigin.overlayID ||
                parentOverlayOrigin.overlayID === e.overlayID
            ) {
                management.vr.removeOverlay(e)
            } else {
                return e
            }
        })

    }
}

let overlayRender = OverlayRender.instance

export { overlayRender }
