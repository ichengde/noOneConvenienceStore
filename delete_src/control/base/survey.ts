import { overlayValidation, materialType, bottomMarkValidation } from '../util/type'


export class survey {
    private nodes = {}
    private hotspots
    private roamMeta

    private h5TimeScale: number


    public plays: any = {}
    public video2Hotspot: object = {}
    public videos: object = {}
    public dealHotspots
    public overlays: Array<overlayValidation> = []

    constructor(roamMeta, hotspots, version) {
        this.roamMeta = roamMeta
        this.h5TimeScale = roamMeta.h5TimeScale ? roamMeta.h5TimeScale : 1

        roamMeta.nodes.forEach((n) => {
            this.nodes[n.nodeID] = n
        })

        this.dealHotspots = JSON.parse(JSON.stringify(hotspots))

        this.dealHotspots.forEach(h => {
            h.position = JSON.parse(h.position)
            this.dealHotspots[h.id] = h

            if (!this.video2Hotspot[h.position.videoId]) {
                this.video2Hotspot[h.position.videoId] = []
            }

            this.video2Hotspot[h.position.videoId].push(h)
        })

        roamMeta.overlays && roamMeta.overlays.forEach(o => {
            if (!o.webHide) {

                if (o.startTime !== undefined) {
                    o.startTime = parseFloat((o.startTime * this.h5TimeScale).toFixed(3))
                }
                if (o.endTime !== undefined) {
                    o.endTime = parseFloat((o.endTime * this.h5TimeScale).toFixed(3))
                }

                if (o.type !== materialType.COMMAND_MATERIAL &&
                    o.type !== materialType.ROUTER_MATERIAL &&
                    !(o.type === materialType.IMAGE_MATERIAL && o.imageType === 3)   // text
                ) {
                    o.text += `?v=${version}`
                }
                this.overlays[o.overlayID] = o
            }
        })


        roamMeta.videos.forEach(v => {

            if (v.overlays) {
                v.overlays = v.overlays.map((oid) => {
                    let clone = Object.assign({}, this.overlays[oid])
                    return Object.assign(clone, {
                        videoID: v.videoID
                    })
                })

                v.overlays.forEach((overlay) => {
                    this.expandOverlayDes_(overlay)
                })
            } else {
                v.overlays = []
            }

            this.videos[v.videoID] = v
        })


        roamMeta.autoRouters && roamMeta.autoRouters.forEach((router) => {
            if (router.targetVideoID !== undefined) {
                this.plays[router.routerID] = this.routerPoint2hotspot_(router.targetVideoID, router.targetVideoTime, router.overlays, router.longitude)
            } else if (router.pathPoint) {
                let currentNode = {}
                let rootNode = currentNode

                router.pathPoint.forEach((point, i) => {
                    currentNode = Object.assign(currentNode, this.routerPoint2hotspot_(point.videoID, point.waitPoint, point.overlays, point.longitude))
                    if (i < router.pathPoint.length - 1) {
                        currentNode['decision'] = {
                            '*': {}
                        }
                        currentNode = currentNode['decision']['*']
                    }
                })
                this.plays[router.routerID] = rootNode
            } else {
                console.error('unknown router type', router.routerID)
            }
        })

    }
    getOverlaysByVideos(videoIds) {
        let overlays = []
        videoIds.forEach((vid) => {
            if (this.videos[vid].overlays) {
                overlays = overlays.concat(this.videos[vid].overlays)
            }
        })

        return overlays
    }

    expandOverlayDes_(overlay) {
        let that = this

        if (!overlay.des) return

        overlay.des = overlay.des.reduce(function (acc, id) {
            // item expanded
            if (typeof id === 'object') return acc.concat([id])


            // item hide for web, ignore 
            if (!that.overlays[id]) return acc

            that.expandOverlayDes_(that.overlays[id])

            return acc.concat([that.overlays[id]])
        }, [])
    }

    getHotspotsByVideoId(videoId) {
        return this.video2Hotspot[videoId]
    }

    getPlayByRouterId(routerId) {
        if (this.plays[routerId]) {
            return this.plays[routerId]
        } else {
            console.error('get play error', this.plays, routerId)
        }
    }

    routerPoint2hotspot_(videoId, videoTime, overlays, longitude) {
        let that = this
        let hotspotVideoId = videoId

        let hotspot = that.findHotspotByVideoTime_(videoId, videoTime)
        if (hotspot === undefined) {
            return
        }

        let materials = []
        if (overlays) {
            materials = overlays.map(function (oid) {
                let o = that.overlays[oid]

                let clone = Object.assign({
                    videoID: hotspotVideoId
                }, o)

                that.expandOverlayDes_(clone)
                return clone
            })
        }

        return {
            hotspotId: hotspot.id,
            material: materials.concat(that.videos[hotspotVideoId].overlays),
            longitude: longitude
        }
    }

    findHotspotByVideoTime_(videoId, videoTime) {
        let hotspotVideoId = videoId
        let hotspotVideoTime = videoTime * this.h5TimeScale
        let hotspots = this.video2Hotspot[hotspotVideoId]

        if (!hotspots) {
            console.error('can\'t find hotspot', hotspotVideoId, '/', videoTime, 'by video')
            return
        }

        let minTime = 0.2
        let hotspot = undefined
        hotspots.forEach((h) => {
            if (Math.abs(h.position.timeoffset - hotspotVideoTime) < minTime) {
                minTime = Math.abs(h.position.timeoffset - hotspotVideoTime)
                hotspot = h
            }
        })

        if (!hotspot) {
            console.error('can\'t find hotspot', hotspotVideoId, '/', videoTime, 'by video time', hotspots)
            return
        }

        return hotspot
    }

    getOverlay(overlayId) {
        return this.overlays[overlayId]
    }

    get logoOverlay(): any {
        if (this.roamMeta.logoOverlay) {
            let t = this.overlays[this.roamMeta.logoOverlay]
            let bottomMark: bottomMarkValidation = {
                src: t.text,
                params: {
                    startLatitude: t.startLatitude,
                    startLongitude: t.startLongitude,
                    endLongitude: t.endLongitude,
                    endLatitude: t.endLatitude
                }
            }
            return bottomMark
        } else {
            console.warn('bottomMark err')
            return ''
        }
    }
}


function _formattedAngle(angle) {
    angle = angle % 360
    if (angle < 0) angle += 360
    return angle
}
