
import { projectConfig } from '../../config'
import { materialVideoStyleType, materialType, transitionValidation, overlayValidation, displayType, hotspotValidation, tagNavigation, moveStatusType } from '../util/type'
import { request } from './requestClient'
import { status } from './roamStatus'
import { transitionUtil, LiteEvent, commonUtil, isMaterialInTime } from '../util/util'
import { ui } from '../ui/uiControl'
import { overlayRender } from './overlayRender'
import { audio } from './audio'
import { survey } from './survey'
import { processBar } from '../plug/processBar'
import { searchBar } from '../plug/searchBar'
import { walkAway } from '../plug/walkAway'
import { beginnerGuider } from '../plug/beginnerGuider'
import { navigationOperate } from '../plug/navigationOperate'

import { panoStatus } from '../pano/status'
// import { pano } from './pano/Core'

declare let worldRenderer

// import { flat } from './flatCore'

interface currentTransitionValidation {
    transition: transitionValidation,
    pos: number
}

class roamManagement {
    private static _instance: roamManagement
    static get instance(): roamManagement {
        if (!roamManagement._instance) {
            roamManagement._instance = new roamManagement()
        }
        return roamManagement._instance
    }

    // private frontLike: frontLike = new frontLike()
    public survey: survey
    public vr: any
    public pathGen: any
    public searchBar: searchBar
    public processBar: processBar
    public walkAway: walkAway
    public navigationOperate: navigationOperate
    public overlayTouched = new LiteEvent()
    public customOverlay: Array<any> = []

    public beginnerGuider: beginnerGuider = new beginnerGuider()
    constructor() {
        this.overlayTouched.on((overlayId) => {
            overlayRender.handleTouchedEvent_(overlayId)
        })

        overlayRender.setCallback((overlayIds) => {
            if (status.theRestOfTheBusiness === undefined ||
                status.theRestOfTheBusiness.decision === undefined) {
                // 进入新获取浮层状态

                // 记录当下节点
                let lastOverlay = this.survey.getOverlay(overlayIds.pop())
                status.isPressedTheOverlay = true
                if (lastOverlay.type === materialType.ROUTER_MATERIAL) {
                    status.theRestOfTheBusiness = this.survey.getPlayByRouterId(lastOverlay.text)

                    if (status.theRestOfTheBusiness.hotspotId) {
                        status.endHotspot = status.hotspotById(status.theRestOfTheBusiness.hotspotId)
                        this.getPath()
                        // flat.go()
                    }
                }
            } else {
                let business = this.decisionByOverlays(status.theRestOfTheBusiness)
                if (business) {
                    status.theRestOfTheBusiness = business

                    status.endHotspot = status.hotspotById(status.theRestOfTheBusiness.hotspotId)
                    this.getPath()
                    // flat.go()
                }
            }
        })
    }

    updateUrlShareCurrentLocation(moveStatus: moveStatusType, navigationId) {
        if (this.vr) {
            const longitude = this.vr.getLongitude() / Math.PI * 180
            const latitude = this.vr.getLatitude() / Math.PI * 180
            projectConfig.wechatApi.longitude = longitude
            projectConfig.wechatApi.latitude = latitude

            const navigation: tagNavigation = status.hotspotById(navigationId)
            projectConfig.wechatApi.setNavigationInfo(moveStatus, navigation)
        }
    }

    async getHotspot() {
        let version = projectConfig.isVrMode === true ? 'v2' : 'v3'
        // foreach every hotspot ,getting path with start to other hotspot
        status.roamHotspot = await request.hotspotByVersion(version)

        return Promise.resolve()
    }

    async getHotspotGroup() {
        status.hotspotGroup = await request.hotspotGroup()

        return Promise.resolve()
    }


    public decisionByOverlays(currentNode) {
        if (currentNode.decision) {
            if (currentNode.decision['*']) {
                return currentNode.decision['*'];
            }
        }
    }

    private getCurrentTransition(currentTime) {
        let result: currentTransitionValidation
        status.path.transition.forEach((trst, index) => {
            if (currentTime >= trst.time) {
                result.transition = trst
                result.pos = index
            }
        })
        return result
    }

    getPath() {
        status.path = {}
        panoStatus.path = []
        panoStatus.position = 0

        let { snapStart, snapEnd } = this.getSnapStartAndEnd();

        status.path.video = management.pathGen.getFlattenVideo({ id: snapStart.videoId }, [], { id: snapEnd.videoId })
        status.path.video = projectConfig.debug.videoSrcChange(status.path.video)
        status.path.transition = management.pathGen.getPath({ id: snapStart.videoId }, [], { id: snapEnd.videoId })

        panoStatus.path = panoStatus.tidyPath(management.pathGen.getSnapshotPath({ id: snapStart.videoId, time: snapStart.time }, { id: snapEnd.videoId, time: snapEnd.time }, true))

        console.log(panoStatus.path);

        status.stopTime = transitionUtil.calcTransitionTimeForFlat(status.path.transition, status.path.transition.length - 1, status.endHotspot)
    }

    private getSnapStartAndEnd() {
        let snapStart = this.getSnapStart();

        let snapEnd;
        const endInfo = this.survey.dealHotspots[status.endHotspot.id];
        snapEnd = { videoId: endInfo.position.videoId, time: endInfo.position.timeoffset };

        return { snapStart, snapEnd };
    }

    private getSnapStart() {
        let snapStart;
        if (status.prevVideoTransition !== undefined) {
            console.log('transition->endHotspot');
            snapStart = status.prevVideoTransition;
        }
        else {
            console.log('currentHotspot->endHotspot');
            const startInfo = this.survey.dealHotspots[status.currentHotspot.id];
            snapStart = { videoId: startInfo.position.videoId, time: startInfo.position.timeoffset };
        }
        return snapStart;
    }

    multipleNavigationToFindTheShortestPath(multipleNavigations) {
        let snapStart = this.getSnapStart()
        let snapEnd = []
        multipleNavigations.forEach((nav) => {
            if (nav.info) {
                snapEnd.push({ id: nav.info.videoId, time: nav.info.timeoffset, navigation: nav })
            }
        })

        const findSetEndNavigationBySnapEnd = (shortestEnd) => {
            return snapEnd.find((snap) => {
                return snap.id === shortestEnd.id && snap.time === shortestEnd.time
            })
        }

        const shortestEnd = management.pathGen.findShortestDestination({ id: snapStart.videoId, time: snapStart.time }, snapEnd)
        status.endHotspot = findSetEndNavigationBySnapEnd(shortestEnd).navigation

        // return snapEnd
    }

    reachHotspot() {
        console.log('reachHotspot')
        // 清空 prevVideoTransition
        status.prevVideoTransition = undefined

        status.isPressedBlinkButton = false

        // status.setSkipMaterialVideo = false

        status.currentHotspot = status.endHotspot

        this.updateCameraFov()

        this.searchBar.updateSelectList()

        ui.showOrHidePlayButton(false)
        
        ui.ShowOrHideRoamSelectSwitch(true)
        // ui.setShowOrHide('.rightTop-container', true)
    }

    addOverlay(materialItem, currentTime: number) {
        if (isMaterialInTime(materialItem, currentTime) === true) {
            let ov = this.createOverlay_(materialItem)
            this.addOverlayChildren(materialItem, ov);
            // if (materialItem.children) {
            //     let that = this
            //     ov.children = []
            //     materialItem.children.forEach(function (childId) {
            //         const surveyOverlay = management.survey.overlays[childId]
            //         if (surveyOverlay) {
            //             management.survey.overlays[childId].renderParent = materialItem
            //             ov.children.push(that.createOverlay_(management.survey.getOverlay(childId), true))
            //         }
            //
            //         if (typeof childId === 'object') {
            //             childId.renderParent = materialItem
            //             ov.children.push(that.createOverlay_(childId))
            //         }
            //     })
            //     console.log('%c add lot of overlay', 'color:green', materialItem)
            // }


            if (status.isPressedTheOverlay === true && parseInt(materialItem.displayMode) === 1) {
                this.vr.addOverlay(ov)
            }

            if (status.isPressedTheOverlay === false && (parseInt(materialItem.displayMode) === 0 || materialItem.displayMode === undefined)) {
                this.vr.addOverlay(ov)
            }

            if (parseInt(materialItem.displayMode) === 2) {
                this.vr.addOverlay(ov)
            }

            if (currentTime !== undefined) {
                this.vr.updateOverlayPosition(ov, currentTime, materialItem)
            }

            this.customOverlay.push(ov)
        }
    }
    addOverlayChildren(materialItem, overlay) {
        if (materialItem.children) {
            let that = this
            overlay.children = []
            materialItem.children.forEach(function (childId) {
                const surveyOverlay = management.survey.overlays[childId]
                if (surveyOverlay) {
                    management.survey.overlays[childId].renderParent = materialItem
                    overlay.children.push(that.createOverlay_(management.survey.getOverlay(childId), true))
                }

                if (typeof childId === 'object') {
                    childId.renderParent = materialItem
                    let tmp = that.createOverlay_(childId)
                    overlay.children.push(tmp)
                    that.addOverlayChildren(childId, tmp);
                }
            })
            console.log('%c add lot of overlay', 'color:green', materialItem)
        }


    }
    removeAllOverlays() {
        if (!this.customOverlay.length) return
        this.customOverlay.forEach((ov) => {
            this.vr.removeOverlay(ov)
        })
        this.customOverlay = []
    }

    createOverlay_(mt, child?: boolean) {
        let that = this
        let newOverlay

        let param

        if (child === undefined && mt.startLongitude) {
            param = {
                image: mt.text,
                longitude: mt.startLongitude,
                latitude: mt.startLatitude,
                layer: mt.layer,
                sizeScale: mt.sizeScale
            }
        } else {
            param = {
                image: mt.text,
                xOffset: mt.xOffset,
                yOffset: mt.yOffset,
                layer: mt.layer,
                sizeScale: mt.sizeScale
            }
        }


        if (mt.imageType && mt.tilesHorizontal) {
            param = {
                image: mt.text,
                longitude: mt.startLongitude,
                latitude: mt.startLatitude,
                layer: mt.layer,
                type: mt.imageType,
                duration: mt.duration,
                landmark: mt.landmark,
                tilesHorizontal: mt.tilesHorizontal,
                tilesVertical: mt.tilesVertical,
                numberOfTiles: mt.numberOfTiles,
                sizeScale: mt.sizeScale
            }
        }

        if (mt.imageType && mt.imageType === 3) {
            param = mt
            param.type = mt.imageType
        }

        if (mt.vertical) {
            param['vertical'] = true
        }

        newOverlay = that.vr.overlay(param)
        if (mt.des) {
            newOverlay.event = () => {
                that.overlayTouched.trigger(mt.overlayID)
            }
        }
        newOverlay.overlayID = mt.overlayID
        return newOverlay
    }

    updateCameraFov() {
        // 更新fov 依据 hotspot的信息 默认为110
        let fov = JSON.parse(status.currentHotspot.position).fov
        if (!fov) {
            fov = projectConfig.defaultFov
        }
        worldRenderer.camera.fov = fov
        worldRenderer.camera.updateProjectionMatrix()
    }


    getMaterial(id: string) {
        let material = []
        if (!status.isPressedTheOverlay || status.theRestOfTheBusiness === undefined) {

            material = management.survey.getOverlaysByVideos([JSON.parse(status.hotspotById(id).position).videoId])
        } else {
            console.log('已经点击了浮层,持续在浮层状态 this.direction', status.theRestOfTheBusiness)

            if (status.theRestOfTheBusiness) {
                material = status.theRestOfTheBusiness.material
            }
        }

        console.log(material)
        return material
    }

    revertLastPoint() {
        status.endHotspot = status.popStashHotspotAndOverlay()

        status.isPressedTheOverlay = false
        status.isPressedBlinkButton = true
        status.skipMaterialVideo = true

        management.searchBar.hide()

        // flat.go()
    }


    getHotspotSnapshot(hotspot) {
        let pic: string
        if (hotspot.snapshot) {
            pic = hotspot.snapshot
        } else {
            let info = JSON.parse(hotspot.position)
            pic = projectConfig.debug.picChange(management.pathGen.getSnapshot(info.videoId, info.timeoffset))
        }
        return pic
    }

}



let management = roamManagement.instance

export { management, currentTransitionValidation, searchBar, processBar, walkAway, navigationOperate }