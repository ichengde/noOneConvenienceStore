import { projectConfig } from '../../config'
import { ui } from '../ui/uiControl'
import * as CryptoJS from "crypto-browserify"
import { roamHotspotValidation, hotspotGroupValidation, v4HotspotsResType, redPacketCount } from '../util/type'
import { audio } from './audio'
import { commonUtil } from '../util/util'
import { coupon } from '../pano/coupon';

class requestClient {
    private static _instance: requestClient
    static get instance(): requestClient {
        if (!requestClient._instance) {
            requestClient._instance = new requestClient
        }
        return requestClient._instance
    }

    public requestProcess<T>(src: string, method: string = 'GET', params?: string, headers?: object): Promise<T> {
        let request = new XMLHttpRequest()
        request.open(method, src)
        request.setRequestHeader('Content-Type', 'application/json;charset=UTF-8')
        if (!!headers) {
            for (var key in headers) {
                if (headers.hasOwnProperty(key)) {
                    request.setRequestHeader(key, headers[key])
                }
            }
        }
        
        return new Promise((resolve, reject) => {
            request.onload = function () {
                if (request.status >= 200 && request.status < 400) {
                    let resp = JSON.parse(request.responseText)
                    resolve(resp)
                }
            }
            request.onerror = function (error) {
                commonUtil.debug(error)
                reject(error)
            }
            request.send(params)
        })
    }

    async info(): Promise<any> {
        let ans = await <any>this.requestProcess(projectConfig.apiSite + 'v2/roam/info?roamId=' + projectConfig.roamId)

        document.title = ans.result.name

        return Promise.resolve(ans.result)
    }

    async detail(): Promise<any> {
        var params = {
            roam_id: projectConfig.roamId,
            platform: 'Web',
        }

        let ans = await <any>this.requestProcess(projectConfig.apiSite + 'v2/roam/detail', 'POST', JSON.stringify(params))
        let bgmID: number = ans.detail.meta.backgroundMusic
        let speakID: number
        let bgmSrc: string
        let speakSrc: string

        if (ans.detail.meta && ans.detail.meta.welcomeOverlays) {
            speakID = ans.detail.meta.welcomeOverlays[0]
        }

        for (var overlay of ans.detail.meta.overlays) {
            if (overlay.overlayID === bgmID) {
                bgmSrc = overlay.text
            }
            if (overlay.overlayID === speakID) {
                speakSrc = overlay.text
            }
        }

        if (bgmSrc) {
            audio.createBgm(bgmSrc)
        } else {
            console.warn('bgm undefined')
        }

        if (speakSrc) {
            audio.setSpeakSrc(speakSrc)
        } else {
            console.warn('speak undefined')
        }

        projectConfig.detailInFo = ans

        return Promise.resolve(ans)
    }


    async preloadPano(): Promise<void> {
        let num = 0

        let promise = new Promise<void>(resolve => {
            if (projectConfig.panoSrc.length > 0) {
                let toGetPano = function (src) {
                    let oReq = new XMLHttpRequest()
                    oReq.open('GET', src)
                    oReq.onerror = function (error) {
                        console.log(error)
                    }
                    oReq.onload = function (oEvent) {
                        num++
                        // ui.updateProgress(Math.floor(num / projectConfig.panoSrc.length * 100))
                        if (num === projectConfig.panoSrc.length) {
                            resolve()
                        } else {
                            toGetPano(projectConfig.panoSrc[num])
                        }
                    }
                    oReq.send()
                }

                toGetPano(projectConfig.panoSrc[num])
            } else {
                resolve()
            }
        })
        return promise
    }

    hotspotByVersion(verison): Promise<roamHotspotValidation> {
        let site: string
        switch (verison) {
            case 'v2':
                site = projectConfig.apiSite + verison + '/roam/hotspot?roamId=' + projectConfig.roamId
                break
            case 'v3':
                site = projectConfig.apiSite + verison + '/inwalk/hotspots?roamId=' + projectConfig.roamId
                break
            default:
        }
        return <Promise<roamHotspotValidation>>this.requestProcess(site)
    }

    hotspotGroup(): Promise<hotspotGroupValidation> {
        return <Promise<hotspotGroupValidation>>this.requestProcess(projectConfig.apiSite + 'v3/inwalk/hotspot_groups?roamId=' + projectConfig.roamId)
    }

    addedCouponToRequestCode(encode, cardId) {
        return <Promise<hotspotGroupValidation>>this.requestProcess(`${projectConfig.apiSite}v4/wechat/get_code?code=${encode}&cardId=${cardId}&userId=${projectConfig.userId}`)
    }

    getCoupon(): Promise<v4HotspotsResType> {
        return new Promise((resolve, reject) => {
            request.requestProcess(`${projectConfig.apiSite}v4/inwalk/hotspots?inwalkId=${projectConfig.roamId}`).then((res: v4HotspotsResType) => {
                if (res.code === 0) {
                    resolve(res)
                } else {
                    reject()
                }
            })
        })
    }


    getCouponText(cardId) {
        return new Promise((resolve, reject) => {
            request.requestProcess(`${projectConfig.apiSite}v3/wechat/card?cardId=${cardId}`).then((res: any) => {
                if (res.errcode === 0) {
                    const couponText = res.card.general_coupon.base_info.title

                    resolve(couponText)
                } else {
                    reject()
                }
            }).catch((err) => {
                console.log(err)
            })
        })
    }

    getAllCouponText() {
        return new Promise((resolve, reject) => {
            request.requestProcess(`${projectConfig.apiSite}v3/coupon/items?inwalkId=${projectConfig.roamId}&count=200`).then((res: any) => {
                let couponTexts = {}

                const data = res.couponCards

                resolve(data)
            }).catch((err) => {
                console.log(err)
            })
        })
    }

    getShareTemplate() {
        return new Promise((resolve, reject) => {
            request.requestProcess(`${location.protocol}//mall.zhixianshi.com/${projectConfig.roamId}/wxa/share.json`).then((res: any) => {

                resolve(res)
            }).catch((err) => {
                console.log(err)
            })
        })
    }

    getRedPacketCount(): Promise<redPacketCount> {
        return request.requestProcess(`${projectConfig.apiSite}v4/tianhong/get_red_pack_count`)
    }
}
let request = requestClient.instance
export { request }
