import { projectConfig } from '../../config'
import { commonUtil } from '../util/util'

class Audio {
    public speakAudio: HTMLAudioElement = document.createElement('audio')
    public bgm: HTMLAudioElement = document.createElement('audio')
    private isBan: boolean = false
    private icon = <HTMLImageElement>document.querySelector('#audio-icon')
    private static _instance: Audio
    static get instance() {
        if (!Audio._instance) {
            Audio._instance = new Audio()
        }
        return Audio._instance
    }

    /* 
        for ios 9.0
    */
    firstPlayForIos9 = true

    private constructor() {
        this.speakAudio.id = 'speakAudio'
        this.speakAudio.autoplay = true
        document.querySelector('body').appendChild(this.speakAudio)

        document.addEventListener('touchstart', (e: any) => {
            if (this.bgm.src) {
                this.bgmPlay()
                if (commonUtil.isIOS9OrLess() === true) {
                    if (this.firstPlayForIos9 === true) {
                        // flat.makeItPlay()
                        this.firstPlayForIos9 = false
                    }
                }
            }
        });
    }

    public setSpeakSrc(src) {
        this.speakAudio.src = src
    }
    public speakPlay() {
        if (this.isBan === false && this.speakAudio !== undefined) {
            this.speakAudio.play()
        }
    }
    public speakPause() {
        this.speakAudio.pause()
    }
    public bgmPlay() {
        if (this.isBan === false && this.bgm !== undefined) {
            this.bgm.loop = true
            this.bgm.play()

            this.iconOperateEvent()
        }
    }

    public bgmPause() {
        this.bgm.pause()
    }

    public iconShow() {
        this.icon.style.zIndex = '1001'
    }

    private iconOperateEvent() {
        this.icon.style.zIndex = '1001'
        this.icon.addEventListener('click', () => {
            if (this.isBan === false) {
                this.isBan = true
                this.icon.src = './images/audio-ban.png'
                this.bgmPause()
                this.speakAudio.pause()
            } else {
                this.isBan = false
                this.icon.src = './images/audio-ok.png'
                this.bgmPlay()
            }
        })
    }


    public createBgm(src) {
        var that = this

        this.bgm.autoplay = true
        this.bgm.id = 'bgm'
        this.bgm.loop = true
        this.bgm.src = src

        document.querySelector('body').appendChild(this.bgm)
        this.bgmPlay()
    }
}

let audio = Audio.instance
export { audio }