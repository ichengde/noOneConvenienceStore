
import { management } from '../base/roamManagement'
import { projectConfig } from '../../config'
import { tagNavigation } from '../util/type'
import { request } from '../base/requestClient'
import { status } from '../base/roamStatus'
import { pano } from '../pano/core'
import { stat } from '../util/stat'

export class RedPacket {
    line: number
    row :number
    navigations :tagNavigation[]
    available:boolean = false
    backgroundOverlays

    redPacketBlock: HTMLImageElement = <HTMLImageElement>document.querySelector('#red-packet')
    redPacketInfo: HTMLImageElement = <HTMLImageElement>document.querySelector('#red-packet-info')
    redPacketRaven: HTMLImageElement = <HTMLImageElement>document.querySelector('#red-packet-info .red-packet-raven')

    constructor(navigations: tagNavigation[]){
        this.navigations = navigations
        this.row  = 4
        this.line = 5

        this.bindControl()
        if (navigations && navigations.length > 0) {
            this.showButton()
        }

        let that = this
        that.fetchCount()
        window.setInterval(()=>{
            that.fetchCount()
        }, 60000)
    }

    fetchCount() {
        let that = this;
        request.getRedPacketCount().then((res) => {
            that.redPacketBlock.classList.remove('unknown')

            if (res.status === 'success' && !!res.count && res.count > 0) {
                that.available = true
                that.redPacketBlock.classList.add('available')
            } else {
                that.available = false
                that.redPacketBlock.classList.remove('available')
            }

            return Promise.resolve()
        }).catch ((e)=>{
            that.redPacketBlock.classList.add('unknown')
        })
    }

    bindControl() {
        let that = this;
        this.redPacketBlock && this.redPacketBlock.addEventListener('click', () => {
            that.hideButton()

            if (that.available) {
                that.showNavigations()
            } else {
                that.showInfo()
                window.setTimeout(()=>{
                    that.hideInfo()
                    that.showButton()
                }, 4500)
            }
        })
    }

    containsNavigation(navigation) {
        return this.navigations.includes(navigation)
    }

    showNavigations(){
        this.backgroundOverlays = this.convert()
        if(this.backgroundOverlays.length > 0 ){
            this.backgroundOverlays.forEach(function (overlay) {
                management.vr.addOverlay(overlay)
            })

        }
    }

    hideNavigations() {
        this.backgroundOverlays.forEach(function (overlay) {
            management.vr.removeOverlay(overlay)
        })

    }

    showButton() {
        this.redPacketBlock.classList.remove('hide')
    }

    hideButton() {
        this.redPacketBlock.classList.add('hide')
    }

    showInfo() {
        this.redPacketRaven.src = './images/raven.gif'
        this.redPacketInfo.classList.remove('hide')

        let that = this
        window.setTimeout(()=>{
            that.redPacketRaven.classList.add('over')
        },10)
    }

    hideInfo() {
        this.redPacketRaven.classList.remove('over')
        this.redPacketInfo.classList.add('hide')
    }

    generateBackgroudOverlay(longitude){
        let count = this.navigations.length
        let unit = 100;
        let width = this.row * unit/1000*5;
        let height = Math.ceil(count / this.row)*unit/1000*5;
     //   const longitude = management.vr.getLongitude() / Math.PI * 180
        let param = {
            overlayID: 'redPacket-bg',
            image: "./images/red_packet_nav_bg.png",
            longitude: longitude,
            latitude: 0,
            sizeScale:1.2,
            layer: 0,
            type: 1,
            group: 'redPacket'
        }
        return management.vr.overlay(param)
    }
    generateCloseOverlay(){
        let param = {
            overlayID: 'redPacket-close',
            image: './images/redPacket_close.png',
            xOffset: 0.5,
            yOffset: 1.1,
            sizeScale:1.2,
            type: 1,
            group: 'redPacket',
        }
        const newOverlay = management.vr.overlay(param)
        newOverlay.event = () => {
            this.hideNavigations()
        }
        return newOverlay
    }

    convert(){
        let count = this.navigations.length
        let maxCountEachPanel = this.row * this.line;
        let that = this
        let panelCount = Math.ceil(count / (maxCountEachPanel))
        let backgroudOverlays = [];
        const longitude = management.vr.getLongitude() / Math.PI * 180
        const longitudeDelta = panelCount > 1 ?-15:0;
        for( let i =0 ; i < panelCount ; i++){
            const offset = i%2 ? 60*Math.ceil(i/2): -60*Math.ceil(i/2);
            let overlay = this.generateBackgroudOverlay(longitude +offset+longitudeDelta);
            overlay.children = []
            let currentPanelCount = (count - maxCountEachPanel*i) > maxCountEachPanel ? maxCountEachPanel:(count - maxCountEachPanel*i)
            for (let j= 0;j<currentPanelCount;j++){
                let navigation = this.navigations[i*maxCountEachPanel+j]
                let yOffsetOrigin = 0.2
                let paddingX = 0.08
                let paddingY =0.08
                let currentRow = j %this.row +1
                let currenLine = Math.floor(j/this.row ) +1
                let xOffset = (2*currentRow -1) / (2*this.row)*(1 - 2* paddingX) +paddingX ;
                let yOffset = (2*currenLine -1) / (2*this.line)*(1 - 2* paddingY-yOffsetOrigin ) +yOffsetOrigin + paddingY ;
                let param = {
                    overlayID: `${navigation.id}-redPacket`,
                    image: navigation.poster,
                    xOffset: xOffset,
                    yOffset: yOffset,
                    sizeScale:1.2,
                    type: 1,
                    group: 'redPacket',
                }
                const newOverlay = management.vr.overlay(param)
                newOverlay.event = () => {
                    stat.sendEventStat('red_packet.click', {
                        location: navigation.id,
                        store: navigation.desc,
                    })
                    status.endHotspot = navigation
                    pano.roamBlind()
                    that.hideNavigations()
                }
                overlay.children.push(newOverlay)
            }
            overlay.children.push(this.generateCloseOverlay());
            overlay.onRemove = () => {
                overlay.hasRemoved = true;
                let needshow = true;
                that.backgroundOverlays.forEach(function (tmp) {
                    if(tmp.hasRemoved !== true){
                        needshow = false;
                    }

                })
                if(needshow ===true){
                    that.showButton()
                }
            }
            backgroudOverlays.push(overlay);
        }
        return backgroudOverlays;
    }
}
