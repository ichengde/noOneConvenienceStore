
import { management } from '../base/roamManagement'
import { ui } from '../ui/uiControl'

/* 
    add 
        <div id="revertLastStatus">返回</div>
    in index.html
*/

export class controlSet {
    skipControl = <HTMLDivElement>document.querySelector('#skipControl')
    revertLastStatus = <HTMLDivElement>document.querySelector('#revertLastStatus')
    arrow = <HTMLDivElement>document.querySelector('#arrow')
    arrowR = <HTMLDivElement>document.querySelector('#arrow_r')
    wrap = <HTMLDivElement>document.querySelector('.wrap')
    constructor() {
        this.revertLastStatus && this.revertLastStatus.addEventListener('click', () => {
            management.revertLastPoint()
        });

        this.controlSlipEvent()
    }

    hideSkipButton() {
        ui.setShowOrHide('#skipControl', false, 0)
    }

    showSkipButton() {
        ui.setShowOrHide('#skipControl', true, 1999)
    }

    controlSlipEvent() {
        this.arrow.addEventListener("click", () => {
            this.wrap.scrollLeft = this.wrap.scrollLeft - 50
        })

        this.arrowR.addEventListener("click", () => {
            this.wrap.scrollLeft = this.wrap.scrollLeft + 50
        })
    }

}