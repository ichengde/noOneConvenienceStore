
import { status } from '../base/roamStatus'
import { management } from '../base/roamManagement'
import { pano } from '../pano/core'
import { panoStatus } from '../pano/status'
import { guiderType, moveStatusType } from '../util/type'
import { stat } from '../util/stat'

export class navigationOperate {
    originSearchBar: HTMLDivElement = <HTMLDivElement>document.querySelector('.roam-select-input-block')
    navigationOperate: HTMLDivElement = <HTMLDivElement>document.querySelector('.navigation-operate-block')

    navigationInBlock: HTMLInputElement = <HTMLInputElement>document.querySelector('#navigation-in-block')
    navigationFinalBlock: HTMLDivElement = <HTMLDivElement>document.querySelector('#navigation-final-block')

    navigationInInput: HTMLInputElement = <HTMLInputElement>document.querySelector('#navigation-in-block>.navigation-text')
    navigationFinalText: HTMLDivElement = <HTMLDivElement>document.querySelector('#navigation-final-block>.navigation-text')

    navigationStart: HTMLDivElement = <HTMLDivElement>document.querySelector('#navigation-start')

    controlBlock: HTMLDivElement = <HTMLDivElement>document.querySelector('.roam-control')

    backDom = document.querySelector('#naviagtion-back')

    _modifyObject: 'in' | 'final' = 'final'

    constructor() {

        this.bindReturnOriginOperateEvent();

        this.bindSwitchModifyNavigationInStatus();

        this.bindNavigationInInputWhenInputtingEvent();

        this.bindStartNavigatorEvent();

    }

    private bindNavigationInInputWhenInputtingEvent() {
        this.navigationInInput && this.navigationInInput.addEventListener('input', (ev) => {
            const target: HTMLInputElement = <HTMLInputElement>ev.srcElement;
            management.searchBar.search(target.value);
        });
    }

    private bindSwitchModifyNavigationInStatus() {
        this.navigationInBlock && this.navigationInBlock.addEventListener('click', () => {
            this.modifyObject = 'in';
            management.searchBar.showPreviewPanel()
        });
    }

    private bindReturnOriginOperateEvent() {
        this.backDom && this.backDom.addEventListener('click', () => {
            pano.cancelRoamPreview()
            this.convertToOrigin()
            this.getTheLastNavigationPathToUpdateMiniProgramInfo()
        });

        this.navigationFinalBlock && this.navigationFinalBlock.addEventListener('click', () => {
            pano.cancelRoamPreview()
            this.convertToOrigin()
            this.getTheLastNavigationPathToUpdateMiniProgramInfo()
        });
    }

    getTheLastNavigationPathToUpdateMiniProgramInfo() {
        if (panoStatus.navigationWay) {
            if (panoStatus.isOrigin === false && panoStatus.isDestination === false) {
                management.updateUrlShareCurrentLocation(moveStatusType.way, panoStatus.navigationWay.endHotspot.id)
            }
        }
    }

    private bindStartNavigatorEvent() {
        this.navigationStart && this.navigationStart.addEventListener('click', () => {
            stat.sendEventStat('navigation.walk.start', {
                src: status.currentHotspot.id,
                src_name: status.currentHotspot.desc,
                dst: status.endHotspot.id,
                dst_name: status.endHotspot.desc,
            })

            pano.roamGo()
            management.beginnerGuider.checkAndGuidance(guiderType.startNavigationGuide)
            management.searchBar.hide()
        });
    }

    openAndfocusInNavigation(endNavigationId) {
        const endNavigation = status.hotspotById(endNavigationId)
        status.endHotspot = endNavigation
        management.getPath()
        this.convertToNavigator()

        this.modifyObject = 'in';
        management.searchBar.show()

        management.searchBar.showPreviewPanel()

        this.navigationInInput.focus()
    }


    previewNavigation() {
        if (this.modifyObject === 'final') {
            pano.roamPreview()
        } else {
            // 先到目标点 再进行导航
            pano.reachStartNavigationToEndNavigation();
        }
    }


    convertToNavigator() {
        this.modifyObject = 'final'
        this.originSearchBar.style.display = 'none'
        this.navigationOperate.style.display = 'block'
        this.controlBlock.style.display = 'none'
        management.searchBar.consoleBar.classList.add('navigate-active')
        management.searchBar.hidePreviewPanel()

        this.navigationInInput.value = ''
        this.adjustBlockWidth()
        this.showFinalText()
        this.previewNavigation()
    }

    convertToOrigin() {
        this.modifyObject = 'final'
        this.navigationOperate.style.display = 'none'
        this.originSearchBar.style.display = 'flex'
        this.controlBlock.style.display = 'flex'
        management.searchBar.consoleBar.classList.remove('navigate-active')
        management.searchBar.showPreviewPanel()
    }

    showFinalText() {
        let domIdentify
        let navigationName
        if (this.modifyObject === 'final') {
            navigationName = status.endHotspot.desc

            this.navigationFinalText.innerHTML = navigationName
        } else {
            navigationName = status.currentHotspot.desc

            this.navigationInInput.value = navigationName
        }

    }

    get modifyObject() {
        return this._modifyObject
    }

    set modifyObject(cate) {
        if (cate === 'in') {
            this._modifyObject = 'in'
            this.navigationInInput.style.color = '#76ff68'
            this.navigationFinalBlock.style.color = '#fff'

            management.searchBar.hideSpecialGroup()
        } else {
            this._modifyObject = 'final'
            this.navigationInInput.style.color = '#fff'
            this.navigationFinalBlock.style.color = '#76ff68'

            management.searchBar.showSpecialGroup()
        }

        management.searchBar.updateSelectList()
    }

    adjustBlockWidth() {
        const widthTo = (dom) => {
            dom.style.width = `${window.innerWidth - this.navigationStart.offsetWidth - 52 - 22}px`
        }
        widthTo(this.navigationInBlock)
        widthTo(this.navigationInBlock.querySelector('.navigation-input-background'))
        widthTo(this.navigationFinalBlock)
        widthTo(this.navigationFinalBlock.querySelector('.navigation-input-background'))

    }
}
