import { projectConfig } from '../../config'
import { management } from '../base/roamManagement'
import { ui } from '../ui/uiControl'
import { status } from '../base/roamStatus'
import { displayType, hotspotValidation, tagNavigation, groupStatucValidation, v4HotspotsType, guiderType } from '../util/type'
import { commonUtil } from '../util/util'
import { audio } from '../base/audio'
import { pano, panoCore } from '../pano/core'
import { gridLine } from './gridLine'
import { navigationOperate } from './navigationOperate'
import * as scroll from 'iscroll'
import { request } from '../base/requestClient'
import { RedPacket } from '../plug/redPacket'
import { panoStatus } from '../pano/status'
import { stat } from '../util/stat'
/*
    该页code hotspot代表导航点
*/

export class searchBar {
    currentInputPatt: RegExp

    searchInputDom = <HTMLInputElement>document.querySelector('#roam-select-input')
    inputBlock = <HTMLDivElement>document.querySelector('.roam-select-input-block')
    // switch = <HTMLImageElement>document.querySelector('.free-roam-switch')
    groupBar: NodeListOf<HTMLDivElement>
    panel = <HTMLDivElement>document.querySelector('.free-roam-panel')
    closeButton = document.querySelector('.roam-revert')
    roamGoButton = <HTMLDivElement>document.querySelector('.roam-go')
    roamBlinkButton = <HTMLDivElement>document.querySelector('.roam-blink')
    awayContainer = <HTMLDivElement>document.querySelector('.away-container')
    listBar: HTMLDivElement = <HTMLDivElement>document.querySelector('.roam-select-list')
    listItem: NodeListOf<HTMLDivElement>
    specialChoice: NodeListOf<HTMLDivElement> = <any>document.querySelectorAll('.special-choice')
    searchBarContainer = document.querySelector('.search-bar-container')
    scrollList: HTMLDivElement = <HTMLDivElement>document.querySelector('#inwalk-scroll-list')
    controlBlock: HTMLDivElement = <HTMLDivElement>document.querySelector('.roam-control')
    consoleBar: HTMLDivElement = <HTMLDivElement>document.querySelector('.console-header')
    navigateConsole: HTMLDivElement = <HTMLDivElement>document.querySelector('.navigate-console')

    _groupId: string = ''
    panelHidden: boolean = false


    gridSheet: gridLine = new gridLine()
    myScroll
    tagNavigations

    _couponTexts

    washRoom = []
    infantRoom = []
    tagOrders = ['便利生活', '皮鞋皮具', '内衣家居', '男士服装', '女装', '儿童成长', '餐饮娱乐', '化妆品精品', '运动休闲', '快时尚', '超市']
    redPacket: RedPacket
    redPacketHotspots = []

    constructor() {
        this.updateGroupName()
        this.convertNavigationInfo()

        request.getAllCouponText().then((couponCards) => {
            this.setCouponTexts(couponCards)
            this.updateHotspotGroupTagData();
            this.updateSelectList()

            this.switchSearchBarEvent()
            this.selectGroupBarEvent()
            this.selectItemEvent()
            this.inputingEvent()
            this.roamGoButtonEvent()
            this.roamBlinkButtonEvent()

            this.bindTapListEvent()
            this.bindScrollBlurInputDomEvent();

        })
    }

    private bindScrollBlurInputDomEvent() {
        this.scrollList.addEventListener('scrollStart', this.blurInputDom.bind(this), false)
    }

    private bindTapListEvent() {
        this.scrollList.addEventListener('tap', this.tapItem.bind(this), false)
    }

    public addGroupTemplate(groupId, groupName): string {
        // <img class="hotspot-group-icon" src="images/floor-${groupId + 1}.png">
        let template = `
            <div class="group-bar" data-group="${groupId}">
                ${groupName}
            </div>
        `

        return template
    }

    private addSpecialGroupTemplate(groupId, groupName): string {
        // <img class="hotspot-group-icon" src="images/floor-${groupId + 1}.png">
        const imageSource = `./images/${groupId}.png`
        let template = `
            <div class="group-bar special-group-bar" data-group="${groupId}" style="font-size: 10px;">
                <img src="${imageSource}" style="height: 16px;transform: translateY(25%);" />
                ${groupName}
            </div>
        `

        return template
    }

    public hideSpecialGroup() {
        const doms = document.querySelectorAll(`.special-group-bar`)

        for (let index = 0; index < doms.length; index++) {
            const element = <HTMLDivElement>doms[index];
            element.style.display = 'none'
        }

    }

    public showSpecialGroup() {
        const doms = document.querySelectorAll(`.special-group-bar`)

        for (let index = 0; index < doms.length; index++) {
            const element = <HTMLDivElement>doms[index];
            element.style.display = 'block'
        }

    }

    public updateGroupName() {
        let groupHtml: string = ''

        status.hotspotGroup.groups = status.hotspotGroup.groups.sort((a, b) => {

            return a.sort - b.sort

        })

        status.hotspotGroup.groups.forEach((group) => {
            groupHtml += this.addGroupTemplate(group.gid, group.desc)
        })

        groupHtml += this.addSpecialGroupTemplate('washRoom', '洗手间')
        groupHtml += this.addSpecialGroupTemplate('infantRoom', '母婴室')

        this.searchBarContainer.innerHTML = groupHtml
        this.groupBar = <any>document.querySelectorAll('.group-bar')
    }


    public updateSelectList() {
        this.listBar = <any>document.querySelector('.roam-select-list')
        this.listBar.innerHTML = ''

        const consoleBarHeight = this.consoleBar.getBoundingClientRect().height

        this.tagNavigations && this.tagNavigations.forEach((group, groupIndex) => {

            const tags = group.tags
            let that = this
            const keyNames = Object.keys(tags).sort((a, b) => {
                return that.tagOrders.indexOf(b) - that.tagOrders.indexOf(a)
            })

            if (this.currentInputPatt) {
                keyNames.forEach((tagName) => {
                    const navigations: Array<any> = tags[tagName]
                    let filterNavigations = navigations.filter((navigation) => {
                        return this.currentInputPatt && this.currentInputPatt.test(navigation.desc)
                    })

                    if (filterNavigations.length > 0) {
                        this.composeTag(tagName, filterNavigations, consoleBarHeight);
                    }
                })
            } else {
                if (group.gid === this.groupId) {
                    keyNames.forEach((tagName) => {
                        let navigations = tags[tagName]
                        this.composeTag(tagName, navigations, consoleBarHeight);
                    })
                }
            }


        })


    }

    private getFilterStartOrEndNavigationFunc() {
        return (nav) => {
            let banNavigation = { id: undefined };
            if (management.navigationOperate._modifyObject === 'in') {
                if (status.endHotspot) {
                    banNavigation = status.endHotspot;
                }
            }
            else {
                if (status.currentHotspot) {
                    banNavigation = status.endHotspot;
                }
            }
            return banNavigation.id !== nav.id;
        };
    }

    private makeScroll() {
        if (this.listBar.children.length > 0 && this.myScroll === undefined) {
            this.myScroll = new scroll(this.scrollList, {
                tap: true
            });
            this.myScroll.on('scrollStart', this.blurInputDom.bind(this));
        }
        if (this.myScroll) {

            this.myScroll.refresh()

            this.myScroll.scrollTo(0, 0);
        }
    }

    private composeTag(tagName: string, navigations: any, consoleHeight: number) {
        const existTagNameDom = document.querySelector(`#${tagName}-tag`)
        const existTagContainerDom = document.querySelector(`#${tagName}-container`)
        let tagNameDom
        let tagContainerDom


        tagNameDom = this.findTagNameDom(existTagContainerDom, tagNameDom, existTagNameDom, tagName);

        tagContainerDom = this.findTagContainerDom(existTagContainerDom, tagContainerDom, tagName);

        if (navigations) {
            this.insertNavigationsDom(navigations, tagContainerDom).then(() => {

                const tagNameDomPosInfo = tagNameDom.getBoundingClientRect();
                const topOffset = tagNameDomPosInfo.top + tagNameDomPosInfo.height - consoleHeight;
                this.gridSheet.toPaint(tagContainerDom, topOffset);

                this.makeScroll();
            })

        }
    }

    private insertNavigationsDom(navigations: any, tagContainerDom: any) {
        return new Promise((resolve, reject) => {

            navigations.forEach((navigation) => {
                if (navigation.hide !== true) {
                    this.addSelectItemAndFilterCurrentPosition(tagContainerDom, navigation);
                }
            });

            resolve()
        })
    }

    private findTagContainerDom(existTagContainerDom: Element, tagContainerDom: any, tagName: string) {
        if (existTagContainerDom) {
            tagContainerDom = existTagContainerDom;
        }
        else {
            tagContainerDom = document.createElement('div');
            tagContainerDom.id = `${tagName}-container`;
            tagContainerDom.classList.add('tagContainer');
            this.listBar.appendChild(tagContainerDom);
        }
        return tagContainerDom;
    }

    private findTagNameDom(existTagContainerDom: Element, tagNameDom: any, existTagNameDom: Element, tagName: string) {
        if (existTagContainerDom) {
            tagNameDom = existTagNameDom;
        }
        else {
            tagNameDom = document.createElement('div');
            tagNameDom.classList.add('tagName');
            tagNameDom.id = `${tagName}-tag`;
            tagNameDom.innerHTML = tagName;
            this.listBar.appendChild(tagNameDom);
        }
        return tagNameDom;
    }

    private updateHotspotGroupTagData() {
        const navigation = status.roamHotspot.hotspots
        // const couponTexts = await request.getAllCouponText()
        const findNavigation = (navigationId) => {
            return navigation.find((nav) => {
                return nav.id === navigationId;
            })
        }

        const getCoupon = (navigationId) => {
            if (this._couponTexts) {
                return this._couponTexts.filter((txts) => {
                    if (txts) {
                        return txts.navigationId === navigationId
                    }
                })
            }

        }

        const group = status.hotspotGroup.groups.map((group, groupIndex) => {
            let groupTag = { 'unTag': [] }
            group.hotspots.map((hotspot) => {
                const apiNav = hotspot
                const nav: tagNavigation = findNavigation(hotspot.hid)
                const tags = nav.tags
                const realNav = Object.assign(nav, apiNav)
                realNav.floor = group.desc

                realNav['coupon'] = getCoupon(realNav.id)

                if (tags && tags.length > 0) {
                    tags.forEach((tag) => {
                        if (tag != '红包') {
                            if (groupTag[tag] === undefined) {
                                groupTag[tag] = []
                            }
                            groupTag[tag].push(realNav)
                        }
                    })
                }
                else {
                    groupTag['unTag'].push(realNav)
                }
            })

            if (groupTag['unTag'].length === 0) {
                delete groupTag['unTag']
            }

            group['tags'] = groupTag
            return group
        });


        this.tagNavigations = group
        this.setDefaultGroup(2);
    }

    private convertNavigationInfo() {
        const navigation = status.roamHotspot.hotspots.map((navigation: tagNavigation) => {
            try {
                navigation.info = JSON.parse(navigation.position);

                this.processSpecialTags(navigation);
            }
            catch (err) {
                console.log(err);
            }
            return navigation;
        });

        if (this.redPacketHotspots.length > 0 && !!!this.redPacket) {
            this.redPacket = new RedPacket(this.redPacketHotspots)
        }
        return navigation;
    }

    private processSpecialTags(navigation: tagNavigation) {
        if (navigation.tags) {
            if (navigation.tags.includes('洗手间')) {
                this.washRoom.push(navigation);
            }
            if (navigation.tags.includes('母婴室')) {
                this.infantRoom.push(navigation);
            }
            if (navigation.tags.includes('红包')) {
                this.redPacketHotspots.push(navigation)
            }

        }
    }

    private setDefaultGroup(num: number) {
        const group = this.tagNavigations
        let defaultPosition = 0
        if (group && group.length > 0) {
            if (num < group.length) {
                defaultPosition = num
            }
            this.groupId = group[num].gid;
        }
    }

    roamGoButtonEvent() {
        this.roamGoButton.addEventListener('click', () => {
            status.isPressedTheOverlay = false
            status.isPressedBlinkButton = false
            status.skipMaterialVideo = true

            management.navigationOperate.convertToNavigator()
        })
    }

    roamBlinkButtonEvent() {
        this.roamBlinkButton.addEventListener('click', () => {
            stat.sendEventStat('navigation.jump', {
                src: status.currentHotspot.id,
                src_name: status.currentHotspot.desc,
                dst: status.endHotspot.id,
                dst_name: status.endHotspot.desc,
            })

            status.isPressedTheOverlay = false
            status.isPressedBlinkButton = true
            status.skipMaterialVideo = true

            management.searchBar.hide()
            pano.roamBlind()

            // flat.go()
        })
    }

    inputingEvent() {
        this.searchInputDom.addEventListener('input', () => {
            this.search(this.searchInputDom.value);
        })
    }

    search(value) {
        ui.unselectedSearchBar();
        this.currentInputPatt = new RegExp('.*' + value + '.*', 'gi');
        this.updateSelectList();
    }

    switchSearchBarEvent() {
        if (projectConfig.debug.enable !== true) {
            window.addEventListener('touchmove', (e) => {
                e.preventDefault();
            }, false)
        }


        this.searchInputDom.addEventListener('click', () => {
            this.updateSelectList()
            status.theRestOfTheBusiness = undefined

            panoStatus.stashNavigationWay(status.currentHotspot, status.endHotspot)
            management.searchBar.show()
        })

        this.closeButton.addEventListener('click', () => {
            status.theRestOfTheBusiness = undefined
            management.searchBar.hide()
        })
    }

    clearSelectListUI() {
        this.updateListItem()

        for (let d = 0; d < this.listItem.length; d++) {
            this.listItem[d].style.color = '#505050'
            this.listItem[d].style.fontWeight = '100'
            // this.listItem[d].style.background = 'none'
            this.listItem[d].style.border = '2px solid rgba(255, 66, 86, 0)'
        }
    }

    highlightSelectList(i) {
        i.style.color = '#fe423e'
        i.style.fontWeight = 'bold'
        // i.style.background = 'rgba(253, 205, 205, 0.4)'
        i.style.border = '2px solid rgba(255, 66, 86, 0.4)'

    }

    updateListItem() {
        this.listItem = <NodeListOf<HTMLDivElement>>document.querySelectorAll('.roam-select-item')
    }

    selectItemEvent() {
        if (this.listItem) {
            for (let d = 0; d < this.listItem.length; d++) {
                this.listItem[d].style.color = '#505050'
                this.listItem[d].style.fontWeight = '100'
            }
        }
    }

    tapItem(event) {
        this.clearSelectListUI()
        let ev = event || window.event
        let target = <HTMLSourceElement>ev.target || ev.srcElement

        if (target.classList.contains('item-text') ||
            target.classList.contains('item-poster')) {
            target = target.parentElement
        }
        if (!!this.panelHidden) {
            stat.sendEventStat('navigation.walk.start', {
                src: status.currentHotspot.id,
                src_name: status.currentHotspot.desc,
                dst: status.endHotspot.id,
                dst_name: status.endHotspot.desc,
            })
            pano.roamGo()
            management.beginnerGuider.checkAndGuidance(guiderType.startNavigationGuide)
            this.hide()
        } else if (target.classList.contains('roam-select-item')) {
            const id = target.getAttribute('data-value')
            const navigation = status.hotspotById(id)
            if (management.navigationOperate.modifyObject === 'final') {
                this.searchInputDom.blur()
                status.endHotspot = navigation
            } else {
                status.hotspotBeforePreview = status.currentHotspot
                status.currentHotspot = navigation
                this.hidePreviewPanel()
                management.navigationOperate.previewNavigation()
                management.navigationOperate.navigationInInput.blur()
            }

            this.searchInputDom.value = navigation.desc
            this.showGoAndBlinkButton()
            management.navigationOperate.showFinalText()
            this.highlightSelectList(target)
            management.getPath()
        }
    }

    addSelectItemAndFilterCurrentPosition(desDom, navigation) {
        const id = navigation.hid
        const name = navigation.desc
        const poster = navigation.poster
        const coupon = navigation.coupon

        let item = document.createElement('div')
        item.classList.add('roam-select-item')

        let itemPoster = document.createElement('img')
        itemPoster.classList.add('item-poster')
        itemPoster.src = poster
        if (!poster) {
            itemPoster.src = `images/final.png`
        }
        itemPoster.alt = name

        if (this.redPacket.containsNavigation(navigation)) {
            let redPacketImg = document.createElement('img')
            redPacketImg.classList.add('item-red-packet')
            redPacketImg.src = `images/red_packet_corner.png`
            item.appendChild(redPacketImg)
            itemPoster.classList.add('item-poster-with-red-packet')
        }

        item.appendChild(itemPoster)


        if (coupon) {
            var couponText = '&nbsp;'

            if (coupon.length > 0) {
                couponText = coupon[0].desc
                // coupon.forEach((c) => {
                //     couponText += c.desc
                // })
            }

            let itemText = document.createElement('div')
            itemText.classList.add('item-text')
            itemText.innerHTML = couponText

            item.appendChild(itemText)

        }

        item.setAttribute('data-value', id)
        item.setAttribute('data-name', name)

        desDom.appendChild(item)
        // 搜索栏的项目添加完成

    }

    selectGroupBarEvent() {
        this.searchBarContainer.addEventListener('click', (ev) => {
            let target = <HTMLDivElement>ev.target || ev.srcElement
            if (target.classList.contains('group-bar')) {
                this.groupId = target.getAttribute('data-group')
                if (this.groupId === 'washRoom' || this.groupId === 'infantRoom') {
                    const name = this.groupId
                    const spot = this[name]
                    if (spot && spot.length > 0) {
                        management.multipleNavigationToFindTheShortestPath(this[name])

                        stat.sendEventStat('navigation.walk.start', {
                            src: status.currentHotspot.id,
                            src_name: status.currentHotspot.desc,
                            dst: status.endHotspot.id,
                            dst_name: status.endHotspot.desc,
                        })

                        management.getPath()

                        this.hide()
                        pano.roamGo()
                        pano.doFindPathAngleRotate()
                        this.setDefaultGroup(2)
                    }
                } else {
                    this.updateSelectList();
                }

            }
        })
    }

    set groupId(id) {
        this.selectedSearchBar(id)
        this.resetSearch();
        this._groupId = id
    }

    private resetSearch() {
        this.searchInputDom.value = ''
        this.currentInputPatt = undefined;
    }

    get groupId() {
        return this._groupId
    }

    selectedSearchBar(target) {
        if (typeof target === 'object') {
            ui.selectedSearchBar(target)
        } else {
            const dom = document.querySelector(`.group-bar[data-group="${target}"]`)
            ui.selectedSearchBar(dom)
        }
    }

    show() {
        this.panel.style.display = 'block'
        this.navigateConsole.classList.add('navigate-console-open')
        management.removeAllOverlays()

        const guide = management.beginnerGuider.checkAndGuidance(guiderType.chooseStoreNameGuide)
        if (guide === false) {
            this.searchInputDom.blur()
        }
    }

    hide() {
        management.navigationOperate.convertToOrigin()
        this.showPreviewPanel()
        this.panel.style.display = 'none'
        this.resetSearch();
        this.hideGoAndBlinkButton()
        this.navigateConsole.classList.remove('navigate-console-open')
    }

    showPreviewPanel() {
        this.panel.classList.remove('panel-navigate-preview')
        this.panelHidden = false
    }

    hidePreviewPanel() {
        this.panel.classList.add('panel-navigate-preview')
        this.panelHidden = true
    }

    isHide() {
        if (this.panel.style.display === 'none') {
            return true
        } else {
            return false
        }
    }

    enable() {
        // this.switch.style.zIndex = '1001'
    }

    disable() {
        // this.switch.style.zIndex = '-10'
    }

    showGoAndBlinkButton() {
        if (this.roamBlinkButton.style.display === '' ||
            this.roamBlinkButton.style.display === 'none') {
            this.roamGoButton.style.display = 'block'
            this.roamBlinkButton.style.display = 'block'

            this.roamGoButton.classList.add('slideInRight')
            this.roamBlinkButton.classList.add('slideInRight')
            this.closeButton.classList.add('slideInRight')


            this.roamGoButton.classList.add('animated')
            this.roamBlinkButton.classList.add('animated')
            this.closeButton.classList.add('animated')


            this.roamGoButton && this.roamGoButton.addEventListener('animationend', () => {
                this.roamGoButton.classList.remove('slideInRight');
                this.roamGoButton.classList.remove('animated');
            });

            this.roamBlinkButton && this.roamBlinkButton.addEventListener('animationend', () => {
                this.roamBlinkButton.classList.remove('slideInRight');
                this.roamBlinkButton.classList.remove('animated');
            });

            this.closeButton && this.closeButton.addEventListener('animationend', () => {
                this.closeButton.classList.remove('slideInRight');
                this.closeButton.classList.remove('animated');
            });
        }
    }

    hideGoAndBlinkButton() {
        this.roamGoButton.style.display = 'none'
        this.roamBlinkButton.style.display = 'none'
    }

    setCouponTexts(couponTexts) {
        this._couponTexts = couponTexts
    }

    getCouponTexts() {
        return this._couponTexts
    }

    blurInputDom() {
        this.searchInputDom.blur()
        management.navigationOperate.navigationInInput.blur()
    }
}
