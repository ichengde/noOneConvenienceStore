
export class frontLike {
    dom: HTMLImageElement = <HTMLImageElement>document.querySelector('#frontLike')
    like: boolean = false
    scaleNum: number = 1
    constructor() {
        this.dom.addEventListener('touchend', () => {
            if (this.like === false) {
                this.dom.src = './images/front-like-after.png'
                this.like = true
            } else {
                this.dom.src = './images/front-like.png'
                this.like = false
            }
            this.scaleNum = 1
            this.dom.style.transform = `scale(${this.scaleNum})`
        })

        this.dom.addEventListener('touchstart', () => {
            if (this.scaleNum < 2) {
                this.scaleNum = this.scaleNum + 0.2
                console.log(`scale(${this.scaleNum})`)
                this.dom.style.transform = `scale(${this.scaleNum})`
            }
        })

    }
}
