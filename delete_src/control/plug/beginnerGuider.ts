import { guiderType } from '../util/type'
import { commonUtil } from '../util/util'
import * as Cookies from "js-cookie"
export class beginnerGuider {
    firstScreenGuide = false
    chooseStoreNameGuide = false
    startNavigationGuide = false

    dom = <HTMLImageElement>document.querySelector('#beginnerGuider')
    guidanceType: guiderType
    constructor() {
        const type = ['firstScreenGuide', 'chooseStoreNameGuide', 'startNavigationGuide']
        type.forEach(this.getPreviousInfo.bind(this))

        this.dom.addEventListener('click', () => {
            if (this.guidanceType) {
                this.finish(this.guidanceType)
                this.dom.classList.remove('beginnerGuider-show-ios')
                this.dom.classList.remove('beginnerGuider-show-android')
                this.dom.style.display = 'none'
            }
        })
    }
    private getPreviousInfo(name) {
        const f = Cookies.get(name);
        if (f !== undefined) {
            this[name] = this.string2bool(f);
            Cookies.set(name, f, { expires: 90 });
        } else {
            Cookies.set(name, 'false', { expires: 90 });
        }
    }

    string2bool(str: string) {
        str = str.toLowerCase()
        if (str === 'true') {
            return true
        } else {
            return false
        }
    }
    checkAndGuidance(name) {
        let flag = this[name]
        if (this[name] !== undefined && this[name] === false) {
            this.guidanceType = name
            this.dom.src = `./images/${name}.png`

            this.dom.onload = () => {
                if (commonUtil.isIOS()) {
                    this.dom.classList.add('beginnerGuider-show-ios')
                } else {
                    this.dom.classList.add('beginnerGuider-show-android')
                }

                this.dom.style.display = 'block'
            }

            return true
        } else {
            return false
        }
    }

    finish(name) {
        this[name] = true
        Cookies.set(name, 'true', { expires: 90 });

    }
    reset() {
        this.firstScreenGuide = false
        this.chooseStoreNameGuide = false
        this.startNavigationGuide = false
        Cookies.set('startNavigationGuide', 'false', { expires: 90 });
        Cookies.set('chooseStoreNameGuide', 'false', { expires: 90 });
        Cookies.set('firstScreenGuide', 'false', { expires: 90 });
    }
}