
import { management } from '../base/roamManagement'
import { projectConfig } from '../../config'
import { tagNavigation, guiderType } from '../util/type'
import { stat } from '../util/stat'

class videoControl {

    videoPanel: HTMLVideoElement = document.querySelector('#liveIntroductionVideo')
    choicePanel: HTMLDivElement = document.querySelector('.video-control')
    skipVideoButton: HTMLDivElement = document.querySelector('#skipControl')

    constructor() {
        this.skipVideoButton && this.skipVideoButton.addEventListener('click', this.skipVideo.bind(this))
        this.choicePanel && this.choicePanel.addEventListener('click', (e) => {
            const choiceDom = e.srcElement
            const choice = choiceDom.getAttribute('data-choice')

            if (choice.toString() === 'one') {
                stat.sendEventStat('function.introduction.click', {
                    floor: '1'
                })
                this.playVideo(`//ccc.zhixianshi.com/${projectConfig.roamId}/video/first.mp4`)
            } else if (choice.toString() === 'fifth') {
                stat.sendEventStat('function.introduction.click', {
                    floor: '5'
                })
                this.playVideo(`//ccc.zhixianshi.com/${projectConfig.roamId}/video/fifth.mp4`)
            }
        })
        window.addEventListener('touchstart', (e) => {
            const choiceDom = e.srcElement
            const choice = choiceDom.getAttribute('data-choice')
            if (!choice) {
                this.hideChoicePanel()
            }
        })

        this.BindVideoEvent();
    }

    private BindVideoEvent() {
        this.videoPanel.addEventListener('ended', () => {
            this.skipVideo();
        });
        this.videoPanel.addEventListener('pause', () => {
            this.skipVideo();
        });
    }

    playVideo(src?: string) {
        this.hideChoicePanel()
        this.videoPanel.src = src
        this.makeWideWidth()
        this.videoPanel.style.zIndex = '2000'
        this.videoPanel.style.display = 'block'
        this.videoPanel.play();
        this.showSkip()
    }

    skipVideo() {
        this.videoPanel.style.zIndex = '0'
        this.videoPanel.style.display = 'none'
        this.videoPanel.pause()
        this.hideSkip()
    }

    makeWideWidth() {
        this.videoPanel.style.width = '100%'
        this.videoPanel.style.transform = 'translateX(0%) translateY(-50%)'
        this.videoPanel.style.height = 'auto'
        this.videoPanel.style.minHeight = '150px'
        this.videoPanel.style.minWidth = '300px'
        this.videoPanel.style.left = '0px'
        this.videoPanel.style.top = '50%'
    }

    showChoicePanel() {
        if (this.choicePanel) {
            this.choicePanel.style.display = 'block'
            this.choicePanel.style.zIndex = '1001'
        }
    }
    hideChoicePanel() {
        if (this.choicePanel) {
            this.choicePanel.style.display = 'none'
            this.choicePanel.style.zIndex = '0'
        }
    }

    showSkip() {
        this.skipVideoButton.style.display = 'block'
    }

    hideSkip() {
        this.skipVideoButton.style.display = 'none'
    }
}

class recommended {

    overlayList = []
    convert(mt) {
        const param = {
            image: mt.text,
            longitude: mt.startLongitude,
            latitude: mt.startLatitude,
            sizeScale: mt.sizeScale,
            layer: mt.layer
        }
        const newOverlay = management.vr.overlay(param)
        newOverlay.event = () => {
            this.clear()
        }
        management.vr.addOverlay(newOverlay)
        this.overlayList.push(newOverlay)
    }
    clear() {
        this.overlayList.forEach((ov) => {
            management.vr.removeOverlay(ov)
        })
    }
    show() {
        this.clear()
        let paramClose = {
            overlayID: 'close',
            type: 5,
            text: 'close',
            displayMode: 2
        }
        const longitude = management.vr.getLongitude() / Math.PI * 180
        let width = 90
        let delta = 20
        let param1 = {
            overlayID: 'anniversary1',
            text: './images/anniversary1.jpg',
            startLongitude: longitude - width - delta,
            sizeScale: 1.8,
            startLatitude: 0,
            layer: 0,
            type: 1,
            group: 'anniversary1',
        }
        let param2 = {
            overlayID: 'anniversary2',
            text: './images/anniversary2.jpg',
            startLongitude: longitude - delta,
            startLatitude: 0,
            sizeScale: 1.8,
            layer: 0,
            type: 1,
            group: 'anniversary1',
        }
        let param3 = {
            overlayID: 'anniversary3',
            text: './images/anniversary3.jpg',
            sizeScale: 1.8,
            startLongitude: longitude + width - delta,
            startLatitude: 0,
            layer: 0,
            type: 1,
            group: 'anniversary1',
        }
        let param4 = {
            overlayID: 'anniversary4',
            text: './images/anniversary4.jpg',
            sizeScale: 1.8,
            startLongitude: longitude + width * 2 - delta,
            startLatitude: 0,
            layer: 0,
            type: 1,
            group: 'anniversary1',
        }
        this.convert(param1)
        this.convert(param2)
        this.convert(param3)
        this.convert(param4)
    }
}

export class baby {
    babyHead: HTMLImageElement = <HTMLImageElement>document.querySelector('#baby-head')

    menuBabyHead: HTMLImageElement = <HTMLImageElement>document.querySelector('#baby-content-head')
    menuBabyBottom: HTMLImageElement = <HTMLImageElement>document.querySelector('#baby-content-bottom')

    menu: HTMLImageElement = <HTMLImageElement>document.querySelector('.baby-content')
    menuButtons = document.querySelectorAll('.baby-menu-button')

    videoControl = new videoControl()
    recommended = new recommended()

    constructor() {
        this.bindBabyControl()
        this.bindMenuControl()
        this.bindMenuButtonEvent()
    }

    private bindMenuControl() {
        const hideBodyShowHead = () => {
            this.showBabyHead();
        }

        this.menuBabyBottom && this.menuBabyBottom.addEventListener('click', hideBodyShowHead);
        this.menuBabyHead && this.menuBabyHead.addEventListener('click', hideBodyShowHead);

        this.menu && this.menu.addEventListener('animationend', () => {
            this.babyHead.classList.remove('fadeInLeft');
            this.babyHead.classList.remove('animated');
        });
    }

    private showBabyHead() {
        this.menu.style.display = 'none';
        this.babyHead.style.display = 'block';
        this.babyHead.classList.add('fadeInLeft');
        this.babyHead.classList.add('animated');
    }

    private bindBabyControl() {
        this.babyHead && this.babyHead.addEventListener('click', () => {
            this.showBabyBody();
        });

        this.babyHead && this.babyHead.addEventListener('animationend', () => {
            this.menu.classList.remove('flipInY');
            this.menu.classList.remove('animated');
        });

    }

    private showBabyBody() {
        this.babyHead.style.display = 'none';
        this.menu.style.display = 'block';
        this.menu.classList.add('flipInY');
        this.menu.classList.add('animated');
    }

    bindMenuButtonEvent() {
        const menuDom = document.querySelector('.baby-menu')

        const unselectAll = (nodeOfDom) => {
            for (let index = 0; index < nodeOfDom.length; index++) {
                const element = nodeOfDom[index];
            }
        }

        menuDom && menuDom.addEventListener('click', (e) => {

            const buttons = document.querySelectorAll('.baby-menu-button')
            unselectAll(buttons)

            const dimDom: HTMLDivElement = <HTMLDivElement>e.srcElement

            if (dimDom.classList.contains('baby-menu-button')) {
                const myButton: HTMLDivElement = <HTMLDivElement>e.srcElement

                const action = dimDom.getAttribute('id')
                if (action === 'navigateToFindstores') {
                    stat.sendEventStat('function.click', {
                        function: 'navigation',
                        description: '导航'
                    })

                    management.searchBar.updateSelectList()

                    management.searchBar.show()
                }

                if (action === 'liveIntroduction') {
                    stat.sendEventStat('function.click', {
                        function: 'introduction',
                        description: '真人介绍'
                    })
                    this.videoControl.showChoicePanel()
                }

                if (action === 'beginnerGuide') {
                    stat.sendEventStat('function.click', {
                        function: 'guide',
                        description: '新手引导'
                    })
                    management.beginnerGuider.reset()
                    management.beginnerGuider.checkAndGuidance(guiderType.firstScreenGuide)
                }

                if (action === 'recommendedShop') {
                    stat.sendEventStat('function.click', {
                        function: 'mall_ad',
                        description: '全店推荐'
                    })
                    this.recommended.show()
                }

                this.showBabyHead()
            }
        })
    }

}
