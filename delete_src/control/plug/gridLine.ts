
export class gridLine {
    item: HTMLDivElement
    topOffset: number
    constructor() { }

    getContainerSize(container) {
        return container.getBoundingClientRect()

    }

    toPaint(container, topOffset) {
        this.topOffset = topOffset
        this.paintColLine(container)
        this.paintRowLine(container)
    }

    paintColLine(container: HTMLDivElement) {
        let lineString = ''
        this.item = document.querySelector('.roam-select-item')
        if (container && this.item) {
            const containerInfo = this.getContainerSize(container)
            const containerWidth = containerInfo.width
            const colWidth = this.item.getBoundingClientRect().width

            const colCount = containerWidth / colWidth - 1

            for (let i = 0; i < colCount-1; i++) {

                const line = document.createElement('div')
                line.classList.add('colLine')
                const left = `${colWidth * (i + 1)}px`

                const heightOffset = 20
                const top = `${this.topOffset + heightOffset}px`;
                const height = `${containerInfo.height - 2 * heightOffset}px`;


                lineString += `<div class="colLine" style="left:${left};top:${top};height:${height}"></div>`

                // container.appendChild(line)
                container.insertAdjacentHTML('beforeend', lineString)
            }
        }

    }


    paintRowLine(container) {
        let lineString = ''
        this.item = document.querySelector('.roam-select-item')
        if (container && this.item) {
            const containerInfo = this.getContainerSize(container)
            const containerHeight = containerInfo.height
            const colHeight = this.item.getBoundingClientRect().height

            const colCount = containerHeight / colHeight - 1

            for (let i = 0; i < colCount; i++) {

                const line = document.createElement('div')
                line.classList.add('rowLine')
                const top = `${this.topOffset + colHeight * (i + 1)}px`
                const widthOffset = 20
                const left = `${widthOffset}px`;
                const width = `${containerInfo.width - 2 * widthOffset}px`;

                lineString += `<div class="rowLine" style="left:${left};top:${top};width:${width}"></div>`

                // container.appendChild(line)
                container.insertAdjacentHTML('beforeend', lineString)
            }
        }

    }


    clear() {
        const lines = document.querySelectorAll('.line')
        for (let i = 0; i < lines.length; i++) {
            lines[i].parentElement.removeChild(lines[i])
        }
    }
}
