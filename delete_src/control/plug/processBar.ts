import { management } from '../base/roamManagement'
import { ui } from '../ui/uiControl'
import { status } from '../base/roamStatus'
import { LiteEvent } from '../util/util'

export class processBar {
    playPauseControlButton: HTMLImageElement = <any>document.querySelector('#playPauseControlButton')
    stopControlButton: HTMLImageElement = <any>document.querySelector('#stopControlButton')
    clickedPlay = new LiteEvent()
    clickedStop = new LiteEvent()

    constructor() {
        this.playPauseControlButton.addEventListener('click', () => {
            console.log('%c cilck', 'color: red', status.playing)
            if (status.playing) {
                // flat.makeItPause()
                this.playPauseControlButton.src = 'images/play.png'
            } else {
                // flat.makeItPlay(true)
                this.playPauseControlButton.src = 'images/pause.png'
            }
        })
    }

    update() {
        if (status.playing) {
            this.playPauseControlButton.src = 'images/pause.png'
        } else {
            this.playPauseControlButton.src = 'images/play.png'
        }
    }


    show() {
        ui.showOrHideStopButton(false)
        ui.showOrHidePlayButton(true)
        status.playing = true
    }

    hide() {
        ui.showOrHideStopButton(false)
        ui.showOrHidePlayButton(false)
        status.playing = false
    }
}