

import { management } from '../base/roamManagement'
import { ui } from '../ui/uiControl'
import { status } from '../base/roamStatus'
import { displayType } from '../util/type'

export class walkAway {
    switch = <HTMLDivElement>document.querySelector('#walkAway')
    awayContainer = <any>document.querySelector('.away-container')
    mainMenu = <any>document.querySelector('.main-walker-menu')
    topMenu = document.querySelector('.choice-walker')
    awayFloor = document.querySelector('.choice-walker>.floor')
    bg = <HTMLDivElement>document.querySelector('.main-walker-menu')

    constructor() {
        this.enable()
        this.bindWalkAwayBtnEvent()
        this.update()
    }

    private enable(): void {
        this.switch.style.display = 'inline'
    }

    private update() {
        status.hotspotGroup.groups.forEach((group) => {
            // walk away mode
            if (this.awayFloor) {
                let frame = document.createElement('div')
                frame.setAttribute('gid', group.gid)
                frame.classList.add('frame')
                let box = document.createElement('div')
                box.classList.add('box')
                let img = document.createElement('img')
                img.src = group.poster
                let textBox = document.createElement('div')
                textBox.classList.add('textBox')
                let text = document.createElement('div')
                text.innerText = group.desc
                this.bg.style.display = 'flex'
                box.appendChild(img)
                box.appendChild(textBox)
                textBox.appendChild(text)
                frame.appendChild(box)
                let that = this
                frame.addEventListener('click', function () {
                    that.mainMenu.style.display = 'flex'
                    let gid = this.getAttribute('gid')
                    status.hotspotGroup.groups.forEach(function (group) {
                        if (gid === group.gid) {
                            let container = document.querySelector('.wrap')
                            container.innerHTML = ''
                            group.hotspots.forEach(function (h) {
                                if (h.hide !== true) {
                                    let hFrame = document.createElement('div')
                                    hFrame.setAttribute('data-id', h.hid)
                                    hFrame.classList.add('line-frame')
                                    let box = document.createElement('div')
                                    box.classList.add('box')
                                    let img = document.createElement('img')
                                    img.src = h.poster

                                    let textBox = document.createElement('div')
                                    textBox.classList.add('textBox')

                                    let text = document.createElement('div')
                                    text.innerText = h.desc

                                    box.appendChild(img)
                                    box.appendChild(textBox)
                                    textBox.appendChild(text)

                                    hFrame.appendChild(box)
                                    hFrame.addEventListener('click', function () {

                                        if (!status.currentHotspot) {
                                            let triggerHideDom = <HTMLDivElement>document.querySelector('.trigger-image')
                                            triggerHideDom.style.display = 'none'
                                            status.displayMode = displayType.panoramaPicture
                                        }

                                        status.skipMaterialVideo = true
                                        status.isPressedTheOverlay = false
                                        status.isPressedBlinkButton = true
                                        management.searchBar.hide()

                                        status.endHotspot = status.hotspotById(this.getAttribute('data-id'))
                                        console.log('hFrame', this.getAttribute('data-id'), 'status.endHotspot', status.endHotspot)
                                        // flat.go()


                                    })
                                    container.appendChild(hFrame)
                                }
                            })
                        }
                    })
                })
                this.awayFloor.appendChild(frame)
            }
        })
    }

    private bindWalkAwayBtnEvent() {
        this.switch.addEventListener('click', () => {
            if (this.awayContainer.style.display === 'none' || this.awayContainer.style.display === '') {
                management.searchBar.hide()
                this.show()
            } else {
                this.hide()
            }
        })
    }

    show() {
        this.awayContainer.style.display = 'block'
        this.mainMenu.style.display = 'none'
    }
    hide() {
        this.mainMenu.style.display = 'none'
        this.awayContainer.style.display = 'none'

    }
}
