
import { projectConfig } from '../../config'
import * as Cookies from "js-cookie"
import * as CryptoJS from "crypto-browserify"
import { commonUtil } from '../util/util'
import { request } from '../base/requestClient'
import { moveStatusType, tagNavigation } from '../util/type'
import { management } from '../base/roamManagement'
import { stat } from '../util/stat'

declare var wx
const oneMinute = 1 / 24 / 60;

interface infoType {
    id: string,
    name: string,
    pic: string
}



// document.querySelector('head').innerHTML += '<script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>'
// 异步请求 跨域问题 只能自行插入在主页面中
export class wechatApi {
    private _link = location.origin + location.pathname
    debug = false

    private _longitude: number
    private _latitude: number
    serverLink: string
    loginLink: string
    baseLink: string

    _title: string

    navigationInfo: infoType = {
        id: '',
        name: '',
        pic: ''
    }

    shareTemplate: any
    constructor(debugModel) {
        this.debug = debugModel;
        this.baseLink = location.origin + location.pathname;

        this.bindCopyEvent();

    }

    private bindCopyEvent() {
        const panel = document.querySelector('#vr-panel');
        panel.addEventListener('click', this.refresh.bind(this));
        window.addEventListener('click', this.refresh.bind(this));
    }
    copy(text) {

        let inputDom = <HTMLInputElement>document.createElement('input')
        inputDom.id = 'inputForCopyDom'
        document.body.appendChild(inputDom)

        const originValue = inputDom.value
        inputDom.value = text;
        var isiOSDevice = navigator.userAgent.match(/ipad|iphone/i);

        if (isiOSDevice) {

            var editable = inputDom.contentEditable;
            var readOnly = inputDom.readOnly;

            inputDom.contentEditable = 'true';
            inputDom.readOnly = true;

            var range = document.createRange();
            range.selectNodeContents(inputDom);

            var selection = window.getSelection();
            selection.removeAllRanges();
            selection.addRange(range);

            inputDom.setSelectionRange(0, 999999);
            inputDom.contentEditable = editable;
            inputDom.readOnly = true;

        }

        document.execCommand('copy');

        inputDom.value = originValue;
        inputDom.parentElement.removeChild(inputDom)
    }
    refresh(event?: Event) {
        let params
        let site

        const navigationId = this.navigationInfo.id
        if (navigationId) {
            params = this.getParams(navigationId);
            this.copy(params);
            site = this.baseLink + `#` + encodeURIComponent(params)
        } else {
            site = this.baseLink
        }
        console.log(params)

        window.history.replaceState({}, '', site)

        if (event) {
            const clickDom = <HTMLInputElement>event.srcElement
            let okList = ['roam-select-input', 'roam-select-input-block', 'search-section']
            if (okList.includes(clickDom.id)) {
                const inputDom = <HTMLInputElement>document.querySelector('#roam-select-input')
                inputDom.focus()
            }
        }
    }

    register() {
        this.requestTicket('jsapi').then((ticket) => {
            let { timestamp, nonceStr, signature } = this.getSignture(ticket);
            let roamResp = projectConfig.loadInfo

            wx.config({
                debug: this.debug,
                appId: projectConfig.wechat_app_id, // 必填，公众号的唯一标识
                timestamp: timestamp, // 必填，生成签名的时间戳
                nonceStr: nonceStr, // 必填，生成签名的随机串
                signature: signature, // 必填，签名，见附录1
                jsApiList: ['onMenuShareAppMessage', 'onMenuShareTimeline', 'onMenuShareQQ', 'onMenuShareWeibo', 'onMenuShareQZone', 'addCard', 'chooseCard'] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
            })

            wx.ready(() => {
                wx.onMenuShareTimeline({
                    title: roamResp.name, // 分享标题
                    link: this.shareLink, // 分享链接
                    imgUrl: roamResp.poster, // 分享图标
                    trigger: () => {

                    },
                    success: function () {
                        // 用户确认分享后执行的回调函数
                    },
                    cancel: function () {
                        // 用户取消分享后执行的回调函数
                    }
                })

                wx.onMenuShareAppMessage({
                    title: roamResp.name, // 分享标题
                    desc: roamResp.desc, // 分享描述
                    link: this.shareLink, // 分享链接
                    imgUrl: roamResp.poster, // 分享图标
                    type: 'link', // 分享类型,music、video或link，不填默认为link
                    dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
                    trigger: () => {

                    },
                    success: function () {
                        // 用户确认分享后执行的回调函数
                    },
                    cancel: function () {
                        // 用户取消分享后执行的回调函数
                    }
                })

                wx.onMenuShareQQ({
                    title: roamResp.name, // 分享标题
                    desc: roamResp.desc, // 分享描述
                    link: this.shareLink, // 分享链接
                    imgUrl: roamResp.poster, // 分享图标
                    trigger: () => {

                    },
                    success: function () {
                        // 用户确认分享后执行的回调函数
                    },
                    cancel: function () {
                        // 用户取消分享后执行的回调函数
                    }
                })

                wx.onMenuShareWeibo({
                    title: roamResp.name, // 分享标题
                    desc: roamResp.desc, // 分享描述
                    link: this.shareLink, // 分享链接
                    imgUrl: roamResp.poster, // 分享图标
                    trigger: () => {

                    },
                    success: function () {
                        // 用户确认分享后执行的回调函数
                    },
                    cancel: function () {
                        // 用户取消分享后执行的回调函数
                    }
                })

                wx.onMenuShareQZone({
                    title: roamResp.name, // 分享标题
                    desc: roamResp.desc, // 分享描述
                    link: this.shareLink, // 分享链接
                    imgUrl: roamResp.poster, // 分享图标
                    trigger: () => {

                    },
                    success: function () {
                        // 用户确认分享后执行的回调函数
                    },
                    cancel: function () {
                        // 用户取消分享后执行的回调函数
                    }
                })
            })

        })




    }

    set shareLink(s: string) {
        this._link = s
    }

    get shareLink(): string {
        const myPosId = Cookies.get('located')
        if (myPosId) {
            let params = this.getParams(myPosId);
            this._link = `${this.baseLink}#${params}`
        } else {
            this._link = this.baseLink
        }

        return this._link
    }

    private getParams(myPosId: any) {
        let params = {
            located: myPosId,
            name: this.navigationInfo.name,
            title: this._title,
        }

        return JSON.stringify(params);
    }

    set longitude(s: number) {
        Cookies.set('longitude', s, {
            expires: oneMinute * 5
        })
        this._longitude = s
    }

    set latitude(s: number) {
        Cookies.set('latitude', s, {
            expires: oneMinute * 5
        })
        this._latitude = s
    }

    get longitude(): number {
        let tmp = Cookies.get('longitude')
        if (!tmp) {
            tmp = this._longitude
            tmp = commonUtil.getQueryParameter('longitude')
        }
        return tmp;
    }

    get latitude(): number {
        let tmp = Cookies.get('latitude')
        if (!tmp) {
            tmp = this._latitude
            tmp = commonUtil.getQueryParameter('latitude')
        }
        return tmp;
    }

    logErrFunc(error) {
        console.log(error)
    }

    login() {
        this.serverLink = location.protocol + `//dev.zhixianshi.com/v1/messaging/wechat-auth?successRedirectURL=${this.shareLink}`
        this.loginLink = `https://open.weixin.qq.com/connect/oauth2/authorize?appid=${projectConfig.LOGIN_WECHAT_APPID}&redirect_uri=${this.serverLink}&response_type=code&scope=snsapi_base&state=123&connect_redirect=1#wechat_redirect`

        window.location.href = this.loginLink
    }

    chooseCard() {
        this.requestTicket('wx_card').then((ticket) => {
            let { timestamp, nonceStr, signature } = this.getSignture(ticket);
            wx.chooseCard({
                timestamp: timestamp, // 卡券签名时间戳
                nonceStr: nonceStr, // 卡券签名随机串
                signType: 'sha1', // 签名方式，默认'SHA1'
                cardSign: signature, // 卡券签名

                success: function (res) {
                    var cardList = res.cardList; // 用户选中的卡券列表信息
                    console.log(res)
                }

            });
        })
    }


    addCard(cardId) {
        var statPayload = {}
        let coupons = management.searchBar.getCouponTexts()
        if (!!coupons) {
            for (var i = 0; i < coupons.length; i++) {
                let coupon = coupons[i]
                if (coupon.wechatCardId === cardId) {
                    statPayload = {
                        id: coupon.customId,
                        name: coupon.desc,
                        location: coupon.navigationId,
                        store: coupon.navigationName,
                    }

                    break
                }
            }
        }

        stat.sendEventStat('store.coupon.click', statPayload)

        this.requestTicket('wx_card').then((ticket) => {
            let { timestamp, nonceStr, signature } = this.getSignture(ticket, cardId);

            wx.addCard({
                cardList: [{
                    cardId: cardId,
                    cardExt: JSON.stringify({
                        timestamp: timestamp,
                        nonce_str: nonceStr,
                        signature: signature
                    })
                }],
                success: function (res) {
                    stat.sendEventStat('store.coupon.add', statPayload)
                    let cardList = res.cardList
                    const encode = res.cardList[0].code
                    request.addedCouponToRequestCode(encode, cardId).then((ans) => {
                        console.log(JSON.stringify(ans))
                    })
                },
                fail: function (res) {
                    console.error(res)
                }
            });
        })
    }

    private requestTicket(type) {
        return new Promise((resolve, reject) => {

            let wxCardTicketRequest = new XMLHttpRequest()

            wxCardTicketRequest.open('get', window.location.protocol + '//api.zhixianshi.com/v3/wechat/ticket?type=' + type)
            wxCardTicketRequest.setRequestHeader('Content-type', 'application/json')
            wxCardTicketRequest.onload = () => {
                if (wxCardTicketRequest.status >= 200 && wxCardTicketRequest.status < 400) {
                    let resp = JSON.parse(wxCardTicketRequest.responseText)
                    let ticket = resp.ticket;

                    resolve(ticket)
                } else {
                    reject()
                }
            }

            wxCardTicketRequest.onerror = this.logErrFunc
            wxCardTicketRequest.send()
        }).catch((err) => {
            console.log(`get ticket fail`, err)
        })
    }

    private getSignture(ticket: any, cardId?: any) {
        let timestamp = parseInt((Date.now() / 1000).toString());
        let nonceStr = Math.random().toString(36).substring(2);
        let signature

        if (cardId) {
            let args = [{
                name: 'api_ticket', val: ticket
            }, {
                name: 'timestamp', val: timestamp
            }, {
                name: 'card_id', val: cardId
            }, {
                name: 'nonce_str', val: nonceStr
            }];

            args = args.sort((e, t) => {
                if (e.val.toString() < t.val.toString()) {
                    return -1;
                }
                if (e.val.toString() >= t.val.toString()) {
                    return 1;
                }
                return 0;
            });
            let c = "";
            for (let i in args)
                c += args[i].val;
            signature = CryptoJS.createHash('sha1').update(c).digest('hex');
        } else {
            let con = 'jsapi_ticket=' + ticket + '&noncestr=' + nonceStr + '&timestamp=' + timestamp + '&url=' + this.shareLink
            signature = CryptoJS.createHash('sha1').update(con).digest('hex')
        }


        return { timestamp, nonceStr, signature };
    }

    setNavigationInfo(moveStatus: moveStatusType, navigation?) {
        if (this.shareTemplate) {
            if (moveStatus === moveStatusType.portal) {
                const portalTemplate = this.shareTemplate['portal']['template']
                this._title = portalTemplate
            }

            if (moveStatus === moveStatusType.way || moveStatus === moveStatusType.navigation) {
                const navigationTemplate = this.shareTemplate[moveStatus]['template']
                const params = this.shareTemplate[moveStatus]['values']

                let slogan = navigationTemplate
                if (navigation) {
                    slogan = slogan.replace('__0__', navigation.desc)
                }

                if (navigation.tags && navigation.tags.includes('红包')) {
                    slogan = slogan.replace('__1__', '红包')
                }

                for (let i = 0; i < params.length; i++) {
                    slogan = slogan.replace('__' + i + '__', params[i])
                }

                this._title = slogan
            }
        }

        if (navigation) {
            this.navigationInfo = {
                id: navigation.id,
                name: navigation.desc,
                pic: navigation.poster
            }

            if (navigation.tags && (navigation.tags.includes('洗手间') || navigation.tags.includes('母婴室'))) {
                this.navigationInfo = {
                    id: 'portal',
                    name: 'portal',
                    pic: navigation.poster
                }
                this._title = this.shareTemplate['portal']['template']
            }
        }
        this.refresh()
    }


    getShareTemplate() {
        return request.getShareTemplate().then((tem) => {
            this.shareTemplate = tem

            const defaultTitle = tem['portal']['template']
            this._title = defaultTitle
            return Promise.resolve()
        })
    }

}
