
import { commonUtil } from './util'
import { request } from '../base/requestClient'
import { projectConfig } from '../../config'
import { welcomeAnimationValidation } from './type'
// import * as VConsole from 'vconsole'

export class debug {
    enable: boolean = false
    dom = <HTMLElement>document.querySelector('.debug-block')
    videoSrc = [{
        origin: 'http://video.ali.inreal.tv/baoan_tianhong_5floor/path/mid/flatten/',
        path: 'http://10.10.10.108:8182/path/mid/flatten/'
    }, {
        origin: 'http://video.ali.inreal.tv/baoan_tianhong_5floor/path/flatten/',
        path: 'http://10.10.10.108:8182/path/mid/flatten/'
    }]
    picSrc = [{
        origin: 'http://video.ali.inreal.tv/baoan_tianhong_5floor/snapshot/high/',
        path: 'http://10.10.10.108:8182/snapshot/high/'
    }, {
        origin: 'http://video.ali.inreal.tv/tianhong_v2/snapshot/high/',
        path: 'http://10.10.10.108/snapshot/high/'
    }]
    constructor() {

        if (commonUtil.getQueryParameter('debug') === '') {
            this.enable = false
        } else {
            this.enable = true
        }

        if (this.enable) {
            console.log('%c此为debug版本', 'color:red')
            this.dom.style.display = 'block'

            // let vConsole = new VConsole()
        }

    }

    videoSrcChange(videos) {
        let origin = 'path/flatten'
        let path = 'path/mid/flatten'

        if (projectConfig.loadInfo.welcomeAnimation) {
            if (projectConfig.loadInfo.welcomeAnimation.tempPath) {
                path = projectConfig.loadInfo.welcomeAnimation.tempPath
            }
        }


        let v = videos.map((video) => {
            let real = video.replace(origin, path)

            if (projectConfig.debug.enable) {
                this.videoSrc.forEach((src) => {
                    real = real.replace(src.origin, src.path)
                })
            }
            return real
        })

        return v

    }


    picChange(pic) {
        let real = pic

        if (projectConfig.debug.enable) {
            this.picSrc.forEach((src) => {
                real = real.replace(src.origin, src.path)
            })

        }

        console.log(real)
        return real
    }

    entranceChange(welcomeAnimation: welcomeAnimationValidation): welcomeAnimationValidation {
        let t = welcomeAnimation

        if (t.loading) {
            t.loading = `${projectConfig.detailInFo.detail.materialURI}/${t.loading}`
        } else {
            t.loading = 'https://mall.zhixianshi.com/baoan_tianhong_5floor/image/h5logo.png'
        }

        if (t.trigger) {
            if (t.trigger.background) {
                t.trigger.background.src = `${projectConfig.detailInFo.detail.materialURI}/${t.trigger.background.src}`
            }

            if (t.trigger.button) {
                t.trigger.button = t.trigger.button.map((b) => {
                    b.src = `${projectConfig.detailInFo.detail.materialURI}/${b.src}`
                    return b
                })
            }
        }

        return t
    }


    private testFun() {

        const testTianHongRedEnvelope = document.querySelector('#testTianHongRedEnvelope');
        testTianHongRedEnvelope && testTianHongRedEnvelope.addEventListener('click', () => {
            request.getRedPacketCount().then((res) => {
                return Promise.resolve()
            }).catch((e) => {
            })
        });

        const testLogin = document.querySelector('#testLogin');
        testLogin && testLogin.addEventListener('click', () => {
            projectConfig.wechatApi.login();
        });

        const testAddCard = document.querySelector('#testAddCard');
        testAddCard && testAddCard.addEventListener('click', () => {
            projectConfig.wechatApi.addCard('p0wo90aviWYMcm7wQ2vqGhg-DQ98')
        });

        const testChooseCard = document.querySelector('#testChooseCard');
        testChooseCard && testChooseCard.addEventListener('click', () => {
            projectConfig.wechatApi.chooseCard()
        });
    }
}
