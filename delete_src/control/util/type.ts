
export interface projectConfigValidation {
    publicKey: string,
    isVrMode: boolean,
    apiSite: string,
    roamId: string,
    detailInFo: detailInfoValidation,
    videoChosen: videoQuality,
}

export interface hotspotAndOverlayValidation {
    hotspot: hotspotValidation,
    // overlays: Array<any>
}

export enum materialType {
    TEXT_MATERIAL = 0,
    IMAGE_MATERIAL = 1,
    AUDIO_MATERIAL = 2,
    VIDEO_MATERIAL = 3,
    ROUTER_MATERIAL = 4,
    COMMAND_MATERIAL = 5,
    URL_MATERIAL = 6,
    COUPON_MATERIAL = 102
}

export enum materialVideoStyleType {
    fullScreenCenter = 0,
    wideScreen = 1
}


export enum dislayModeType {
    navigationMode = 0,
    businessMode = 1,
    allMode = 2
}

export enum videoQuality {
    highDefinition = 0,
    lowDefinition = 1
}

export interface triggerButtonValidation {
    src: string,
    style: string,
    hotspotId: string
}

export interface triggerValidation {
    background: {
        src: string,
        // 该结构需自动居中 保持大小
        style: string,
    },
    button: triggerButtonValidation[]
}

export interface bottomMarkValidation {
    src: string,
    params?: {
        startLongitude: number,
        startLatitude: number,
        endLongitude: number,
        endLatitude: number
    }
}

interface entranceAnimationValidation {
    type: number
    duration: number
    endFov: number
    startFov: number
    entranceAngle: number
    // 进入以后的角度
}

export interface welcomeAnimationValidation {
    trigger?: triggerValidation
    // trigger需要负责更多细节 有无trigger将表示有无入场动画
    animation?: entranceAnimationValidation
    // 动画
    overlay: Array<string>
    // 第一张图里的浮层
    entrance: Array<string>
    // 进入以后的载入图
    defaultFov: number
    // 默认的 全局fov视角
    loading: string
    // 启动页的logo
    enableTrigger: boolean
    // 可能弃用 是否启用Trigger
    tempPath: string
    // 可能弃用 临时访问路径
}

export interface loadInfoValidation {
    name: string,
    poster: string,
    id: string,
    desc: string,
    bottomMark: string,
    welcomeAnimation?: welcomeAnimationValidation,
    uri?: string,
    videoVersion?: number
}

interface detailHotspotValidation {
    hid: string,
    poster: string,
    timeoffset: number,
    video_id: number,
    desc: string,
    angle: number
}

export interface detailInfoValidation {
    code: number,
    detail: {
        rid: string,
        title: string,
        desc: string,
        angleRatio: number,
        angleSpeed: number,
        h5_url: string,
        hotspots: Array<detailHotspotValidation>
        location: {
            geo_point: Array<number>,
            desc: string
        },
        materialURI: string,
        meta_version: number,
        poster: string,
        type: { id: number, name: string },
        video_size: number,
        meta: any,
        navigatorStepLen: number
    }
}


export enum displayType {
    panoramaPicture,
    panoramaVideo,
    planeWalkingVideo,
    materialVideo
}

export interface hotspotValidation {
    desc: string
    id: string
    position: string
    poster: string
    snapshot: string
    start?: boolean
}

export interface roamHotspotValidation {
    code: number,
    hotspots: Array<hotspotValidation>
}

export interface groupOfHotspotsValidation {
    desc: string
    hid: string
    poster: string
    sort?: number
    hide?: boolean
}

export interface groupStatucValidation {
    gid: string
    desc: string
    poster: string
    hotspots: Array<groupOfHotspotsValidation>
    sort: number
}

export interface hotspotGroupValidation {
    code: number,
    groups: Array<groupStatucValidation>
}


export interface optionValidation {
    parent: string,
    source: string[][],
    publicKey: string,
    showBottomMark: boolean,
    autoLoad: boolean,
    bottomMark: bottomMarkValidation,
    defaultYaw: number,
    animationType?: SwitchAnimationType
}
export interface transitionValidation {
    angleSpeed: number
    duration: number
    offset: number
    reverse: boolean
    time: number
    url: Array<string>
    videoId: number
}
export interface pathValidation {
    transition?: Array<transitionValidation>,
    video?: Array<string>
}

export interface overlayValidation {
    overlayID: number,
    type: number,
    endTime: number,
    startLongitude: number,
    startLatitude: number,
    text: string,
    endLongitude: number,
    endLatitude: number,
    displayMode: dislayModeType
    startTime: number,
    lockCamera: boolean,
    xOffset: number,
    yOffset: number,
    imageType: number,
    parent?: overlayValidation,
    scaleMode?: number,
    children?: Array<number>,
    renderParent?: overlayValidation,
    params?: Array<string>
}

export enum SwitchAnimationType {
    None = 0,
    Rotate = 1,
    Zoom = 1 << 1,
    ZoonAfterRotate = 1 << 2,
    RotateYAfterX = 1 << 3
}

export interface stairsInfoType {
    status: boolean,
    floor: number,
    action: 'down' | 'up'
}

export enum typeDescr {
    coupon = 102
}

export interface tagNavigation extends hotspotValidation {
    info?: any
    tags?: string[]
    floor?: string
}


export interface v4HotspotsType {
    endLatitude: number
    endLongitude: number
    startLatitude: number
    startLongitude: number
    endTime: number
    navigationId?: string
    overlayID: string
    text: string
    startTime: number
    type: typeDescr
    videoId: number
    couponId: string
}

export interface v4HotspotsResType {
    code: number,
    hotspots: v4HotspotsType[]
}

export enum moveStatusType {
    portal = 'portal',
    navigation = 'navigation',
    way = 'way'
}

export enum guiderType {
    firstScreenGuide = 'firstScreenGuide',
    chooseStoreNameGuide = 'chooseStoreNameGuide',
    startNavigationGuide = 'startNavigationGuide'
}
export interface redPacketCount {
    amount: number,
    count: number,
    status: string
}
