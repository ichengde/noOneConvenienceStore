import { projectConfig } from '../../config'
import { commonUtil } from '../util/util'
import { request } from '../base/requestClient'

class Stat {
    private static _instance: Stat
    public static get instance() {
        if (!Stat._instance) {
            Stat._instance = new Stat()
        }
        return Stat._instance
    }

    private headers: object
    private client: object
    private session: object

    public sendEventStat(event: string, params?: object) {
        if (!!!this.headers) {
            this.headers = {
                'X-LC-Id': projectConfig.statAppId,
                'X-LC-Key': projectConfig.statAppKey,
            }
        }

        if (!!!this.client) {
            this.client = {
                id: projectConfig.userId,
                platform: projectConfig.statPlatform,
                app_version: projectConfig.statVersion,
                app_channel: projectConfig.statChannel,
            }
        }

        if (!!!this.session) {
            this.session = {
                id: `${projectConfig.userId}${projectConfig.startTime}`
            }
        }

        if (!!!params) {
            params = {}
        }

        var payload = {
            client: this.client,
            session: this.session,
            events: [
                {
                    event: event,
                    ts: Date.now(),
                    attributes: {
                        userid: projectConfig.userId,
                        ...params,
                    }
                }
            ]
        }

        request.requestProcess(projectConfig.statUrl, 'POST', JSON.stringify(payload), this.headers).then(()=>{
            return Promise.resolve()
        }).catch((e)=>{

        })
    }
}

let stat = Stat.instance

export { stat }
