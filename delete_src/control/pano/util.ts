export function calDirection(currentPosition, forwardPosition, desPosition) {
    let angle1 = Math.atan2(desPosition[2] - currentPosition[2], (desPosition[1] - currentPosition[1]));
    let angle2 = Math.atan2(forwardPosition[2] - currentPosition[2], (forwardPosition[1] - currentPosition[1]));
    const angle = (angle1 - angle2) / Math.PI * 180
    return angle;
}

export function calLatitude(height, currentPosition, desPosition) {
    var length = Math.sqrt((currentPosition[1] - desPosition[1]) * (currentPosition[1] - desPosition[1]) + (currentPosition[2] - desPosition[2]) * (currentPosition[2] - desPosition[2]));
    return -Math.atan(height / length) * 180 / Math.PI;
}



export function formatAngle(a1) {
    a1 = a1 % 360;
    if (a1 < 0) a1 += 360;
    if (a1 > 180) a1 -= 360;

    return a1;
}

export function desPointAngle(operatePlace, needRotateAngle, arrowOffset) {
    let inOffset = (operatePlace + needRotateAngle) % 360;
    inOffset = inOffset < 0 ? inOffset + 360 : inOffset;
    let offset = (arrowOffset % 360) < 0 ? (arrowOffset % 360) + 360 : (arrowOffset % 360);
    let delta = Math.abs(inOffset - offset);
    delta = delta > 180 ? 360 - delta : delta;
    if (delta > 90) {
        inOffset = arrowOffset + 180;
    } else {
        inOffset = arrowOffset;
    }
    return inOffset
}



export function clacPointsDeviation(currentPano, nextPano) {
    if (!nextPano) {
        return {
            longitude: 0,
            latitude: 0
        }
    }
    const start = currentPano
    const des = nextPano

    let currentPointIndex = start.pointIndex
    let helpPointIndex = 0
    const desPointIndex = des.pointIndex
    helpPointIndex = currentPointIndex + 1

    if (helpPointIndex >= start.points.length - 1) {
        helpPointIndex = start.points.length - 1
        currentPointIndex = start.points.length - 2
    }

    const crtPoint = start.points[currentPointIndex]
    const hlpPoint = start.points[helpPointIndex]
    const desPoint = des.points[desPointIndex]

    return {
        longitude: calDirection(crtPoint, hlpPoint, desPoint),
        latitude: calLatitude(1.4, crtPoint, desPoint)
    }
}
