

class PanoStatus {
    private static _instance: PanoStatus
    public static get instance() {
        if (!PanoStatus._instance) {
            PanoStatus._instance = new PanoStatus()
        }
        return PanoStatus._instance
    }

    /* 
        图片导航
    */
    path = []
    position = 0

    navigationWay

    get current() {
        return this.path[this.position]
    }

    get next() {
        const pos = this.position + 1
        if (this.check(pos)) {
            return this.path[pos]
        }
    }

    check(p) {
        if (this.path) {
            const max = this.path.length - 1
            const min = 0
            if (p <= max && p >= 0) {
                return true
            }
            return false
        }
    }

    get isDestination() {
        return this.position === this.desPos
    }

    get desPos() {
        return this.path.length - 1
    }

    get isOrigin() {
        return this.position === 0
    }

    moveTo(position) {
        this.position = position
    }

    tidyPath(path) {

        const pictureList = []
        const oPath = path.map((snapInfo, i) => {
            const snap = {}
            const keyName = Object.keys(snapInfo)
            keyName.forEach((key) => {
                if (snapInfo[key] !== undefined) {
                    if (key.indexOf('c') === 0 && key !== 'corner') {
                        if (snap['intersection'] === undefined) {
                            snap['intersection'] = {
                                corner: snapInfo['corner']
                            }
                        }
                        snap['intersection'][String(key).slice(1)] = snapInfo[key]
                    } else {
                        snap[key] = snapInfo[key]
                    }
                }
            })

            return snap
        })

        return oPath
    }

    get pathSource() {
        let sce = []
        this.path.forEach((p) => {
            if (p.intersection) {
                sce.push([p.snapshot, p.intersection.snapshot])
            } else {
                sce.push([p.snapshot])
            }
        })
        console.log('sce', sce)
        return sce
    }

    stashNavigationWay(startNavigation, endNavigation) {
        this.navigationWay = {
            startNavigation: JSON.parse(JSON.stringify(startNavigation)),
            endNavigation: JSON.parse(JSON.stringify(endNavigation))
        }
    }
}

let panoStatus = PanoStatus.instance

export { panoStatus }