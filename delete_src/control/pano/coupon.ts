import { request } from '../base/requestClient'
import { projectConfig } from '../../config'
import { typeDescr, materialType, v4HotspotsType, v4HotspotsResType } from '../util/type'
import { isMaterialInTime } from '../util/util'
import { management } from '../base/roamManagement'
import { status } from '../base/roamStatus'
// import { wechatApi } from '../plug/wechatShare'

/* 
    该页code hotspot代表热点
*/


export class coupon {
    hotspots: any
    defaultPicture = `./images/toast.png`
    constructor() { }

    fetchCoupon() {
        return request.getCoupon().then((res) => {
            this.hotspots = this.embellishRecommendAndCoupon(res.hotspots)
            return Promise.resolve()
        })
    }

    embellishRecommendAndCoupon(hotspots) {

        hotspots = hotspots.map((hotspot) => {
            if (hotspot.text !== null) {
                const couponHotspot = {}
                let couponInfo = {}
                let productInfo = {}
                let recommendInfo = {}
                let currentPositionInfo = {}
                let navigationInfo = {}
                const baseId = hotspot['overlayID']
                const keyNames = Object.keys(hotspot)

                keyNames.forEach((key: string) => {
                    if (key.includes('coupon') || key === 'text' || key === 'type') {
                        couponInfo[key] = hotspot[key]
                    } else if (key.includes('product')) {
                        productInfo[key] = hotspot[key]
                    } else if (key.includes('recommend')) {
                        recommendInfo[key] = hotspot[key]
                    }else if(key.includes('position')){
                        currentPositionInfo[key] = hotspot[key];
                    }else if(key.includes('navigation')){
                        navigationInfo[key] = hotspot[key];
                    } else {
                        couponHotspot[key] = hotspot[key]
                    }
                })
                couponHotspot['navigationId'] = navigationInfo['navigationId']
                couponHotspot['text'] = couponHotspot['backgroundImage']
                couponHotspot['type'] = 1
                couponHotspot['vertical'] = true

                /*      
                    示例
                    {
                         "text": "http://video.ali.inreal.tv/59f2e74567f3560044595acb/image/dztj.png",
                         "type": 1,
                         "xOffset": 0.25,
                         "yOffset": 0.68,
                         "des": [
                             {
                                 "text": "http://video.ali.inreal.tv/59f2e74567f3560044595acb/image/b2tanmujiang.png",
                                 "type": 1
                             }
                         ]
                     }, 
                */

                const floor = status.hotspotById(navigationInfo['navigationId']).floor;
                const positionName = floor ===undefined ? status.hotspotById(navigationInfo['navigationId']).desc: `${floor}-${status.hotspotById(navigationInfo['navigationId']).desc}`
                const currentPosition = {}
                currentPosition['overlayID'] = `${baseId}-currentPosition`
                currentPosition['text'] = positionName
                currentPosition['type'] = 1
                currentPosition['fontSize'] = 25
                currentPosition['fontColor'] = '#000000'
                currentPosition['imageType'] = 3
                currentPosition['xOffset'] = currentPositionInfo['positionXOffset']
                currentPosition['yOffset'] = currentPositionInfo['positionYOffset']
                currentPosition['vertical'] = true



                let recommend
                if(recommendInfo['recommendImage']){
                    let stat = {
                        overlayID : "stat",
                        type:5,
                        text:"stat",
                        params:["store.ad"]
                    }
                    recommend = {}
                    recommend['overlayID'] = `${baseId}-recommend`
                    recommend['text'] = recommendInfo['recommendImage']
                    recommend['type'] = 1
                    recommend['xOffset'] = recommendInfo['recommendXOffset']
                    recommend['yOffset'] = recommendInfo['recommendYOffset']
                    recommend['vertical'] = true
                    recommend['des'] = [{
                        overlayID: `${baseId}-product`,
                        text: productInfo['productImage'],
                        vertical: true,
                        startLongitude: couponHotspot['startLongitude'] || 0,
                        startLatitude: couponHotspot['startLatitude'] || 0,
                        endLongitude: couponHotspot['endLongitude'] || 0,
                        endLatitude: couponHotspot['endLatitude'] || 0,
                        startTime: couponHotspot['startTime'] || 0,
                        endTime: couponHotspot['endTime'] || 0,
                        type: 1,
                        des:[{
                            overlayID: "back",
                            displayMode: 2,
                            type: 5,
                            text: "back"
                        }]
                    },stat]
                }



                /* 
                ,   示例
                        {
                            "text": "http: //video.ali.inreal.tv/59f2e74567f3560044595acb/image/zxyh.png",
                            "type": 1,
                            "xOffset": 0.75,
                            "yOffset": 0.68,
                            "children": [
                                {
                                    "text": "满100减99",
                                    "type": 3,
                                    "xOffset": 0.5,
                                    "yOffset": 0.57,
                                    "fontColor": "#ffffff",
                                    "fontSize": 20
                                }
                            ],
                            "des": [
                                {
                                    "text": "优惠券ID",
                                    "type": 102
                                }
                            ]
                        }
                */

                let redPacket ;
                if(status.hotspotById(navigationInfo['navigationId']).tags.indexOf("红包") !== -1){
                    redPacket = {}
                    redPacket['overlayID'] = `${navigationInfo['navigationId']}-redPacket`
                    redPacket['text'] = './images/navigationRedPacket.png'
                    redPacket['type'] = 1
                    redPacket['xOffset'] = (typeof couponInfo['text'] === 'object' &&couponInfo['text'].length === 2)?0.89:0.83
                    redPacket['yOffset'] = -0.11
                    redPacket['sizeScale'] = 1.5
                    redPacket['vertical'] = true
                }
                let navigateToCommand =   {
                    overlayID: `${baseId}-navigateTo`,
                    text: 'navigateTo',
                    type:5,
                    params:[navigationInfo['navigationId']]
                }


                const navigationTo = {}
                navigationTo['overlayID'] = `${baseId}-navigationTo`
                navigationTo['text'] = './images/navigation.png'
                navigationTo['type'] = 1
                navigationTo['xOffset'] = 0.5
                navigationTo['yOffset'] = 1.2
                navigationTo['vertical'] = true
                navigationTo['des'] = [{
                    overlayID: `${baseId}-navigateTo`,
                    text: 'navigateTo',
                    type:5,
                    params:[navigationInfo['navigationId']]
                }]

                if (typeof couponInfo['text'] === 'string') {
                    const coupon = {}
                    coupon['overlayID'] = `${baseId}-couponImage`
                    coupon['text'] = couponInfo['couponImage']
                    coupon['type'] = 1
                    coupon['vertical'] = true
                    coupon['xOffset'] = couponInfo['couponXOffset']
                    coupon['yOffset'] = couponInfo['couponYOffset']
                    coupon['children'] = [] //文字优惠说明
                    coupon['couponId'] = couponInfo['text'] //文字优惠说明

               //     coupon['des'] = [navigateToCommand]

                    coupon['des'] = [{
                        overlayID: `${baseId}-couponDo`,
                        text: couponInfo['text'],
                        type: couponInfo['type']
                    }]

                    couponHotspot['children'] = []
                    if(recommend){couponHotspot['children'].push(recommend)}
                    couponHotspot['children'].push(coupon)
                    couponHotspot['children'].push(navigationTo)
                    couponHotspot['children'].push(currentPosition)
                    if(redPacket !==undefined){ couponHotspot['children'].push(redPacket)}

                } else if (couponInfo['text'].length === 2) {

                    const coupon1 = {}
                    coupon1['overlayID'] = `${baseId}-couponImage-1`
                    coupon1['text'] = couponInfo['coupon'][0]['image']
                    coupon1['type'] = 1
                    coupon1['vertical'] = true
                    coupon1['xOffset'] = couponInfo['coupon'][0]['xOffset']
                    coupon1['yOffset'] = couponInfo['coupon'][0]['yOffset']
                    coupon1['children'] = [] //文字优惠说明
                    coupon1['couponId'] = couponInfo['text'][0]

                  //  coupon1['des'] = [navigateToCommand]

                    coupon1['des'] = [{
                        overlayID: `${baseId}-couponDo`,
                        text: couponInfo['text'][0],
                        type: couponInfo['type']
                    }]
                    const coupon2 = {}
                    coupon2['overlayID'] = `${baseId}-couponImage-2`
                    coupon2['text'] = couponInfo['coupon'][1]['image']
                    coupon2['type'] = 1
                    coupon2['vertical'] = true
                    coupon2['xOffset'] = couponInfo['coupon'][1]['xOffset']
                    coupon2['yOffset'] = couponInfo['coupon'][1]['yOffset']
                    coupon2['children'] = [] //文字优惠说明
                    coupon2['couponId'] = couponInfo['text'][1]

                //    coupon2['des'] = [navigateToCommand]
                    coupon2['des'] = [{
                        overlayID: `${baseId}-couponDo`,
                        text: couponInfo['text'][0],
                        type: couponInfo['type']
                    }]
                    couponHotspot['children'] = []
                    if(recommend){couponHotspot['children'].push(recommend)}
                    couponHotspot['children'].push(coupon1)
                    couponHotspot['children'].push(coupon2)
                    couponHotspot['children'].push(navigationTo)
                    couponHotspot['children'].push(currentPosition)
                    if(redPacket !==undefined){ couponHotspot['children'].push(redPacket)}
                }




                return couponHotspot
            }
        })
        return hotspots
    }

    async getCoupouByNavigationId(navId) {

        const needHotspots = this.hotspots.filter((hotspot) => {
            return hotspot && hotspot.navigationId === navId
        })

        if (needHotspots instanceof Array) {

            for (let htIndex = 0; htIndex < needHotspots.length; htIndex++) {
                const nh = needHotspots[htIndex];

                let coupons = nh.children

                if (coupons && coupons.length > 0) {
                    coupons = coupons.filter((coupon) => {
                        return coupon.overlayID.includes('couponImage')
                    })

                    for (let index = 0; index < coupons.length; index++) {
                        const coupon = coupons[index];

                        //const couponId = coupon['des'][0]['text']
                        const couponId = coupon['couponId']
                        if (couponId) {
                            const text = await request.getCouponText(couponId)
                            if (text) {
                                coupon.children.push({
                                    "text": text,
                                    "type": 1,
                                    "imageType": 3,
                                    "xOffset": 0.5,
                                    "yOffset": 0.57,
                                    "fontColor": "#ffffff",
                                    "fontSize": 20
                                })
                            }
                        }
                    }
                }
            }
        }

        return needHotspots
    }

}