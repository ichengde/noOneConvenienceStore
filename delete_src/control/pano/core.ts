import { projectConfig } from '../../config'
import { materialVideoStyleType, materialType, displayType, transitionValidation, SwitchAnimationType, stairsInfoType, tagNavigation, moveStatusType } from '../util/type'
import { ui } from '../ui/uiControl'
import { management, currentTransitionValidation } from '../base/roamManagement'
import { overlayRender } from '../base/overlayRender'
import { commonUtil, transitionUtil, materialVideoUtil } from '../util/util'
import { status } from '../base/roamStatus'
import { panoStatus } from './status'
import { audio } from '../base/audio'
import { calDirection, calLatitude, formatAngle, desPointAngle, clacPointsDeviation } from './util'
import * as Cookies from "js-cookie"
import { request } from '../base/requestClient';
import { coupon } from './coupon';
import { baby } from '../plug/baby';
import { stat } from '../util/stat'

declare var InWalkPlayer
declare var worldRenderer


export class panoCore {
    private static _instance: panoCore
    public static get instance() {
        if (!panoCore._instance) {
            panoCore._instance = new panoCore()
        }
        return panoCore._instance
    }


    overlayList = []
    bindFlag = false
    secondPathRotate

    findPathAngle

    firstEnterIntersectionFlag = false
    intersecting = false

    BlinkInNavigation
    blinkAfterGoFlag: any
    public static coupon = new coupon()
    public static baby = new baby()
    constructor() {
    }


    bindReachEvent() {
        if (this.bindFlag === false) {

            management.vr.on('canplay', () => {


                this.overlayList.forEach((e) => {
                    management.vr.addOverlay(e)
                })

                if (this.firstEnterIntersectionFlag === true) {
                    // 路口图片
                    this.intersecting = true
                    this.next()
                    this.firstEnterIntersectionFlag = false
                } else {

                    // this.walkUpAndDownStairs()


                    if (panoStatus.path && panoStatus.position !== 0 && panoStatus.position !== panoStatus.path.length - 1) {
                        status.prevVideoTransition = { videoId: panoStatus.current.videoId, time: panoStatus.current.time }

                        ui.showOrHideOperate(true)
                    }



                    if (panoStatus.isOrigin) {
                        ui.showOrHideOperate(false, 'previous')
                    } else if (panoStatus.isDestination) {
                        // 导航点
                        ui.showOrHideOperate(false, 'next')
                        this.showOverlay(status.endHotspot.id)

                        if (status.currentHotspot.id !== status.endHotspot.id) {
                            stat.sendEventStat('navigation.walk.finish', {
                                src: status.currentHotspot.id,
                                src_name: status.currentHotspot.desc,
                                dst: status.endHotspot.id,
                                dst_name: status.endHotspot.desc,
                            })
                        }

                        status.currentHotspot = status.endHotspot
                        status.prevVideoTransition = undefined

                        if (this.secondPathRotate) {
                            console.log(`到结尾 ${this.secondPathRotate}`)
                            // management.vr.autoYaw(this.secondPathRotate, 3000, 1)
                            management.vr.autoRotate(this.secondPathRotate, 0, 3000, 1)
                            this.secondPathRotate = undefined
                        }
                    }


                    if (this.BlinkInNavigation) {
                        this.showOverlay(this.BlinkInNavigation)
                        this.BlinkInNavigation = undefined
                    }

                    // 为了能改起点 先跳后导航
                    this.checkIsNeedBlinkAfterGo()


                    /*  if (this.secondPathRotate) {
                         // 终点处理 跳转过来
                         management.vr.autoYaw(this.secondPathRotate, 3000, 1)
                         this.secondPathRotate = undefined
                         this.showOverlay(status.endHotspot.id)
                     }
                      */

                }



            })

            this.bindButtonUI();



            this.bindFlag = true
        }
    }


    private bindButtonUI() {
        const nextDom = document.querySelector('.snap-enter');
        const previousDom = document.querySelector('.snap-return');
        nextDom.addEventListener('click', () => {
            this.next();
        });
        previousDom.addEventListener('click', () => {
            this.previous();
        });
    }

    roamPreview() {
        this.clearOverlay()
        if (this.checkIsSame()) {
            return
        }
        // 显示浮层
        this.bindReachEvent()

        ui.showOrHideOperate(false)

        this.createOperate(panoStatus.current, panoStatus.next, true)
        this.overlayList.forEach((e) => {
            management.vr.addOverlay(e)
        })

        this.doFindPathAngleRotate();
    }

    public doFindPathAngleRotate() {
        if (this.findPathAngle !== undefined) {
            // management.vr.autoYaw(this.findPathAngle, 3000, 1)
            management.vr.autoRotate(this.findPathAngle, 0, 3000, 1);
            this.findPathAngle = undefined;
        }
    }

    cancelRoamPreview() {
        if (status.hotspotBeforePreview !== undefined) {
            status.endHotspot = status.hotspotBeforePreview
            status.hotspotBeforePreview = undefined

            this.roamBlind();
        } else {
            this.clearOverlay()
            this.resetTransition()
        }
    }

    roamGo() {
        if (this.checkIsSame()) {
            return
        }

        this.bindReachEvent()
        ui.showOrHideOperate(true, 'next')

        this.clearOverlay()
        this.createOperate(panoStatus.current, panoStatus.next)

        this.overlayList.forEach((e) => {
            management.vr.addOverlay(e)
        })

        this.resetTransition();
        management.updateUrlShareCurrentLocation(moveStatusType.way, status.endHotspot.id)
    }

    private checkIsSame() {
        if (status.currentHotspot.id === status.endHotspot.id) {
            const inNavigation = <tagNavigation>status.currentHotspot
            const angle = inNavigation.info.angle
            if (angle) {
                // management.vr.autoYaw(angle, 3000, 1)
                management.vr.autoRotate(angle, 0, 3000, 1)
            }
            return true
        } else {
            return false
        }
    }

    private resetTransition() {
        if (status.prevVideoTransition) {
            console.log('清空prevVideoTransition');
            status.prevVideoTransition = undefined;
        }
    }

    roamBlind(endAngle?, faceAngle?) {

        if (this.checkIsSame()) {
            return
        }

        this.clearOverlay()
        ui.showOrHideOperate(false)
        this.bindReachEvent()

        let pic: string[][]
        pic = [[this.DestinationNav.pic]]

        this.loadSence(0, this.DestinationNav.angle, pic)
        this.setRoamBlindOverlayShow(status.endHotspot.id)
        this.resetPath()
        status.currentHotspot = status.endHotspot

        this.resetTransition()
        management.updateUrlShareCurrentLocation(moveStatusType.navigation, status.endHotspot.id)
    }

    setRoamBlindOverlayShow(id) {
        this.BlinkInNavigation = id
    }

    next() {
        if (management.vr.worldRenderer.loading === true || !panoStatus.path) {
            return
        }

        this.overlayList = []

        console.log(panoStatus.position, panoStatus.path)

        const origin = panoStatus.path[panoStatus.position]
        let prePos = panoStatus.position + 1
        // 虚拟位置移动 已是下一张图
        let inPano = panoStatus.path[panoStatus.position + 1]
        let nextPano = panoStatus.path[panoStatus.position + 2]

        let info = this.getInstructionRenderInfo(origin, inPano)

        let intersectionInfo = origin.intersection
        if (intersectionInfo && this.intersecting === false && prePos !== 1) {

            const debugInfo = `
            进入路口的切换
            位于下标 ${panoStatus.position} 将载入 此点路口图片
            `
            console.log(debugInfo, origin)

            this.firstEnterIntersectionFlag = true

            info = this.getInstructionRenderInfo(origin, intersectionInfo, true)
            this.switchSence('next', origin.offset, intersectionInfo.offset, 'common')

        } else {

            if (panoStatus.check(prePos)) {

                let switchEffect: 'common' | 'intersection' = 'common'
                if (this.intersecting === true) {
                    // 从路口出来
                    info = this.getInstructionRenderInfo(intersectionInfo, inPano)
                    switchEffect = 'intersection'
                    this.intersecting = false
                }

                panoStatus.moveTo(prePos)

                switch (prePos) {
                    case 1: {
                        const pictureList = panoStatus.pathSource

                        this.loadSence(info.currentPanoFinalAngle, inPano.offset, pictureList)
                        if (panoStatus.isDestination === false) {
                            this.createOperate(inPano, nextPano)
                        } else {
                            this.secondPathRotate = this.DestinationNav.angle
                        }
                        break
                    }
                    case panoStatus.desPos: {
                        this.secondPathRotate = this.DestinationNav.angle
                        const stairsInfo = this.isStairsAround('before')
                        if (stairsInfo.status === true) {
                            this.switchSence('next', info.currentPanoFinalAngle, inPano.offset, switchEffect)
                        } else {
                            this.switchSence('next', info.arrowDirectionOffset, inPano.offset, switchEffect)
                        }
                        //   this.switchSence('next', info.arrowDirectionOffset, inPano.offset, switchEffect)
                        break
                    }
                    default: {

                        const stairsInfo = this.isStairsAround('before')
                        if (stairsInfo.status === true) {
                            this.switchSence('next', info.currentPanoFinalAngle, inPano.offset, switchEffect)
                        } else {
                            this.switchSence('next', info.arrowDirectionOffset, inPano.offset, switchEffect)
                        }

                        this.createOperate(inPano, nextPano)


                    }
                }



            }
        }

        this.updateMiniProgramShareInfo()

    }


    previous() {
        if (management.vr.worldRenderer.loading === true) {
            return
        }

        this.updateMiniProgramShareInfo()
        // 切换全景
        this.overlayList = []

        if (panoStatus.path) {

            if (panoStatus.check(panoStatus.position - 1)) {

                panoStatus.position = panoStatus.position - 1

                this.switchSence('previous', 0, panoStatus.current.offset)

                this.createOperate(panoStatus.current, panoStatus.next)
            }

        }
    }

    setAngleWhenForFindPath(angle) {
        this.findPathAngle = angle
    }

    createOperate(panoA, panoB, hideText?) {
        const info = this.getInstructionRenderInfo(panoA, panoB)

        const stepLen = info.stepLen
        const operatePlace = info.nextPanoOperatePlace
        const needRotateAngle = info.needRotateAngle
        const pointPos = clacPointsDeviation(panoA, panoB)

        const arrowOffset = pointPos.longitude
        const flagLat = pointPos.latitude

        this.setAngleWhenForFindPath(operatePlace)


        const arrow = {
            image: '',
            type: 1,
            longitude: operatePlace,
            latitude: -20.7,
            //landmark: true,
            normal: [0, 1, 0],
            directionOffset: operatePlace,
        };

        let textTips = ''
        arrow.image = './images/arrow-forward.png'


        if (needRotateAngle <= -45) {
            arrow.image = './images/arrow-right.png'
            arrow.longitude -= 10
            textTips = '右转然后'
        }

        if (needRotateAngle >= 45) {
            arrow.image = './images/arrow-left.png'
            textTips = '左转然后'
            arrow.longitude += 10
        }


        const stairsInfo = this.isStairsAround('later')
        if (stairsInfo.status === true) {
            const nextPos = panoStatus.position + 1
            const nextPosFloor = management.pathGen.floorMap(panoStatus.path[nextPos].videoIdPrefix)
            textTips += `前往${nextPosFloor}楼`
        } else {
            textTips += `前行${stepLen}米`
            if (needRotateAngle === 0) {
                arrow.directionOffset += arrowOffset
            }
        }



        const nextBase = {
            image: './images/snap-next.png',
            normal: [0, 1, 0],
            type: 1,
            longitude: operatePlace,
            latitude: -35,
            landmark: true
        };

        const textDescr = {
            type: 3,
            longitude: operatePlace,
            latitude: -35,
            fontSize: 30,
            fontColor: "#fb463b",
            backgroundColor: "#ffffff",
            vertical: true,
            text: textTips,
            layer: 2
        };

        const previousMirror = {
            image: './images/snap-previous.png',
            type: 1,
            xOffset: 0.5,
            yOffset: 1.5,
            normal: [0, 1, 0]
        };


        let inOffset = desPointAngle(operatePlace, needRotateAngle, arrowOffset)


        const debugInfo = `
        从 ${status.currentHotspot.desc} ${status.currentHotspot.id} 至 ${status.endHotspot.desc} ${status.endHotspot.id} 使用path数组下标为${panoStatus.position + 1}
        箭头偏移  ${arrowOffset}
        箭头位置 operatePlace ${operatePlace} 步长 ${stepLen}
        使用${panoStatus.position}与${panoStatus.position + 1}计算得到 下一张图所要转的角度 endAngle ${needRotateAngle}
        娃娃所处位置 ${inOffset}
        `
        console.log(debugInfo)

        if (panoStatus.position + 1 <= panoStatus.path.length - 1) {

            if (panoStatus.current.videoIdPrefix === panoStatus.path[panoStatus.position + 1].videoIdPrefix) {
                const finalFlag = {
                    image: './images/final.png',
                    type: 1,
                    longitude: inOffset,
                    latitude: flagLat,
                    // normal: [0, 1, 0]
                };
                const finalFlagInstance = management.vr.overlay(finalFlag)
                this.overlayList.push(finalFlagInstance)
            }
        }

        const arrowInstance = management.vr.overlay(arrow);
        const textInstance = management.vr.overlay(textDescr);
        const previousInstance = management.vr.overlay(previousMirror);
        const nextBaseInstance = management.vr.overlay(nextBase);

        this.overlayList.push(arrowInstance)
        if (!!!hideText) {
            this.overlayList.push(textInstance)
            textInstance.event = (() => {
                this.next()
            }).bind(this)
        }

        arrowInstance.event = (() => {

            this.next()
        }).bind(this)
        nextBaseInstance.event = (() => {

            this.next()
        }).bind(this)

        previousInstance.event = (() => {

            this.previous()

        }).bind(this)

    }

    resetPath() {

        panoStatus.path = []
        panoStatus.position = 0

    }


    showOverlay(id: string, renderMaterial?: Array<any>) {
        let material = management.getMaterial(id)

        let momentMaterial = renderMaterial ? renderMaterial : material
        let weTime = JSON.parse(status.hotspotById(id).position).timeoffset
        // const couponMaterial = panoCore.coupon.getCoupouByNavigationId(id)
        let hotspot = status.hotspotById(id);

        // customType 1：无店长推荐，无优惠券 2：有店长推荐，无优惠券
        if (hotspot.customType === 1) {
            let noRecommendMaterial = this._generateEmbellishNoRecommend(status.hotspotById(id));
            momentMaterial = momentMaterial.concat(noRecommendMaterial)
            this._dealMaterial(momentMaterial, weTime);
        } else if (hotspot.customType === 2) {
            let recommendMaterial = this._generateEmbellishRecommendNoCoupon(status.hotspotById(id));
            momentMaterial = momentMaterial.concat(recommendMaterial)
            this._dealMaterial(momentMaterial, weTime);
        } else {
            panoCore.coupon.getCoupouByNavigationId(id).then((couponMaterial) => {
                if (couponMaterial) {
                    momentMaterial = momentMaterial.concat(couponMaterial)
                }

                this._dealMaterial(momentMaterial, weTime);
            }, () => {
                this._dealMaterial(momentMaterial, weTime);
            }).catch((err) => {
                console.log(err)
            })
        }

    }




    private _generateEmbellishNoRecommend(hotspot) {
        const background = {}
        const baseId = hotspot['id']
        background['overlayID'] = `${baseId}-navigationbg`
        background['navigationId'] = baseId
        background['text'] = './images/bg_0.png'
        background['type'] = 1
        background['vertical'] = true
        background['startLongitude'] = hotspot.info.angle
        background['startLatitude'] = 20
        background['endLongitude'] = hotspot.info.angle
        background['endLatitude'] = 20
        background['startTime'] = hotspot.info.timeoffset - 0.1;
        background['endTime'] = hotspot.info.timeoffset + 0.1;
        background['videoID'] = hotspot.info.videoId;

        const positionName = hotspot.floor ===undefined ? hotspot.desc : `${hotspot.floor}-${hotspot.desc}`
        const currentPosition = {}
        currentPosition['overlayID'] = `${baseId}-currentPosition`
        currentPosition['text'] = positionName
        currentPosition['type'] = 1
        currentPosition['fontSize'] = 25
        currentPosition['fontColor'] = '#000000'
        currentPosition['imageType'] = 3
        currentPosition['xOffset'] = 0.5
        currentPosition['yOffset'] = 0.5
        currentPosition['vertical'] = true



        let navigateToCommand = {
            overlayID: `${baseId}-navigateTo`,
            text: 'navigateTo',
            type: 5,
            params: [baseId]
        }

        const navigateTo = {}
        navigateTo['overlayID'] = `${baseId}-navigationTo`
        navigateTo['text'] = './images/navigation.png'
        navigateTo['type'] = 1
        navigateTo['xOffset'] = 0.5
        navigateTo['yOffset'] = 2
        navigateTo['vertical'] = true
        navigateTo['des'] = [navigateToCommand]



        background['children'] = []
        background['children'].push(currentPosition)
        background['children'].push(navigateTo)

        if (hotspot.tags.indexOf("红包") !== -1) {
            const redPacket = {}
            redPacket['overlayID'] = `${baseId}-redPacket`
            redPacket['text'] = './images/navigationRedPacket.png'
            redPacket['type'] = 1
            redPacket['xOffset'] = 0.83
            redPacket['yOffset'] = -0.4
            redPacket['vertical'] = true
            redPacket['sizeScale'] = 1.5
            background['children'].push(redPacket)
        }
        return background
    }


    private _generateEmbellishRecommendNoCoupon(hotspot) {
        const background = {}
        const baseId = hotspot['id']
        background['overlayID'] = `${baseId}-navigationbg`
        background['navigationId'] = baseId
        background['text'] = './images/bg_1.png'
        background['type'] = 1
        background['vertical'] = true
        background['startLongitude'] = hotspot.info.angle
        background['startLatitude'] = 20
        background['endLongitude'] = hotspot.info.angle
        background['endLatitude'] = 20
        background['startTime'] = hotspot.info.timeoffset - 0.1;
        background['endTime'] = hotspot.info.timeoffset + 0.1;
        background['videoID'] = hotspot.info.videoId;

        const currentPosition = {}
        const positionName = hotspot.floor ===undefined ? hotspot.desc : `${hotspot.floor}-${hotspot.desc}`
        currentPosition['overlayID'] = `${baseId}-currentPosition`
        currentPosition['text'] = positionName
        currentPosition['type'] = 1
        currentPosition['fontSize'] = 25
        currentPosition['fontColor'] = '#000000'
        currentPosition['imageType'] = 3
        currentPosition['xOffset'] = 0.5
        currentPosition['yOffset'] = 0.14
        currentPosition['vertical'] = true
        let stat = {
            overlayID : "stat",
            type:5,
            text:"stat",
            params:["store.ad"]
        }
        const recommend = {}
        recommend['overlayID'] = `${baseId}-recommend`
        recommend['text'] = './images/dztj.png'
        recommend['type'] = 1
        recommend['xOffset'] = 0.51
        recommend['yOffset'] = 0.68
        recommend['vertical'] = true
        recommend['des'] = [{
            overlayID: `${baseId}-product`,
            text: `//mall.zhixianshi.com/59f2e74567f3560044595acb/image/product/${baseId}.jpg`,
            vertical: true,
            type: 1,
            startLongitude: hotspot.info.angle,
            startLatitude: 20,
            endLongitude: hotspot.info.angle,
            endLatitude: 20,
            startTime: hotspot.info.timeoffset - 0.1,
            endTime: hotspot.info.timeoffset + 0.1,
            des: [{
                overlayID: "back",
                displayMode: 2,
                type: 5,
                text: "back"
            }]
        },stat]

        let navigateToCommand = {
            overlayID: `${baseId}-navigateTo`,
            text: 'navigateTo',
            type: 5,
            params: [baseId]
        }

        const navigateTo = {}
        navigateTo['overlayID'] = `${baseId}-navigationTo`
        navigateTo['text'] = './images/navigation.png'
        navigateTo['type'] = 1
        navigateTo['xOffset'] = 0.5
        navigateTo['yOffset'] = 1.2
        navigateTo['vertical'] = true
        navigateTo['des'] = [navigateToCommand]



        background['children'] = []
        background['children'].push(recommend)
        background['children'].push(currentPosition)
        background['children'].push(navigateTo)


        if (hotspot.tags.indexOf("红包") !== -1) {
            const redPacket = {}
            redPacket['overlayID'] = `${baseId}-redPacket`
            redPacket['text'] = './images/navigationRedPacket.png'
            redPacket['type'] = 1
            redPacket['xOffset'] = 0.83
            redPacket['yOffset'] = -0.11
            redPacket['sizeScale'] = 1.5
            redPacket['vertical'] = true
            background['children'].push(redPacket)
        }


        return background
    }


    private _dealMaterial(momentMaterial: any[], weTime: any) {
        console.log(momentMaterial)
        overlayRender.setMaterials(momentMaterial, weTime);
        // check video material
        if (materialVideoUtil.isHasMaterialVideoInMaterials(weTime, momentMaterial)) {
            if (!status.skipMaterialVideo) {
                overlayRender.render(materialVideoUtil.video);
            }
            else {
                momentMaterial.forEach((m) => {
                    if (m.type === materialType.VIDEO_MATERIAL) {
                        overlayRender.appendChild(m);
                        console.log(m.des);
                        if (m.des) {
                            m.des.forEach(e => {
                                overlayRender.render(e);
                            });
                        }
                    }
                    // audio play
                });
            }
        }
        else {
            momentMaterial.forEach((m) => {
                if (m.type !== materialType.VIDEO_MATERIAL) {
                    overlayRender.render(m);
                }
            });
        }
    }

    loadSence(rotateToLongitude, newSenceLongitude, picture: string[][]) {
        status.option.source = picture

        status.option['startLongitude'] = rotateToLongitude
        status.option['startLatitude'] = 0
        status.option['defaultYaw'] = newSenceLongitude
        status.option['zoomAnimation'] = true

        if (picture.length === 1) {
            status.option['animationType'] = SwitchAnimationType.None
            status.option['skipFirst'] = false
            status.option['startLatitude'] = undefined
            status.option['startLongitude'] = undefined
            status.option['zoomAnimation'] = false
        } else {
            status.option['animationType'] = SwitchAnimationType.Zoom | SwitchAnimationType.Rotate
            status.option['skipFirst'] = true
            const stairsInfo = this.isStairsAround('before')
            if (stairsInfo.status === true) {
                let rotateTolatitude = 0
                let effect = SwitchAnimationType.Zoom | SwitchAnimationType.Rotate
                if (stairsInfo.action === 'down') {
                    effect = effect | SwitchAnimationType.ZoonAfterRotate
                } else if (stairsInfo.action === 'up') {
                    rotateTolatitude = 20
                    effect = effect | SwitchAnimationType.ZoonAfterRotate | SwitchAnimationType.RotateYAfterX
                }
                status.option['animationType'] = effect;
                status.option['startLatitude'] = rotateTolatitude

            }

        }

        management.vr.reload(status.option)
    }

    switchSence(action: 'next' | 'previous', rotateToLongitude = 0, newSenceLongitude, switchEffect?: 'common' | 'intersection') {
        let effect = SwitchAnimationType.Rotate | SwitchAnimationType.Zoom
        if (switchEffect === 'intersection') {
            effect = SwitchAnimationType.Rotate | SwitchAnimationType.Zoom | SwitchAnimationType.ZoonAfterRotate
        }

        let rotateTolatitude = 0


        const stairsInfo = this.isStairsAround('before')
        if (stairsInfo.status === true) {

            if (stairsInfo.action === 'down') {
                effect = effect | SwitchAnimationType.ZoonAfterRotate
            }

            if (stairsInfo.action === 'up') {
                rotateTolatitude = 20
                console.log('上楼状态')
                effect = effect | SwitchAnimationType.ZoonAfterRotate | SwitchAnimationType.RotateYAfterX
            }

        }


        if (action === 'next') {
            management.vr.switch(true, effect, rotateToLongitude, rotateTolatitude, newSenceLongitude, 0)
        }

        if (action === 'previous') {
            management.vr.switch(false, SwitchAnimationType.None, undefined, undefined, newSenceLongitude, 0)
        }
    }

    clearOverlay() {
        this.overlayList.forEach((e) => {
            management.vr.removeOverlay(e)
        })
        this.overlayList = []
    }

    getInstructionRenderInfo(panoA, panoB, isIntersection?: boolean) {

        const currentPanoFinalAngle = formatAngle(panoA.offset + panoA.corner)
        const nextPanoOperatePlace = panoA.offset
        const needRotateAngle = formatAngle(panoA.corner)
        const deviation = clacPointsDeviation(panoA, panoB)
        let arrowDirectionOffset = desPointAngle(nextPanoOperatePlace, needRotateAngle, deviation.longitude)
        const stepLen = panoB !== undefined ? panoB.step : 0

        if (isIntersection === true) {
            arrowDirectionOffset = 0
        }



        return {
            currentPanoFinalAngle: currentPanoFinalAngle,
            nextPanoOperatePlace: nextPanoOperatePlace,
            needRotateAngle: needRotateAngle,
            arrowDirectionOffset: arrowDirectionOffset,
            stepLen: stepLen
        }
    }

    get DestinationNav() {
        const videoId = JSON.parse(status.endHotspot.position).videoId
        const time = JSON.parse(status.endHotspot.position).timeoffset
        return {
            angle: JSON.parse(status.endHotspot.position).angle,
            pic: management.pathGen.getSnapshot(videoId, time)
        }
    }

    walkUpAndDownStairs() {
        const stairsInfo = this.isStairsAround('before')
        if (stairsInfo.status === true) {

            const floor = management.pathGen.floorMap(stairsInfo.floor)
            const dom = <HTMLElement>document.querySelector('#action-tip')
            dom.innerHTML = `进入${floor}层`
            dom.style.display = 'block'
            dom.style.background = '#fff'

            setTimeout(() => {
                dom.style.display = 'none'
                dom.style.background = 'none'

            }, 1000 * 2);
        }
    }

    isStairsAround(compare: 'later' | 'before'): stairsInfoType {
        let prePos
        let toDoAction: 'down' | 'up' = 'down'
        let floorName

        if (compare === 'later') {
            prePos = panoStatus.position + 1
        } else {
            prePos = panoStatus.position - 1
        }

        const crtPos = panoStatus.position
        if (panoStatus.check(prePos)) {

            const prePosFloor = management.pathGen.floorMap(panoStatus.path[prePos].videoIdPrefix)
            const crtPosFloor = management.pathGen.floorMap(panoStatus.path[crtPos].videoIdPrefix)

            if (prePosFloor !== crtPosFloor) {

                if (compare === 'before') {

                    if (parseInt(crtPosFloor, 10) > parseInt(prePosFloor)) {
                        toDoAction = 'up'
                        floorName = crtPosFloor
                    } else {
                        floorName = crtPosFloor
                    }

                }

                if (compare === 'later') {

                    if (parseInt(crtPosFloor, 10) < parseInt(prePosFloor)) {
                        toDoAction = 'up'
                        floorName = prePosFloor
                    } else {
                        floorName = prePosFloor
                    }

                }

                const ans = {
                    status: true,
                    floor: floorName,
                    action: toDoAction
                };

                return ans
            }
        }

        const denyAns = {
            status: false,
            floor: 1000,
            action: toDoAction
        };

        return denyAns

    }

    reachStartNavigationToEndNavigation() {
        this.blinkAfterGoFlag = {
            startNavigation: JSON.parse(JSON.stringify(status.currentHotspot)),
            endNavigation: JSON.parse(JSON.stringify(status.endHotspot))
        };
        console.log(this.blinkAfterGoFlag);
        status.currentHotspot = status.hotspotBeforePreview
        status.endHotspot = this.blinkAfterGoFlag.startNavigation;
        this.roamBlind();
    }


    checkIsNeedBlinkAfterGo() {
        if (this.blinkAfterGoFlag) {
            status.currentHotspot = this.blinkAfterGoFlag.startNavigation
            status.endHotspot = this.blinkAfterGoFlag.endNavigation

            management.getPath()

            this.roamPreview()
            this.blinkAfterGoFlag = undefined
        }
    }

    updateMiniProgramShareInfo() {
        if (panoStatus.isOrigin) {
            management.updateUrlShareCurrentLocation(moveStatusType.navigation, status.currentHotspot.id);
        } else if (panoStatus.position >= panoStatus.path.length - 1) {
            management.updateUrlShareCurrentLocation(moveStatusType.navigation, status.endHotspot.id);
        } else {
            management.updateUrlShareCurrentLocation(moveStatusType.way, status.endHotspot.id);
        }
    }
}

let pano = panoCore.instance

export { pano }
