require('../component/wechatShare');
var icon = require('../component/progress-bar/icon');
var projectConfig = require('../config/projectConfig');
var videoProgressBar = require('../component/progress-bar/bar.js');
var util = require('./util');
var Walker = require('../component/walker.js');

var roamId = getQueryString("roamId") ? getQueryString("roamId") : "58d9f909b123db199f4c86e3";
var environment = getQueryString("environment") ? getQueryString("environment") : "api";
var videoChosen=getQueryString("choice") ? getQueryString("choice") : '';

var currentAngle={angle:0,index:0};
var _transition;
var videoList = [];
var hotspots = [];
var oldhotspots = [];
var allHotSpots = [];
var beginIndex;
var beginSpot;
var currentTimer;
var currentTransition;
var currentHotSpotIndex = null;
var currentVrObject;
var currentOffset;
var prevAngle, prevOffset;
var stopTransitTime;
var whetherNeedStop = false; //whether current video has to pause in specific place

var vrPlayer;
var vrProgressBar;
var vrImg;
var isDisplayVideo = false;
var isFirstPlay = false;
var isFinish_all = false;
var DELTA_PC = 0.03; //0.245;// -0.11; //-0.245;//
var DELTA_Android = -0.13; //0.24;// -0.1;
var DELTA_IOS = 0.04; // 0.1; //-0.2;
var DELTA = DELTA_PC;
var THETA = 1;
var SPEED = 10;
var PRE_SECONDS = 0.5;

var BUS_MODE = 1;
var SPECIFIC_MODE = 2;
var ABSOLUTE_MODE = 3;
var currentMode = BUS_MODE;
var isDebug_ = false;

var _route;
var _transitStartNode;
var _transitEndNode;
var _tranTimeoffset;

var walker;
var isContinuePaused=false;
var pathBox=[];
function displayImg(imgSrc,startAngle) { 
    if (!imgSrc) {
        util.failedtoLoad();
        return;
    }
    vrImg = new InRealPlayer({
        parent: "vrImg-panel",
        source: imgSrc,
        publicKey: projectConfig.publicKey,
        autoLoad: false,
		showBottomMark: true,
		bottomMark: projectConfig.bottomMark,
		defaultYaw:startAngle
    });
    vrImg.load();
	walker = new Walker(vrImg,'vrImg-panel', projectConfig.publicKey,projectConfig.bottomMark,startAngle);
    videoChosen!="" && walker.chooseVideo(videoChosen);
    vrImg.on('canplay', function () {
        vrImg.setFullScreen(true);
    });
} //viewer for img

function playVideo(videoSrc,transition) {
	_transition=transition;
    if (isDisplayVideo) return;
    if (!videoSrc) {
        util.failedtoLoad()
        return;
    }	

    vrProgressBar = new videoProgressBar(walker,true);
    
    walker.on('videoCanplay', function () {
    	console.log("canplay");

        if (!isDisplayVideo) {
        	hideLoadingimg();
            vrProgressBar.onlySetFullScreen();
            walker.setLoop(false);
            isDisplayVideo = true;     
        }
    });
    walker.on("videoEnded", function () {
    	if(!isContinuePaused){
	    	finishVideo(true, null, null);
    	}
    });
    walker.on("videoLoaded", function () {
        hideLoadingimg();
    });
    walker.on('videoPlay', function () {
        $('#playPauseControlButton').attr("src", icon.pauseIconBase64);
        showBtnGroup();
//      if (currentTimer) return;
        isFinish_all = false;
        isContinuePaused=false;
    });
    walker.on('videoPause', function () {
    	console.log("pause");
        $('#playPauseControlButton').attr("src", icon.playIconBase64);
        if (walker.duration() == walker.currentTime())
            isFinish_all = true;
    });
    walker.on('continuePaused',function(){
    	finishVideo(false, true, true);
    	isContinuePaused=true;
    });
    walker.on('beginSeek',function(){
    	console.log("beginSeek");
    	showLoadingimg();
    });
    walker.on('endSeek',function(){
    	console.log("endSeek");
    	hideLoadingimg();
    });
    walker.on('videoError', function (err) {
        util.failedtoLoad()
    });
    
	walker.on('changeAngleStart', function() {
		if(walker.currentTime())
			freezeInteraction();
		currentAngle.index++;
		currentAngle.angle=_transition[currentAngle.index].offset;
	});
	walker.on('changeAngleEnd', function() {
		invokeInteraction();
	});
//  return vrPlayer;
} //play


function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return decodeURI(r[2]);
    return null; //unescape(); 
}
getVideoList();
testFunction();
btnBinding();

function btnBinding() {
    //bind angle button
    $('#angleControlButton').click(function () {
        freezeInteraction();
        currentVrObject.setYaw(currentAngle.angle);
    });
}

function testFunction() {
    //?isDebug=true&delta=0.1&mode=1&startNode=12&endNode=12
    var isDebug = getQueryString("isDebug") ? getQueryString("isDebug") : '';
    var startPos, endPos;
    if (isDebug == "true") {
        currentMode = parseInt(getQueryString("mode")) ? parseInt(getQueryString("mode")) : currentMode;
        DELTA = parseFloat(getQueryString("delta")) ? parseFloat(getQueryString("delta")) : DELTA;
        startPos = parseInt(getQueryString("startNode")) ? parseInt(getQueryString("startNode")) : '';
        endPos = parseInt(getQueryString("endNode")) ? parseInt(getQueryString("endNode")) : '';
        console.log("isDebug:", isDebug);
        console.log("currentMode", currentMode);
        console.log("delta:", DELTA);
        console.log("startPos:", startPos);
        console.log("endPos:", endPos);
        if (startPos && endPos) {
            isDebug_ = true;
            console.log("startPos!");
            $('.test-btn').css("display", "block");
            $('#gotoRoute').click(function () {
                var obj = {
                    roamId: roamId,
                    startNodeId: startPos,
                    endNodeId: endPos
                };
                $.post("http://" + environment + ".zhixianshi.com/v2/roam/path", obj, function (result) {
                    console.log("gotoRoute-result:", result);
                    var src = result.index;
                    if (util.isIOS9OrLess() || !util.browser.versions.ios)
                        src = result.video;

                    playSectionVideo(src, result.transition, startPos);
                }); //post
            }); //click
        } //if 
    } //if - isDebug
}

function getVideoList() { //get all the hotspots "http://10.10.10.141/roam/freeRoam/hotspotsDetail.json"//
	getTitle(environment,roamId, function() {
        $.ajax({
            type: "GET",
            url:  "http://" + environment + ".zhixianshi.com/v2/roam/hotspot?roamId=" + roamId,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(result) {
                hotspots = result.hotspots.slice();
                allHotSpots = result.hotspots.slice();
                for (var index in hotspots) {
                    if (!hotspots[index].hasOwnProperty("start")) continue;
                    beginSpot = hotspots[index];
                    beginIndex = index;
                    hotspots.splice(index, 1);
                    displayImg([beginSpot.snapshot],JSON.parse(beginSpot.position).angle);//-180);
                    createHotspotElem(true);
                    return;
                }
            },//success
            error:function(XMLHttpRequest, textStatus, errorThrown){
                util.failedtoLoad()
            }
        });
    });
}
function getTitle (environment,roamId, callback){
	$.ajax({
		type: "GET",
		url:  "http://"+environment+".zhixianshi.com/v2/roam/info?roamId="+roamId,
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function(result) {
			document.title=result.result.name;
			projectConfig.bottomMark= (result.result.hasOwnProperty("bottomMark")&& result.result.bottomMark!="") ? JSON.parse(result.result.bottomMark) : undefined;
            callback && callback();
		}
	});
}
function createHotspotElem(isStart) {
    $('#navbar').empty();
    for (var index in hotspots) {
    	if(hotspots[index].hasOwnProperty("start") && hotspots[index].start)	continue;
        var posterSite;
        hotspots[index].poster !== undefined ? posterSite = ' src="' + hotspots[index].poster + '"' : posterSite = 'src="images/click_logo.png"';
        var ele = '<div id="video_' + index + '" class="preview-box "><div class="img-box"><img class="image-responsive" ' + posterSite + ' /></div><p>' + hotspots[index].desc + '</p></div>';
        $('#navbar').append(ele);
        if(isStart)	fetchSectionVideoDetail(isStart,index, 0, 1000, false);
        document.querySelector('#video_' + index).addEventListener('click', function () {
            var id = parseInt(this.id.substr(6));
            if(isStart){
            	playStartVideo(id);
				return;
            }
            showLoadingimg();
            if (whetherNeedStop) {
                var restTime = (walker.duration() - stopTransitTime) * 1000;
                hideNavBar();
                fetchSectionVideoDetail(isStart,id, restTime, 1000, true);
            } else {
                fetchSectionVideoDetail(isStart,id, 0, 1000, false);
            }
        });
    }
    showNavBar();
}

function playStartVideo(index){
	if(!pathBox.length)	return;
	var obj=pathBox[index];
    currentHotSpotIndex = index;
	showLoadingimg();
	playSectionVideo(obj.src,obj.transition, obj.startPosition,obj.endPosition,obj.nodeId,obj.startNode,obj.endNode,obj.duration,obj.angleDuration,obj.whetherStopBefore,obj.timeoffset);
	pathBox=[];
}
function fetchSectionVideoDetail(isStart,index, duration, angleDuration, whetherStopBefore) { //hotspot index
	whetherNeedStop = false; //set unable
    var startNode;
    var endNode;
    var prevNode;
    var transitNodeIds = [];
    var timeoffset=0;
    var obj = {};

    var position = JSON.parse(hotspots[index].position);
    var startPosition;
    var endPosition = position;
    
    if (currentHotSpotIndex != null) {
        startNode = parseInt(currentTransition[currentTransition.length - 1].startNodeId);//endNodeId; //eval('('+oldhotspots[currentHotSpotIndex].position+')').nodeId;
        prevNode = parseInt(currentTransition[currentTransition.length - 1].startNodeId);
    	transitNodeIds.push(parseInt(currentTransition[currentTransition.length-1].endNodeId));
    	startPosition =	JSON.parse(oldhotspots[currentHotSpotIndex].position);
    } else { //vrImg
        startNode = eval('(' + beginSpot.position + ')').nodeId;
        prevNode = eval('(' + beginSpot.position + ')').oppositeNodeId;
        transitNodeIds.push(prevNode);
        timeoffset=eval('(' + beginSpot.position + ')').timeoffset;
        startPosition=beginSpot.position;
    }
	if (position.hasOwnProperty("timeoffset")) {
	    endNode = position.oppositeNodeId;
	    transitNodeIds.push(position.nodeId);
	} else
        endNode = position.nodeId;
    obj = {
        roamId: roamId,
        startNodeId: startNode,
        endNodeId: endNode,
        prevNodeId: prevNode
    };
    if (transitNodeIds.length) {
        obj["transitNodeIds"] = transitNodeIds;
        whetherNeedStop = true; //set able when current video has to pause in a specific place;
    }
    if(whetherStopBefore )
    	delete obj.prevNodeId;
    	
    if( oldhotspots.length && eval('('+oldhotspots[currentHotSpotIndex].position+')').hasOwnProperty("timeoffset"))
    	timeoffset=eval('('+oldhotspots[currentHotSpotIndex].position+')').timeoffset;

    if(position.hasOwnProperty("videoId"))
    	obj["transitVideoIds"]=[position.videoId];
    
    if(startPosition && startPosition.hasOwnProperty("videoId")){
    	var id = startPosition.videoId;
    	Array.isArray(obj["transitVideoIds"]) ? obj["transitVideoIds"].push(id) : obj["transitVideoIds"]=[id];
    }	
    $.ajax({
        type:"POST",
        url: "http://" + environment + ".zhixianshi.com/v2/roam/path",
          data: JSON.stringify(obj),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (result) {
            var  src = result.video;
            if(isStart){	
	            pathBox[index]={
	            	src:src,
	            	transition:result.transition,
	            	startPosition: startPosition,
	            	endPosition : endPosition,
	            	nodeId:position.nodeId, 
	            	startNode:startNode, 
	            	endNode:endNode, 
	            	duration:duration, 
	            	angleDuration:angleDuration, 
	            	whetherStopBefore:whetherStopBefore,
	            	timeoffset:timeoffset
	            };
            	return;
            }
            currentHotSpotIndex = index;
            playSectionVideo(src, result.transition,startPosition,endPosition, position.nodeId, startNode, endNode, duration, angleDuration, whetherStopBefore,timeoffset);
        },
        error:function(XMLHttpRequest, textStatus, errorThrown){
        	util.failedtoLoad()
        }
    });

}
function getSeekTime(startPosition,transition){
	if(!startPosition.hasOwnProperty("videoId"))	return null;
	var start = startPosition.nodeId;
	var end = startPosition.oppositeNodeId;
	var time = parseFloat(startPosition.timeoffset);
	//order
	for(var index in transition){
		if(start == transition[index].startNodeId 
			&& end == transition[index].endNodeId 
			&& startPosition.videoId == transition[index].videoId)
				return time;
	}
	//out of order
	for(var index in transition){
		if( end == transition[index].startNodeId 
			&& start == transition[index].endNodeId 
			&& startPosition.videoId == transition[index].videoId){
				var after = parseInt(index)+1;
				var before = parseInt(index)-1;
				var realTimeoffset = index != (transition.length-1) 
									 ? (transition[after].time - transition[index].time)
									 : (transition[index].time - transition[before].time);
				realTimeoffset = realTimeoffset - time; 
				return parseFloat(transition[index].time)+parseFloat(realTimeoffset);//
			}
	}
}
function setStopTransitTime(transition, startNode, endNode,endPosition,seekTime) {
    if (!whetherNeedStop) return;
    var isVideoId, realTimeoffset;
    var timeoffsetStart = parseFloat(eval('(' + hotspots[currentHotSpotIndex].position + ')').timeoffset);
    var timeoffsetEnd = parseFloat(endPosition.timeoffset);
    var hasVideoId;
    for (var index in transition) {
    	hasVideoId = transition[index].hasOwnProperty("videoId") && endPosition.hasOwnProperty("videoId");
        isVideoId = hasVideoId ? endPosition.videoId==transition[index].videoId : true;
        if (transition[index].startNodeId == startNode &&
            transition[index].endNodeId == endNode && isVideoId) {
            stopTransitTime = parseFloat(transition[index].time) + timeoffsetStart; //when to stop the video
            if(seekTime < stopTransitTime) return;
        } //if
    } //for
    if(!endPosition.hasOwnProperty("videoId"))	return;
    for (var index in transition) {
    	isVideoId = (transition[index].hasOwnProperty("videoId") && endPosition.hasOwnProperty("videoId")) 
            			? endPosition.videoId==transition[index].videoId : true;
    	if (transition[index].startNodeId == endNode &&
            transition[index].endNodeId == startNode && isVideoId) {
			var after = parseInt(index)+1;
			var before = parseInt(index)-1;
            realTimeoffset = index != (transition.length-1) 
									 ? (transition[after].time - transition[index].time)
									 : (transition[index].time - transition[before].time);
			realTimeoffset = realTimeoffset - timeoffsetEnd;//transition[index].reverse ? realTimeoffset : timeoffsetEnd; 
            stopTransitTime = parseFloat(transition[index].time) + realTimeoffset; //when to stop the video
            if(seekTime < stopTransitTime) return;
        } //if
    }
}

function playSectionVideo(src, transition,startPosition, endPosition, transitStartNode, startNode, endNode, duration, angleDuration, whetherStopBefore,timeoffset) { //duration:rest length of video , angleDuration:duration for turning angle
    var seekTime = getSeekTime(startPosition,transition);
    seekTime = (seekTime == null) ? timeoffset : seekTime;
    
    //set pause time;
    setStopTransitTime(transition, transitStartNode, endNode,endPosition,seekTime);
    //set corner & prevOffset when displaying image
    var pos=0;
    var newTime=walker.cutExtraParts(seekTime,stopTransitTime,{transition:transition.slice()});
    for (var index in transition){
    	if(transition[index].time<newTime.seekTime)	continue;
    	pos=index-1;
    	if(transition[index].time==newTime.seekTime)	pos=index;
    	break;
    }
    
    var diffAngleSum=0;
    var diffAngle;
    var offset;
	for(var i=pos;i>=0;i--){
	    if(i==0)	offset=JSON.parse(beginSpot.position).offset;
	    else	offset=transition[i-1].offset;
	    diffAngle=transition[i].offset + transition[i].corner - offset;
	    diffAngleSum+=diffAngle;
	}
    var endAngle=walker.getYaw() + diffAngleSum ;//transition[pos].offset - diffAngleSum;
    var callback  =  null;
//  if(!isFirstPlay)
//  	markCurrentVrObject(startNode, angleDuration, 10, transition[0].offset, endAngle, callback, whetherStopBefore);
//	else
//		angleDuration=0;
//	setTimeout(function(){
//		prevAngle = currentVrObject.getYaw();

		currentAngle.angle=transition[0].offset;// the correct angle of different time.
		currentAngle.index=0;
	
		currentVrObject=walker;
        showVideo();
        currentTransition = transition;
        playVideo([src],transition);
        walker.addRoute(src,transition,null);
        if(!isFirstPlay){
    		var startAngle=transition[pos].offset;
        	walker.continue(seekTime,stopTransitTime,endAngle,function(result){
	        	walker.continue(seekTime,stopTransitTime,endAngle,function(result){
	        	});
	        	isFirstPlay = true;
	        });
	        return;
        }//isfirstplay
        walker.continue(seekTime,stopTransitTime,null,null);
//	},angleDuration);

}

function markCurrentVrObject(startNode, duration, durationInterval, offset, endAngle, callback, whetherStopBefore) {
    //set initial angle
    var beginspotNodeID = eval('(' + beginSpot.position + ')').nodeId;
    if (!isFirstPlay){
        currentVrObject = vrImg;
        modifyStartAnlge(vrImg, duration, durationInterval, offset, endAngle, callback);
    }
}

function modifyStartAnlge(vrObject, duration, durationInterval, offset, endAngle, callback) {
    //	freezeInteraction();
    var end = endAngle;
    currentOffset = end;
    preChangeAngle(vrObject, end, duration, 2, durationInterval, function () {
        invokeInteraction();
        callback && callback();
    });
}

function finishVideo(whetherClearInterval, whetherShowNavBar, whetherChangeAngle) {
    if (whetherClearInterval) {
        clearInterval(currentTimer);
        currentTimer = null;
    }


    oldhotspots = hotspots.slice();//save the oldhotspots
    hotspots = allHotSpots.slice();//get all the hotspots
    var finalAngle = eval('(' + oldhotspots[currentHotSpotIndex].position + ')').angle;
    for (var index in hotspots) {
        if (hotspots[index].id != oldhotspots[currentHotSpotIndex].id) continue;
        hotspots.splice(index, 1);
    }
    createHotspotElem(false);
    showNavBar();
    if (isNaN(finalAngle)) return;
    freezeInteraction();
    preChangeAngle(walker, finalAngle, 1000, 2, 10, invokeInteraction);
}

function removeHotspotPlayed() {
    oldhotspots = hotspots;
    hotspots = allHotSpots.slice();
    for (var index in hotspots) {
        if (hotspots[index].id != oldhotspots[currentHotSpotIndex].id) continue;
        hotspots.splice(index, 1);
    }
}



function preChangeAngle(vrObject, end, duration, type, durationInterval, callback) {
    var startAngle = parseInt(vrObject.getYaw());
    var endAngle = parseInt(end) % 360;
    if (startAngle < 0) startAngle += 360;
    if (endAngle < 0) endAngle += 360;

    var distance = Math.abs(endAngle - startAngle);
    if (distance <= 1) {
        callback && callback();
        return;
    }

    var direction = false;
    if (endAngle > startAngle) direction = true;

    if (distance > 180) {
        direction = !direction;
    }

    walker.setYaw2(endAngle, direction ? 1 : 0, duration, type, function () {
        callback && callback();
    });
}

function hideBtnGroup() { //!util.browser.versions.mobile \ (isFinish_all || whetherNeedStop) && pause
    $('#angleControlButton').css("display", "none");
    $('#VRmodeControlButton').css("display", "none");
    $('#playPauseControlButton').css("display", "none");
}

function onlyShowPlayBtn() { //!(isFinish_all || whetherNeedStop) &&pause
    $('#angleControlButton').css("display", "none");
    $('#VRmodeControlButton').css("display", "none");
    $('#controlBar').css({
        "left": "50%",
        "bottom": "50%",
        "margin-left": "-50px",
        "margin-top": "-50px"
    });
    $('#playPauseControlButton').css({
        "width": "100px",
        "height": "114px",
        "display": "block"
    });
}

function hidePlayBtn() { //(!isStopSection && time >= stopTransitTime)
    $('#playPauseControlButton').css("display", "none");
}

function showBtnGroup() { //play
    $('#angleControlButton').css("display", "block");
//  $('#VRmodeControlButton').css("display", "block");
    $('#controlBar').css({
        "left": "0px",
        "bottom": "40%",
        "margin-left": "0px",
        "margin-top": "0px"
    });
    $('#playPauseControlButton').css({
        "width": "24px",
        "height": "auto",
        "display": "block"
    });
}

function hideNavBar() { //whetherNeedStop
    $('#navbar').css("display", "none");
}

function showNavBar() {
	var display='-webkit-inline-box';
	var num=  $('.preview-box').css("width").slice(-1)=="%"   
			? parseFloat($('.preview-box').css("width").slice(0,-1))*0.01*document.body.offsetWidth 
			: parseFloat($('.preview-box').css("width").slice(0,-2));
	var len=$('.preview-box').length;
	var sum=num*len;
	if(!util.browser.versions.mobile ||	(sum && document.body.offsetWidth && sum < document.body.offsetWidth))	
		display="inline-flex";//pc,box<bodywidth
    $('#navbar').css("display", display);
    hideBtnGroup();
}

function hideVrImg() {
    hideNavBar();
//  $('#vrImg-panel').css("opacity", "1");
}

function showVideo() {
    hideVrImg();
    showBtnGroup();
    $('#controlBar').css("opacity", "1");
//  $('#vr-panel').css({
//      "opacity": "1"
//  });
}

function showLoadingimg() {
    $('.loading').css("display", "block");
}

function hideLoadingimg() {
    $('.loading').css("display", "none");
}

function freezeInteraction() {
    $('#playPauseControlButton').css("pointer-events", "none");
    $('#angleControlButton').css("pointer-events", "none");
    currentVrObject.setDeviceMotion(false);
}

function invokeInteraction() {
    $('#playPauseControlButton').css("pointer-events", "auto");
    $('#angleControlButton').css("pointer-events", "auto");
    currentVrObject.setDeviceMotion(true);
}

function play() {
    if (walker.isPaused())
        walker.continue();
    $('#playPauseControlButton').attr("src", icon.pauseIconBase64);
}

function pause() {
    walker.pause();
    $('#playPauseControlButton').attr("src", icon.playIconBase64);
}