function share(){
	this.playPauseControlButton = document.querySelector("#playPauseControlButton");
    this.VRmodeControlButton = document.querySelector("#VRmodeControlButton");
    this.angleControlButton = document.querySelector("#angleControlButton");
    this.loading = document.querySelector('#loading');
    this.panel=document.querySelector("#vrImg-panel");
}

share.prototype.hideBtnGroup=function(){
	this.playPauseControlButton.style.display="none";
    this.VRmodeControlButton.style.display="none";
    this.angleControlButton.style.display="none";
}
share.prototype.hideLoadingimg=function(){
	this.loading.style.display="none";
}
share.prototype.removePanel=function(){
	this.panel.parentNode.removeChild(this.panel);
}
module.exports = share;