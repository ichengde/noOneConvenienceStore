window.NodeList.prototype.forEach = Array.prototype.forEach

var util = {}
var icon = require('../component/progress-bar/icon')
var Share = require('./share.js')
var share = new Share()

var browser = {
  versions: function () {
    var u = navigator.userAgent,
      app = navigator.appVersion
    return { // 移动终端浏览器版本信息   
      trident: u.indexOf('Trident') > -1, // IE内核  
      presto: u.indexOf('Presto') > -1, // opera内核  
      webKit: u.indexOf('AppleWebKit') > -1, // 苹果、谷歌内核  
      gecko: u.indexOf('Gecko') > -1 && u.indexOf('KHTML') === -1, // 火狐内核  
      mobile: !!u.match(/AppleWebKit.*Mobile.*/), // 是否为移动终端  
      ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), // ios终端  
      android: u.indexOf('Android') > -1 || u.indexOf('Linux') > -1, // android终端或者uc浏览器  
      iPhone: u.indexOf('iPhone') > -1, // 是否为iPhone或者QQHD浏览器  
      iPad: u.indexOf('iPad') > -1, // 是否iPad    
      webApp: u.indexOf('Safari') === -1 // 是否web应该程序，没有头部与底部  
    }
  }(),
  language: (navigator.browserLanguage || navigator.language).toLowerCase()
}
util.browser = browser
util.getQueryString = function (name) {
  var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i')
  var r = window.location.search.substr(1).match(reg)
  if (r != null) return decodeURI(r[2])
  return null; // unescape(); 
}
util.isIOS = function () {
  return /(iPad|iPhone|iPod)/g.test(navigator.userAgent)
}
// check ios
util.isIOS9OrLess = function () {
  if (!util.isIOS) {
    return false
  }
  var re = /(iPhone|iPad|iPod) OS ([\d_]+)/
  var iOSVersion = navigator.userAgent.match(re)
  if (!iOSVersion) {
    return false
  }
  // Get the last group.
  var versionString = iOSVersion[iOSVersion.length - 1]
  var majorVersion = parseFloat(versionString)
  return majorVersion <= 9
}
util.updateUrl = function (url, key) {
  var key = (key || 't') + '='; // 默认是"t"
  var reg = new RegExp(key + '\\d+'); // 正则：t=1472286066028
  var timestamp = +new Date()
  if (url.indexOf(key) > -1) { // 有时间戳，直接更新
    return url.replace(reg, key + timestamp)
  }else { // 没有时间戳，加上时间戳
    if (url.indexOf('\?') > -1) {
      var urlArr = url.split('\?')
      if (urlArr[1]) {
        return urlArr[0] + '?' + key + timestamp + '&' + urlArr[1]
      }else {
        return urlArr[0] + '?' + key + timestamp
      }
    }else {
      if (url.indexOf('#') > -1) {
        return url.split('#')[0] + '?' + key + timestamp + location.hash
      }else {
        return url + '?' + key + timestamp
      }
    }
  }
}
util.failedtoLoad = function () {
  share.removePanel()
  share.hideBtnGroup()
  var timer = setInterval(function () {
    share.hideLoadingimg()
    if ($('.loading').css('display') != 'block')	clearInterval(timer)
  }, 1000)
  if ($('#error-info').css('display') == 'block')	return
  $('#error-info').css('display', 'block')
  $('#error-info').append('<div><p>Network error</p><img class="refresh" src="' + icon.refreshIconBase64 + '" style="width:30px;cursor:pointer"/></div>')
  $('.refresh').click(function () {
    if (browser.versions.android)
      location.href = util.updateUrl(location.href)
    else
      location.reload()
  })
}

util.isWechat = function () {
  return /(MicroMessenger)/g.test(navigator.userAgent)
}
module.exports = util
